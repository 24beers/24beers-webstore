/**
 * JavaScript methods used within the web store.
 * Requires the use of jQuery >= v1.9.
 *
 * Copyright 24 Beers Limited.
 */

/**
 * Add product data to minibasket HTML object.
 * 
 * @param {object} htmlObj
 * @param {object} basketData
 * @return {void}
 */
function addProductInfo(htmlObj, basketData) {

    var subtotal = 0;
    var basketSize = basketData.items.length;
    var counter = 1;

    $.each(basketData.items, function(index, basketItem) {
        getProductInfo(basketItem.code, function(name, price, display_price) {
            subtotal += basketItem.quantity * price;
            var htmlItem = $('<div class="item">').appendTo(htmlObj);
            $('<div class="bold">' + name + '</div>' +
                    '<div>' + basketItem.quantity + ' x ' + display_price + '</div>').appendTo(htmlItem);
        }).done(function() {
            if (basketSize === counter++) {
                $('<div class="subtotal">Subtotal: <span class="bold">&pound;' + subtotal.toFixed(2) + '</span></div>').appendTo(htmlObj);
            }
        });
    });
}

function addToBasket(addUrl) {
    $('#btnAddToBasket').attr('disabled', 'disabled');
    $.post(addUrl, {
        sku_code: $('#skuCode').val(),
        quantity: $('#txtQuantity').val()
    }).done(function() {
        reloadBasket();
        $('#btnAddToBasket').prop('disabled', false);
        setTimeout(function() {
            $('#hdrBasket .minibasket').fadeToggle();
        }, 3000);
    });
    return false;
}
/**
 * Generate basket and product content for the mini-basket.
 *
 * @return {void}
 */
function generateMinibasket() {
    $.get('/basket/get').done(function(data) {
        addProductInfo($('#hdrBasket .minibasket .items'), JSON.parse(data));
    });
}

/**
 * Get product data using AJAX GET call.
 * 
 * @param {string} code
 * @param {function} callback
 * @return {@exp;@exp;$@pro;get@call;@call;done|@exp;$@pro;get@call;@call;done}
 */
function getProductInfo(code, callback) {
    return $.get('/product/get_sku/' + code).done(function(productData) {
        var product = JSON.parse(productData);
        callback(product.name, product.sale_price, product.displayable_sale_price);
    });
}

/**
 * Populate the basket content with fresh data and show the mini-basket.
 *
 * @return {void}
 */
function reloadBasket() {

    var minibasket = $('#hdrBasket .minibasket');

    if (minibasket.is(':visible')) {
        minibasket.fadeToggle();
    }

    $('#hdrBasket .minibasket .items').html('');
    generateMinibasket();
    minibasket.fadeToggle();
}

/* Run these functions after everything else in the page has loaded. */
$(window).load(function() {
    /* Mini-basket pop-up in page header. */
    $('#hdrBasket .minibasket').ready(function() {
        var minibasket = $('#hdrBasket .minibasket');
        minibasket.hide();
        generateMinibasket();
        $('#hdrBasket').click(function(event) {
            event.stopPropagation();
            minibasket.fadeToggle();
            return false;
        });
    });

    /* Redirect to basket page when button is clicked. */
    $('#btnGotoBasket').ready(function() {
        $('#btnGotoBasket').click(function() {
            document.location.href = $('#btnGotoBasket').attr('data-href');
            return false;
        });
    });

    /* Account menu in page header. */
    $('#pageheader .accountmenu').ready(function() {
        $('#pageheader .accountmenu ul').hide();
        $('#pageheader .accountmenu .menu-icon').click(function(event) {
            event.stopPropagation();
            $('#pageheader .accountmenu ul').fadeToggle();
            return false;
        });
    });
});