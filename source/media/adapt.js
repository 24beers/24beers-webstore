(function(w, d, config, undefined) {
    if (!config) {
        return;
    }

    var url, url_old, timer;

    var callback = config.callback || function() {};
    var path = config.path ? config.path : '';
    var range = config.range;
    var range_len = range.length;

    var css = d.createElement('link');
    css.rel = 'stylesheet';
    css.media = 'screen';

    function change(i, width) {
        css.href = url;
        url_old = url;
        callback(i, width);
    }

    function adapt() {
        clearTimeout(timer);

        var width = d.documentElement ? d.documentElement.clientWidth : 0;

        var arr, arr_0, val_1, val_2, is_range, file;

        var i = range_len;
        var last = range_len - 1;
        url = '';

        while (i--) {
            arr = range[i].split('=');
            arr_0 = arr[0];
            file = arr[1] ? arr[1].replace(/\s/g, '') : undefined;
            is_range = arr_0.match('to');
            val_1 = is_range ? parseInt(arr_0.split('to')[0], 10) : parseInt(arr_0, 10);
            val_2 = is_range ? parseInt(arr_0.split('to')[1], 10) : undefined;

            if ((!val_2 && i === last && width > val_1) || (width > val_1 && width <= val_2)) {
                file && (url = path + file);
                break;
            }
        }

        if (!url_old) {
            change(i, width);
            path && (d.head || d.getElementsByTagName('head')[0]).appendChild(css);
        } else if (url_old !== url) {
            change(i, width);
        }
    }

    adapt();

    function react() {
        clearTimeout(timer);

        timer = setTimeout(adapt, 16);
    }

    if (config.dynamic) {
        if (w.addEventListener) {
            w.addEventListener('resize', react, false);
        } else if (w.attachEvent) {
            w.attachEvent('onresize', react);
        } else {
            w.onresize = react;
        }
    }
})(this, this.document, ADAPT_CONFIG);