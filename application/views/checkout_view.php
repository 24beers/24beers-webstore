<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once 'include/header_meta.php'; ?>
        <title><?php echo $page_title; ?> | Checkout | 24 Beers</title>
        <meta name="robots" content="noindex">
    </head>
    <body itemscope itemtype="http://schema.org/CheckoutPage">
        <?php require_once 'include/page_header_min.php'; ?>
        <div class="container_12" id="checkout">
            <?php echo $content; ?>
        </div>
        <!-- End 12 column container -->
        <?php require_once 'include/page_footer_min.php'; ?>
        <?php if (isset($payment_key)): ?>
        <script defer>
            function reportError(msg) {
                $('#msgPayErr').append(msg).append('<br>');
                $('#btnSubmitOrder').prop('disabled', false);
                return false;
            }
            $(window).load(function() {
                if ($('#msgJsErr').length > 0) { $('#msgJsErr').remove(); }
    
                $('#formSubmitOrder').submit(function() {
                    if (typeof $('#inpStripeToken').val() !== 'undefined' &&
                            $('#inpStripeToken').val() !== '') {
                        return true;
                    }

                    $('#msgPayErr').text('');
                    $('#btnSubmitOrder').attr('disabled', 'disabled');

                    var error = false;
                    var ccNum = $('.cardnum').val(),
                        cvcNum = $('.cardcvc').val(),
                        expMonth = $('.cardexp_m').val(),
                        expYear = $('.cardexp_y').val();

                     if (!Stripe.validateCardNumber(ccNum)) {
                         error = true;
                         reportError('The credit card number appears to be invalid.');
                     }

                     if (!Stripe.validateExpiry(expMonth, expYear)) {
                         error = true;
                         reportError('The expiration date appears to be invalid.');
                     }

                    if (!Stripe.validateCVC(cvcNum)) {
                        error = true;
                        reportError('The CVC number appears to be invalid.');
                    }

                    if (!error) {
                        Stripe.createToken({
                            number: ccNum,
                            cvc: cvcNum,
                            exp_month: expMonth,
                            exp_year: expYear
                        },
                        function(status, response) {
                            if (response.error) {
                                reportError(status, response.error);
                                return;
                            }

                            $('#inpStripeToken').val(response.id);
                            $('#formSubmitOrder').get(0).submit();
                        });
                    }

                    return false;
                });
            });
        </script>
        <?php endif; ?>
    </body>
</html>