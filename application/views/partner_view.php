<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once 'include/header_meta.php'; ?>
        <title><?php echo $page_title; ?> | <?php echo SITE_TITLE; ?></title>
    </head>
    <body itemscope itemtype="http://schema.org/WebPage">
        <?php require_once 'include/page_header_static.php'; ?>
        <div class="container_12">
            <?php echo $content; ?>
        </div>
        <!-- End 12 column container -->
        <?php require_once 'include/page_footer.php'; ?>
    </body>
</html>