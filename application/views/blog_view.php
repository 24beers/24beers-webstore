<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once 'include/header_meta.php'; ?>
        <title><?php echo $page_title; ?></title>
        <link href="https://plus.google.com/110947345552996147084" rel="author">
    </head>
    <body itemscope itemtype="http://schema.org/WebPage">
        <?php require_once 'include/page_header.php'; ?>
        <div class="container_12" id="blog">
            <?php echo $content; ?>
        </div>
        <!-- End 12 column container -->
        <?php require_once 'include/page_footer.php'; ?>
    </body>
</html>