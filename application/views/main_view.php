<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once 'include/header_meta.php'; ?>
        <title><?php echo isset($page_title) ? "$page_title | " : ""; ?>Buy beer and ale online, UK next day delivery</title>
    </head>
    <body itemscope itemtype="http://schema.org/WebPage">
        <?php require_once 'include/page_header.php'; ?>
        <div class="container_12">
            <?php /*require_once 'include/facet_nav.php';*/ ?>
            <?php echo $content; ?>
        </div>
        <!-- End 12 column container -->
        <?php require_once 'include/page_footer.php'; ?>
    </body>
</html>