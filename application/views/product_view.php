<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once 'include/header_meta.php'; ?>
        <title><?php echo "Buy $product->name from $product->brewery_name online"; ?> | 24 Beers</title>
        <meta property="og:availability" content="instock">
        <meta property="og:description" content="<?php echo $product->description; ?>">
        <meta property="og:image" content="<?php echo $image_url; ?>">
        <meta property="og:price:amount" content="<?php echo $product->name; ?>">
        <meta property="og:price:currency" content="GBP">
        <meta property="og:site_name" content="<?php echo BRAND_NAME; ?>">
        <meta property="og:title" content="<?php echo $product->name; ?>">
        <meta property="og:type" content="product">
        <meta property="og:url" content="<?php echo current_url(); ?>">
    </head>
    <body itemscope itemtype="http://schema.org/WebPage">
        <!-- Begin Facebook SDK -->
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&status=0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <!-- End Facebook SDK -->
        <?php require_once 'include/page_header.php'; ?>
        <div class="container_12">
            <?php echo $content; ?>
        </div>
        <!-- End 12 column container -->
        <?php require_once 'include/page_footer.php'; ?>
        <?php require_once 'include/gplus1.php'; ?>
        <?php /* Removed because SU doesn't yet support HTTPS.
        <!-- Begin StumbleUpon share code -->
        <script type="text/javascript">(function() {
            var li = document.createElement('script'); li.type = 'text/javascript'; li.async = true;
            li.src = '//platform.stumbleupon.com/1/widgets.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(li, s);
        })();</script>
        <!-- End StumbleUpon code -->
        */ ?>
        <script async src="//assets.pinterest.com/js/pinit.js"></script>
        <script src="/media/jquery.raty.min.js"></script>
        <script>
        $(window).load(function() {
            $('#rating').raty({
                path: '/media',
                click: function(score) {
                    $('#inRating').val(score);
                }
            });
            if ($('#reviewsLink').length > 0) {
                $('#reviewsLink').click(function() {
                    $('#reviews').dialog({modal: true});
                    return false;
                });
            }
        });
        </script>
    </body>
</html>