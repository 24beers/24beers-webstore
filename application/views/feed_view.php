<?php echo "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"; ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
    <channel>
        <title><![CDATA[24 Beers Blog]]></title>
        <link>https://www.24beers.co.uk/blog</link>
        <description><![CDATA[News from 24 Beers.]]></description>
        <category>Shopping/Beer/Ecommerce</category>
        <language><?php echo $language; ?></language>
        <copyright><![CDATA[Copyright 24 Beers <?php echo $copyright_year ?>. All rights reserved.]]></copyright>
        <pubDate><?php echo $publish_date; ?></pubDate>
        <atom:link href="<?php echo current_url(); ?>" rel="self" type="application/rss+xml" />
        <?php foreach ($blogs as $blog): ?>
        <item>
            <title><![CDATA[<?php echo $blog->title; ?>]]></title>
            <link><?php echo $blog->url; ?></link>
            <author><?php echo $blog->author; ?></author>
            <description><![CDATA[<?php echo $blog->text; ?>]]></description>
            <pubDate><?php echo $blog->publish_date; ?></pubDate>
            <guid><?php echo $blog->url; ?></guid>
        </item>
        <?php endforeach; ?>
    </channel>
</rss>