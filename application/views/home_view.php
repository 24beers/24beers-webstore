<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once 'include/header_meta.php'; ?>
        <title>Buy real ale and craft beer online, UK delivery | 24 Beers</title>
        <?php require_once 'include/meta_keywords_default.php'; ?>
    </head>
    <body itemscope itemtype="http://schema.org/WebPage">
        <?php require_once 'include/page_header.php'; ?>
        <div class="container_12">
            <?php /*require_once 'include/facet_nav.php';*/ ?>
            <?php echo $content; ?>
        </div>
        <!-- End 12 column container -->
        <?php require_once 'include/page_footer.php'; ?>
    </body>
</html>