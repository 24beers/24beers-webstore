<!-- Begin basket page -->
<h1><?php echo $page_title; ?></h1>
<?php if (isset($basket_messages)): ?>
<ul class="infomessages">
    <?php foreach ($basket_messages as $message): ?>
    <li><?php echo $message; ?></li>
    <?php endforeach; ?>
</ul>
<?php endif; ?>
<div class="grid_12 basket-detail">    
    <p><?php echo validation_errors('', '<br>'); ?></p>
    <?php if (isset($basket->num_items) && $basket->num_items > 0): ?>
    <div class="grid_6 alpha"><h2>Item Description</h2></div>
    <div class="grid_2"><h2>Quantity</h2></div>
    <div class="grid_2"><h2>Price</h2></div>
    <div class="grid_2 omega">&nbsp;</div>
    <?php foreach ($basket->items as $item): ?>
    <div class="grid_12 alpha omega basket-item">
        <div class="grid_2 alpha">
            <img alt="<?php echo $item->name; ?>" height="230" src="<?php echo URL_MEDIA_PRODUCT . "/$item->product_code"; ?>_small.jpg" width="140">
        </div>
        <div class="grid_10 omega">
            <div class="grid_4 alpha"><h3><?php echo $item->name; ?></h3></div>
            <div class="grid_2">
                <a href="<?php echo URL_BASKET_LESS . "/$item->code"; ?>" onclick="_gaq.push(['_trackEvent', '<?php echo APP_GA_EVENT_CAT_BASKET; ?>', '<?php echo APP_GA_EVENT_ACT_DECR; ?>', '<?php echo $item->code; ?>']);" rel="nofollow" title="Remove a <?php echo $item->name; ?>">&minus;</a>
                <span class="qty"><?php echo $item->quantity; ?></span>
                <a href="<?php echo URL_BASKET_MORE . "/$item->code"; ?>" onclick="_gaq.push(['_trackEvent', '<?php echo APP_GA_EVENT_CAT_BASKET; ?>', '<?php echo APP_GA_EVENT_ACT_INCR; ?>', '<?php echo $item->code; ?>']);" rel="nofollow" title="Add a <?php echo $item->name; ?>">&plus;</a>
            </div>
            <div class="grid_2">
                <?php echo format_displayable_price($item->item_total); ?>
                <?php if ($item->quantity > 1): ?>
                (<?php echo format_displayable_price($item->sale_price); ?> each)
                <?php endif; ?>
            </div>
            <div class="grid_2 omega">
                <a href="<?php echo URL_BASKET_REMOVE . "/$item->code"; ?>" onclick="_gaq.push(['_trackEvent', '<?php echo APP_GA_EVENT_CAT_BASKET; ?>', '<?php echo APP_GA_EVENT_ACT_REMOVE; ?>', '<?php echo $item->code; ?>']);" rel="nofollow" title="Remove <?php echo $item->name; ?>">Remove <?php echo $item->name; ?></a>
            </div>
            <div class="clear"></div>
            <div class="grid_10 alpha omega">
                <h4>Description</h4>
                <?php echo $item->description; ?>
            </div>
            <div class="clear"></div>
            <div class="grid_2 alpha">
                <h4>Brewery</h4>
                <?php echo $item->brewery_name; ?>
            </div>
            <div class="grid_2">
                <h4>Style</h4>
                <?php echo $item->style_name; ?>
            </div>
            <div class="grid_2">
                <h4>ABV</h4>
                <?php echo $item->abv; ?>
            </div>
            <div class="grid_2">
                <h4>Container</h4>
                <?php echo $item->size_container; ?>
            </div>
            <div class="grid_2 omega">&nbsp;</div>
        </div>
    </div>
    <!-- end .grid_12 -->
    <?php endforeach; ?>
    <div class="grid_12 alpha omega totals">
        <div class="grid_10 alpha align-right"><h2>Subtotal</h2></div>
        <div class="grid_2 omega align-right"><?php echo format_displayable_price($basket->subtotal); ?></div>
        <div class="clear"></div>
        <div class="grid_10 alpha align-right"><h2>Delivery</h2></div>
        <div class="grid_2 omega align-right"><?php echo format_displayable_price($basket->delivery_charge); ?></div>
        <div class="clear"></div>
        <div class="grid_10 alpha align-right"><h2>Total</h2></div>
        <div class="grid_2 omega align-right"><?php echo format_displayable_price($basket->total); ?></div>
    </div>
    <div class="clear"></div>
    <div class="grid_4 alpha">&nbsp;</div>
    <div class="grid_4">&nbsp;
        <?php /*a class="button-ptve" href="<?php echo URL_CHECKOUT_PAYPAL ?>" rel="nofollow" title="Pay Securely with PayPal">Pay Securely with PayPal &rarr;</a*/ ?>
    </div>
    <div class="grid_4 omega align-right">
        <a class="button-ptve" href="<?php echo URL_BASKET_CHECKOUT ?>" rel="nofollow" title="Pay Securely by Credit/Debit Card">Pay Securely by Card &rarr;</a>
    </div>
    <?php else: ?>
    <div class="grid_4 alpha">You don't have anything in your basket.</div>
    <div class="grid_4 align-center"><a class="button-ptve" href="/">Continue shopping &rarr;</a></div>
    <div class="grid_4 omega">&nbsp;</div>
    <?php endif; ?>
</div>
<!-- End basket page -->
<?php /* content/basket_page.php */