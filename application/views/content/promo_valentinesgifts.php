<!-- Begin Valentine's Day Gifts page -->
<p><span class="bold">We&apos;re feeling the <strong>love</strong> in the air this <strong> Valentine's Day</strong> and there&apos;s no better way to make the one you love feel special than with the gift of <strong>beer</strong>.</span><br>
    You want the perfect beer to set off a home-cooked meal; to spark your taste buds before you go out; to impress the hard-working beer lover in your life. We&apos;ve selected some special ales to make the occasion <em>perfect</em>.</p>
<p>Whenever you buy from us you pay <strong>just &pound;<?php echo STORE_DELIVERY_CHARGE_STANDARD ?> for next working day delivery</strong>.</p>
<h2><a href="<?php echo URL_PRODDETAIL_BEER; ?>/savour-finesse" title="Buy Savour Finesse Saison online"><strong>Savour Finesse</strong> from <strong>Savour Beer</strong></a></h2>
<div class="grid_4">
    <a href="<?php echo URL_PRODDETAIL_BEER; ?>/savour-finesse" title="Buy Savour Finesse Saison online">
        <img alt="Savour Finesse 330ml bottle" height="230" src="<?php echo URL_MEDIA_PRODUCT; ?>/savour-finesse_small.jpg" width="140"></a>
</div>
<div class="grid_8">
    <p>If you ever fancied taking out a Belgian blonde, <strong>Finesse</strong>
        is the one. As the name suggests, this beer has style.</p>
    <p>This is the nicest <strong>Saison</strong> I&apos;ve tried. When you
        inhale the aroma you&apos;re hit by a burst of
        <em>Belgian yeast spices</em> and <em>lemongrass</em>. The flavour is
        refreshing and <em>citrus</em> and you feel the pleasant fizz of big
        <em>Champagne yeast bubbles</em>. The aftertaste is slightly dry and of
        <em>bittersweet hops</em>.</p>
    <p><a href="<?php echo URL_PRODDETAIL_BEER; ?>/savour-finesse" title="Buy Savour Finesse Saison online">Buy <strong>Savour Finesse Saison</strong> &rarr;</a></p>
    <p>If you want more <a href="/category/style/saison" title="Explore all of our saisons">explore all of our <strong>Saisons</strong> &rarr;</a></p>
</div>
<br class="clear">
<h2><a href="<?php echo URL_PRODDETAIL_BEER; ?>/ticketybrew-dubbel" title="Buy TicketyBrew Dubbel online"><strong>TicketyBrew Dubbel</strong> from <strong>TicketyBrew</strong></a></h2>
<div class="grid_4">
    <a href="<?php echo URL_PRODDETAIL_BEER; ?>/ticketybrew-dubbel" title="Buy TicketyBrew Dubbel online">
        <img alt="TicketyBrew Dubbel 330ml bottle" height="230" src="<?php echo URL_MEDIA_PRODUCT; ?>/ticketybrew-dubbel_small.jpg" width="140"></a>
</div>
<div class="grid_8">
    <p>The first thing that enticed me to include
        <strong>TicketyBrew Dubbel</strong> was the seductive design of the
        label draped like a 1950s pin-up model.</p>
    <p>This rich and spicy beer is another British ale brewed in a
        <em>Belgian-style</em> but with its own identity. The flavour is darker
        than the traditional <em>Trappist</em> brews. With extra-dark
        <em>candi syrup</em> and light on the hops to let the yeast flavour
        really stand out. I like to poor the entire contents into my glass for
        the full yeast effect.</p>
    <p><a href="<?php echo URL_PRODDETAIL_BEER; ?>/ticketybrew-dubbel" title="Buy TicketyBrew Dubbel online">Buy <strong>TicketyBrew Dubbel</strong> &rarr;</a></p>
    <p><a href="/search?q=belgian" title="Search our Belgian beers">Search all our <em>Belgian-inspired beers</em> &rarr;</a></p>
</div>
<br class="clear">
<h2><a href="<?php echo URL_PRODDETAIL_BEER; ?>/rise-pale-ale" title="Buy Kubla Rise Pale Ale online"><strong>Rise Pale Ale</strong> from <strong>Kubla Brewery</strong></a></h2>
<div class="grid_4">
    <a href="<?php echo URL_PRODDETAIL_BEER; ?>/rise-pale-ale" title="Buy Kubla Rise Pale Ale online">
        <img alt="Kubla Rise Pale Ale 500ml bottle" height="230" src="<?php echo URL_MEDIA_PRODUCT; ?>/rise-pale-ale_small.jpg" width="140"></a>
</div>
<div class="grid_8">
    <p>If you&apos;re planning to eat seafood for
        <strong>Valentine&apos;s Day</strong> this is the perfect accompaniment.
        This pale ale is anything but pale with a deep <em>amber</em> tan. The
        flavour is zesty <em>citrus</em> bitterness, <em>caramel</em> and
        <em>sweet fruit</em>.</p>
    <p><strong>Kubla</strong> say that all their beers are inspired by their
        natural and wild surroundings. Do you need any more inspiration for your
        <strong>Valentine</strong>?</p>
    <p><a href="<?php echo URL_PRODDETAIL_BEER; ?>/rise-pale-ale" title="Buy Kubla Rise Pale Ale online">Buy <strong>Kubla Rise Pale Ale</strong> &rarr;</a></p>
    <p><a href="/category/style/pale-ale" title="See all of our pale ale">See all of our <em>pale ale</em> &rarr;</a></p>
</div>
<br class="clear">
<h2><a href="<?php echo URL_PRODDETAIL_BEER; ?>/ellenberg-bock" title="Buy Ellenberg&apos;s Brewery Bock"><strong>Ellenberg Bock</strong> from <strong>Ellenberg&apos;s Brewery</strong></a></h2>
<div class="grid_4">
    <a href="<?php echo URL_PRODDETAIL_BEER; ?>/ellenberg-bock" title="Buy Ellenberg&apos;s Brewery Bock">
        <img alt="Ellenberg&apos;s Brewery Bock 500ml bottle" height="230" src="<?php echo URL_MEDIA_PRODUCT; ?>/ellenberg-bock_small.jpg" width="140"></a>
</div>
<div class="grid_8">
    <p>This dark strong <em>wheat beer</em> will go nicely with an after-dinner
        treat such as a rich <em>dark chocolate</em> fondant. The flavours of
        <em>clove</em>, <em>banana</em> and <em>citrus</em> conspire to produce
        a warm beer based on the <strong>German Dunkel Weizenbock</strong> style
        that is perfect for snuggling on a cold wet
        <strong>Valentine&apos;s</strong> evening.</p>
    <p><a href="<?php echo URL_PRODDETAIL_BEER; ?>/ellenberg-bock" title="Buy Ellenberg's Bock online">Buy <strong>Ellenberg&apos; Bock</strong> &rarr;</a></p>
    <p><a href="/search?q=ellenberg" title="See more beers &amp; ales from Ellenberg&apos;s Brewery">See what other beers &amp; ales we have to offer from <strong>Ellenberg&apos;s Brewery</strong> &rarr;</a></p>
</div>
<!-- End Valentine's Day Gifts page -->
<?php /* content/promo_valentinesgifts.php */