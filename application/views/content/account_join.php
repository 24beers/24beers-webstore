<form id="join-form" action="<?php echo URL_ACCT_CREATE; ?>" method="post">
    <?php if (is_basket_complete()): ?>
    <input name="<?php echo PARAM_REDIRECT; ?>" type="hidden" value="<?php echo URL_CHECKOUT; ?>">
    <?php endif; ?>
    <div class="grid_3"><label for="join-name">Name</label></div>
    <div class="grid_3"><input id="join-name" name="name" value="<?php echo set_value('name'); ?>" placeholder="Name"></div>
    <div class="grid_6"><?php echo form_error('name'); ?></div>
    <div class="clear"></div>
    <div class="grid_3"><label for="join-phone">Phone</label></div>
    <div class="grid_3"><input id="join-phone" type="tel" name="phone" value="<?php echo set_value('phone'); ?>" placeholder="Phone"></div>
    <div class="grid_6">
        <span class="hint">In case we need to get hold of you in a hurry</span>
        <?php echo form_error('phone'); ?>
    </div>
    <div class="clear"></div>
    <div class="grid_3"><label for="join-email">Email</label></div>
    <div class="grid_3"><input id="join-email" type="email" name="email" size="25" value="<?php echo set_value('email'); ?>" placeholder="Email"></div>
    <div class="grid_6"><?php echo form_error('email'); ?></div>
    <div class="clear"></div>
    <div class="grid_3"><label for="join-password">Password</label></div>
    <div class="grid_3"><input id="join-password" type="password" name="password" placeholder="Password"></div>
    <div class="grid_6">
        <span class="hint">Minimum length 6 but make it longer for better security</span>
        <?php echo form_error('password'); ?>
    </div>
    <div class="clear"></div>
    <div class="grid_3"><label for="join-dob">Date of Birth</label></div>
    <div class="grid_3"><input id="join-dob" maxlength="10" name="dob" placeholder="<?php echo FORMAT_DATE_INPUT; ?>" type="date" value="<?php echo set_value('dob'); ?>"></div>
    <div class="grid_6">
        <span class="hint">We need to confirm that you are at least 18</span>
        <?php echo form_error('dob'); ?>
    </div>
    <div class="grid_12 align-center padtop">
        <button class="button-ptve" id="join-submit" type="submit">Join</button>
    </div>
</form>
<?php /* content/account_join.php */