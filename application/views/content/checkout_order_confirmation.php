<!-- Begin checkout order confirmation page -->
<p>Thank you for placing your order with <?php echo SITE_NAME; ?></p>
<h2>Delivery</h2>
<p>Your order will be delivered to:<br>
    <?php echo format_address_inline($delivery_address); ?></p>
<h2>Your order items</h2>
<p>Here's the lovely stuff you&apos;ll be receiving:</p>
<div class="grid_8 bold">Item Description</div>
<div class="grid_2 align-center bold">Quantity</div>
<div class="grid_2 align-center bold">Price</div>
<hr>
<?php foreach ($order_lines as $line): ?>
<br class="clear">
<div class="grid_8"><span class="bold"><?php echo $line->name; ?></span></div>
<div class="grid_2 align-center"><?php echo $line->quantity; ?></div>
<div class="grid_2 align-right">
<?php echo format_displayable_price($line->total_amount); ?>
<?php if ($line->quantity > 1): ?>
&nbsp;(<?php echo format_displayable_price($line->unit_price); ?> each)
<?php endif; ?>
</div>
<?php endforeach; ?>
<hr>
<br class="clear">
<div class="grid_10 align-right bold">Subtotal:</div>
<div class="grid_2 align-right"><?php echo format_displayable_price($order->subtotal); ?></div>
<br class="clear">
<div class="grid_10 align-right bold">Delivery:</div>
<div class="grid_2 align-right"><?php echo format_displayable_price($order->delivery_total); ?></div>
<br class="clear">
<div class="grid_10 align-right bold">Total:</div>
<div class="grid_2 align-right"><?php echo format_displayable_price($order->total_amount); ?></div>
<!-- End checkout order confirmation page -->
<?php /* content/checkout_order_confirmation.php */