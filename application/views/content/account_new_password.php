<!-- Begin new password page. -->
<div class="grid_2">&nbsp;</div>
<div class="grid_9">
    <h2>Update Your Password</h2>
    <form action="<?php echo URL_ACCT_UPDATEPW; ?>" method="post" accept-charset="utf-8" autocomplete="off">
        <input name="token" type="hidden" value="<?php echo $token; ?>">
        <div class="grid_6 alpha"><?php echo form_error('token', '<p class="error">', '</p>'); ?></div>
        <div class="grid_3 omega">&nbsp;</div>
        <br class="clear">
        <div class="grid_3 alpha"><label for="password">New Password</label></div>
        <div class="grid_3"><label for="passwordconf">Confirm Password</label></div>
        <div class="grid_3 omega">&nbsp;</div>
        <br class="clear">
        <div class="grid_3 alpha">
            <input id="password" name="password" placeholder="New Password" type="password"><br>
            <?php echo form_error('password'); ?>
        </div>
        <div class="grid_3">
            <input id="passwordconf" name="passwordconf" placeholder="Confirm Password" type="password"><br>
            <?php echo form_error('passwordconf'); ?>
        </div>
        <div class="grid_3 omega"><button class="button-ptve" title="Update Password" type="submit">Update Password</button></div>
    </form>
</div>
<div class="grid_1">&nbsp;</div>
<!-- End new password page. -->
<?php /* content/account_new_password.php */