<!-- Begin FAQ page -->
<nav>
    <ul>
        <li><a href="#faq1">How can I get free delivery?</a></li>
        <li><a href="#faq2">Where do you deliver to?</a></li>
    </ul>
</nav>
<a name="faq1"></a>
<h2>How can I get free delivery?</h2>
<p>We're not currently but we'd love to in the very near future. Any free delivery offers will be announced on our <a href="<?php echo URL_DELIVERY_INFO; ?>" title="Delivery Information">Delivery Information</a> page.</p>
<a name="faq2"></a>
<h2>Where do you deliver to?</h2>
<p>We currently delivery to anywhere in mainland Great Britain. Unfortunately we don't currently cover Northern Ireland but we're hoping to include the whole of the United Kingdom as soon as we can. See our <a href="<?php echo URL_DELIVERY_INFO; ?>" title="Delivery Information">Delivery Information</a> page for more details.</p>
<!-- End FAQ page -->
<?php /* content/faq.php */