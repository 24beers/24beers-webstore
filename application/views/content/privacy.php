<!-- Begin privacy policy page -->
<p><span class="bold"><?php echo BRAND_NAME; ?></span> is serious about our
    commitment to your privacy and we look after any information you give us as
    though it were our own. By visiting
    <span class="bold"><?php echo SITE_NAME; ?></span> you consent for us to use
    your information in accordance with our Privacy Policy.
<h2><?php echo BRAND_NAME; ?> Privacy Policy</h2>
<p><?php echo BRAND_NAME; ?> promises only to use information collected about
    you in accordance with our Privacy Policy below:</p>
<h3>Who We Are</h3>
<p>Any personal information provided to or gathered by <?php echo SITE_NAME; ?>
    is controlled by <?php echo COMPANY_NAME; ?> ("<?php echo BRAND_NAME; ?>", "We", "Us),
    a company registered in England and Wales (Company No. <?php echo COMPANY_NUMBER; ?>),
    with a registered office at <?php echo COMPANY_ADDRESS_INLINE; ?>.</p>
<h3>What Information We Collect</h3>
<p>When you register, place an order or otherwise interact with <?php echo SITE_NAME; ?>
    we may ask you for certain information and you may submit personal data to
    Us. We may also record which products you are interested in and which
    products you purchase as well as customer traffic patterns and site use.</p>
<p>Some examples of how information may be collected by us:</p>
<ul>
    <li>Information You Provide Us: we receive and store information that you
        submit when using our web site or that you provide us in any other way
        (for example by email or telephone). This information may be provided
        when ordering from us (via our web site or by telephone); searching our
        web site; entering competitions; accessing your account; querying order
        status; or by submitting text/photo/video reviews of us and our
        products.</li>
    <li>Information Automatically Provided: we receive, process and store
        certain information whenever you interact with our web site. Like many
        web sites, we use "cookies" (see below) and obtain certain information
        automatically when your Web browser accesses <?php echo SITE_NAME; ?>.
        Information automatically received by us includes the IP address that
        your computer uses to connect to the Internet; your computer, browser,
        operating system and Internet connection details; purchase history;
        clickstream/path analysis of your journey through our web site;
        products you searched for.</li>
    <li>Email Communications: to help us make our email newsletter more useful
        and interesting for our customers we attempt to receive a confirmation
        when you open and click on email newsletters from <?php echo SITE_NAME; ?>
        (if your email software/service supports this option). If you no longer
        wish to receive email newsletters, please notify us using the links
        provided in each newsletter.</li>
</ul>
<h3>How We Use This Information</h3>
<p>Information is kept securely in accordance with our internal security policy
    and may be used to:</p>
<ul>
    <li>Process and deliver your order via the services of our fulfillment
        partner; by placing an order you are giving permission to pass on the
        necessary details to enable fulfillment of your order. In order to
        dispatch your order, we will pass only the necessary data to the
        appropriate carrier for delivery &amp; tracking.</li>
    <li>Provide Customer Support services to you.</li>
    <li>Provide you with an up to date, efficient, and reliable service.</li>
    <li>Help prevent fraud.</li>
    <li>Verify your age.</li>
    <li>Open and run your customer account</li>
    <li>Provide a more personal shopping experience.</li>
    <li>Anonymously provide demographic information to us, such as age bracket,
        general interests, etc., so that we may improve the services and
        marketing we provide.</li>
    <li>Visitors can opt out of Google Analytics for Display Advertising and
        customise Google Display Network ads using the Ads Settings.</li>
</ul>
<p>By making an Order and/or submitting your data to us you agree to this use.</p>
<h3>Data Security</h3>
<p>Your safety is guaranteed because <?php echo SITE_NAME; ?> uses the latest
    security measures to protect your details when you shop with us. Every page
    of the <?php echo SITE_NAME; ?> web site uses 128-bit SSL encryption, your
    password is encrypted with 512-bit encryption and we do not store, or even
    have access to, your credit/debit card information.</p>
<h3>Who Else May See Your Data</h3>
<p>In order to complete your order we may need to disclose some of your
    information, such as delivery address, to our order fulfillment and delivery
    partners and their affiliates.</p>
<h3>Marketing Communications &mdash; <?php echo BRAND_NAME; ?></h3>
<p>We give you the ability to subscribe to our email newsletter on the
     <?php echo SITE_NAME; ?> web site. This subscription list is managed by or
     mailing partner, <?php echo CRM_MAILER_NAME; ?>. We promise not to pass on
     your email address or spam you with emails.</p>
<p>If you no longer wish to receive email newsletters, please notify us using
    the links provided in each newsletter. The link will take you to the
    <?php echo CRM_MAILER_NAME; ?> web site where you can amend your
    subscription preferences.</p>
<h3>Marketing Communications &mdash; 3rd Party</h3>
<p>Although we do not currently have any plans to ever pass your details on to
    3rd parties for the purpose of marketing, we cannot say that this will
    always be the case. <?php echo BRAND_NAME; ?> will never pass your
    information on to third parties unless you give us your consent &mdash;
    either at the time of providing the information to us, or previously if we
    have already received consent from you.</p>
<p>If you have any concerns over receiving unsolicited post you can join the
    Mailing Preference Service and your name should be removed from the lists
    held by responsible direct mail companies.</p>
<h3>Cookies</h3>
<p>In Order to improve our service we may use a device known as a 'cookie'.
    Cookies are a means by which information relating to your Internet activity
    is recorded on your hard drive and used by us to improve the products and
    services available to you.</p>
<p>If you do not wish for us to use cookies when you visit <?php echo SITE_NAME; ?>
    please adjust your Web browser settings to not accept cookies. Please note
    that cookies allow you to take full advantage of the services we provide
    through the <?php echo SITE_NAME; ?> web site and so we recommend that you
    leave them turned on.</p>
<h3>Your Concerns</h3>
<p>If you have any questions or comments about privacy or the <?php echo BRAND_NAME; ?>
    privacy policy, you would like us to delete information that we hold
    about you, or if you think that we have cookies or data about you that you
    do not want us to use or that is incorrect please contact us at the address
    below or email <?php echo EMAIL_CUST_HELP; ?> and the information will be
    corrected as soon as possible or removed from our database as you request.</p>
<p>If you think we have not followed our privacy policy in some way, please
    email us and we can help you resolve your concern.</p>
<h3>Contact Customer Care</h3>
<p>You may resolve any issues that concern your interaction with <?php echo BRAND_NAME; ?> by:</p>
<ul>
    <li>Email to <?php echo EMAIL_CUST_HELP; ?>; or</li>
    <li>Post at <?php echo COMPANY_NAME; ?>, <?php echo COMPANY_ADDRESS_INLINE; ?>; or</li>
    <li>Tweet us <?php echo TWITTER_CUST_HELP; ?> and request us to follow you
        so that you can send us a Direct Message.</li>
</ul>
<!-- End privacy policy page -->
<?php /* content/privacy.php */