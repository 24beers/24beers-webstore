<!-- Begin St. George's Day promotion page -->
<p><span class="bold"><strong>St. George's Day</strong> is a celebration of all
        things <strong>English</strong> and the best way to celebrate is with a
        quality ale.</span><br>
    English brewers from Cumbria to Cornwall are world renowned for brewing fine
    ales. We&apos;ve got a great selection of English beers for you to enjoy
    this <strong>St. George's Day</strong>. Or any other day.</p>
<p>Whenever you buy from us you pay <strong>just &pound;<?php echo STORE_DELIVERY_CHARGE_STANDARD ?> for next working day delivery</strong>.</p>
<h2><a href="<?php echo URL_PRODDETAIL_BEER; ?>/ellenberg-black-ale" title="Buy Ellenberg's Black Ale">
        <strong>Ellenberg's Black Ale</strong> from <strong>Ellenberg's Brewery</strong></a></h2>
<div class="grid_4">
    <a href="<?php echo URL_PRODDETAIL_BEER; ?>/ellenberg-black-ale" title="Buy Ellenberg's Black Ale">
        <img alt="Ellenberg's Black Ale 500ml bottle" height="230" src="<?php echo URL_MEDIA_PRODUCT; ?>/ellenberg-black-ale_small.jpg" width="140"></a>
</div>
<div class="grid_8">
    <p>Although this <strong>black ale</strong> is inspired by the German
        <em>Schwarzbier</em> style it&apos;s a very English beer. The flavour
        has a <em>malty</em> sweetness combined with a subtle
        <em>bitterness</em> and a malty aroma with a hint of
        <em>blackcurrant</em> that adds to the darkness of this ale.</p>
    <p>This beer couldn&apos;t be any more <em>English</em> if it jumped upon a
        noble steed to slay the next dragon it sees.</p>
    <p><a href="<?php echo URL_CATEGORY_STYLE; ?>/black-ale" title="Explore all of our black ales">Explore all of our <strong>Black Ales</strong></a></p>
</div>
<br class="clear">
<h2><a href="<?php echo URL_PRODDETAIL_BEER; ?>/ticketybrew-pale" title="Buy TicketyBrew Pale Ale">
        <strong>TicketyBrew Pale Ale</strong> from <strong>TicketyBrew</strong></a></h2>
<div class="grid_4">
    <a href="<?php echo URL_PRODDETAIL_BEER; ?>/ticketybrew-pale" title="Buy TicketyBrew Pale Ale">
        <img alt="TicketyBrew Pale Ale 330ml bottle" height="230" src="<?php echo URL_MEDIA_PRODUCT; ?>/ticketybrew-pale_small.jpg" width="140"></a>
</div>
<div class="grid_8">
    <p></p>
    <p><a href="<?php echo URL_CATEGORY_STYLE; ?>/pale-ale" title="Explore all of our pale ales">Explore all of our <strong>Pale Ales</strong></a></p>
</div>
<!-- End St. George's Day promotion page -->
<?php /* content/promo_stgeorgesday.php */