<!-- Begin checkout new payment card page -->
<br class="clear">
<form action="<?php echo URL_CHECKOUT_PAYCARD_CREATE; ?>" method="post" autocomplete="off">
    <div class="grid_3"><label for="name">Name on Card</label></div>
    <div class="grid_3 align-right"><input id="name" type="name" name="name" value="<?php echo set_value('name'); ?>" placeholder="Name on Card"></div>
    <div class="grid_6">
        <span class="hint">Name as it appears on the card</span><br>
        <?php echo form_error('name'); ?>
    </div>
    <br class="clear">
    <div class="grid_3"><label for="number">Card Number</label></div>
    <div class="grid_3 align-right"><input id="number" name="number" value="<?php echo set_value('number'); ?>" placeholder="Card Number"></div>
    <div class="grid_6"><?php echo form_error('number'); ?></div>
    <br class="clear">
    <div class="grid_3"><label for="expirymonth">Expiry Date</label></div>
    <div class="grid_3 align-right">
            <input id="expirymonth" name="expirymonth" value="<?php echo set_value('expirymonth'); ?>" size="2" placeholder="MM"> /
            <input id="expiryyear" name="expiryyear" value="<?php echo set_value('expiryyear'); ?>" size="4" placeholder="YYYY">
    </div>
    <div class="grid_6">
        <?php echo form_error('expirymonth'); ?><br>
        <?php echo form_error('expiryyear'); ?>
    </div>
    <br class="clear">
    <div class="grid_3"><label for="cvc">Security Code</label></div>
    <div class="grid_3 align-right"><input id="cvc" type="cvc" name="cvc" value="<?php echo set_value('cvc'); ?>" placeholder="Security Code"></div>
    <div class="grid_6">
        <span class="hint">3 digit code from the back of the card</span><br>
        <?php echo form_error('email'); ?></div>
    <br class="clear">
    <div class="grid_6 align-right"><button id="join-submit" type="submit">Join</button></div>
</form>
<!-- End checkout new payment card page -->
<?php /* content/checkout_new_payment_card.php */