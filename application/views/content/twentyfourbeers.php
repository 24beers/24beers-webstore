<p>Twenty Four Beers is the core idea behind <?php echo SITE_NAME; ?>. The idea being that we source the best real ale and craft beer from
    independent British breweries and condense this into twenty four of, what we consider, the finest beers.</p>
<p>We decide what beers go into the Twenty Four Beers selection. Our decision is based on our taste buds, simple as that. If we really like a
    beer and we think you&apos;ll like it too, we add it to the selection. When a beer goes out of stock we replace it with something fresh.<br>
    We try to ensure that the Twenty Four Beers selection always has a broad range of beer styles so, no matter what your taste, we&apos;ve got
    a beer you&apos;ll love.</p>
<p>Of course, this isn&apos;t just about us. We want you to be involved too. You can let us know what beer you&apos;d like us to stock and
    we&apos;ll take that on board. Although we try our best to know every brewery, there are so many and some are so local, we really appreciate
    your help in introducing us to new breweries.</p>
<a href="/" title="Browse and buy from our selection of Twenty Four Beers">Browse the Twenty Four Beers &rarr;</a>
<?php /* content/twentyfourbeers.php */