<!-- Begin brewery page -->
<h1>About Our Breweries</h1>
<?php foreach ($brewery_list as $brewery): ?>
<h2><a href="<?php echo URL_BREWERY . "/$brewery->code"; ?>" title="View <?php echo $brewery->name; ?>"><?php echo $brewery->name; ?></a></h2>
<div class="grid_8">
    <p><?php echo $brewery->description; ?></p>
</div>
<div class="grid_4">&nbsp;</div>
<br class="clear">
<?php endforeach; ?>
<!-- End brewery page -->
<?php /* application/views/content/brewery_page.php */