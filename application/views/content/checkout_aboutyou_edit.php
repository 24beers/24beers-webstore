<h2>About You</h2>
<?php echo validation_errors(); ?>
<form action="<?php echo URL_CHECKOUT_ABOUTYOU_UPDATE; ?>" method="post" autocomplete="on">
    <div class="grid_3"><label for="name">Name</label></div>
    <div class="grid_3">
        <input id="name" name="name" placeholder="Name" tabindex="1" value="<?php echo $customer->name; ?>" x-autocompletetype="name"><br>
        <?php echo form_error('name'); ?>
    </div>
    <div class="grid_3"><label for="main_phone">Main Phone</label></div>
    <div class="grid_3">
        <input id="main_phone" name="main_phone" placeholder="Main Phone" tabindex="3" type="tel" value="<?php echo $customer->main_phone; ?>"  x-autocompletetype="tel-national"><br>
        <?php echo form_error('main_phone'); ?>
    </div>
    <div class="grid_3"><label for="email">Email</label></div>
    <div class="grid_3">
        <input id="email" name="email" placeholder="Email" tabindex="2" type="email" value="<?php echo $customer->email; ?>" x-autocompletetype="email"><br>
        <?php echo form_error('email'); ?>
    </div>
    <div class="grid_3"><label for="alternative_phone">Alternative Phone</label></div>
    <div class="grid_3">
        <input id="alternative_phone" name="alternative_phone" placeholder="Alternative Phone" tabindex="4" type="tel" value="<?php echo $customer->alternative_phone; ?>" x-autocompletetype="tel-national"><br>
        <?php echo form_error('alternative_phone'); ?>
    </div>
    <br class="clear">
    <div class="grid_3"><label for="dob">Date of Birth</label></div>
    <div class="grid_3">
        <input id="dob" maxlength="10" name="dob" placeholder="<?php echo FORMAT_DATE_INPUT; ?>" tabindex="3" type="date" value="<?php echo $customer->date_of_birth; ?>" x-autocompletetype="birthday"><br>
        <?php echo form_error('dob'); ?>
    </div>
    <br class="clear">
    <div class="grid_12 align-center padtop">
        <button class="button-ptve" title="Save Changes" type="submit">Save Changes</button>
    </div>
</form>
<?php /* content/checkout_aboutyou_edit.php */