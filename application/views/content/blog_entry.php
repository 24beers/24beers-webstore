<!-- Begin blog post -->
<div class="grid_8 blog-post">
    <article>
        <header>
            <div class="grid_8 alpha omega">
                <h1><?php echo $blog->title; ?></h1>
            </div>
            <div class="grid_2 alpha">
                <div>
                    <a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php echo $blog->title; ?>" data-via="24BeersUK" data-hashtags="24beers">Tweet</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                </div>
            </div>
            <div class="grid_2">
                <div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div><br>
            </div>
            <div class="grid_2 omega">
                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
            </div>
            <br class="clear">
            <div class="grid_4 alpha">Author: <a href="https://plus.google.com/110947345552996147084?rel=author"><?php echo $blog->author; ?></a></div>
            <div class="grid_4 omega">Published: <time><?php echo $blog->date; ?></time></div>
        </header>
        <div class="grid_8 alpha omega">
            <?php echo $blog->text; ?>
        </div>
        <footer>
            <div class="grid_8 alpha omega">
                <p><a href="/blog" title="24 Beers Blog">&laquo; Back to blog list</a></p>
            </div>
            <div class="grid_8 alpha omega blog-comments">
                <h2>Comments</h2>
                <div id="disqus_thread"></div>
            </div>
        </footer>
    </article>
</div>
<!-- end .grid_8 -->
<div class="grid_4" id="blognav">
    <?php require_once APPPATH . 'views/include/blog_nav.php'; ?>
</div>
<!-- end .grid_4 -->
<script type="text/javascript">
    var disqus_shortname = '24beers';
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<?php require_once APPPATH . 'views/include/gplus1.php'; ?>
<!-- End blog entry -->
<?php /* Location: application/views/content/blog_entry.php */