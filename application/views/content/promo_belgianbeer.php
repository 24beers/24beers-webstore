<section class="intro-text">
    <p class="bold">Some of the finest beer in the world comes from <em>Belgium</em> and there&apos;s a huge choice to buy Belgian beer online.
        We&apos;ve chosen what we think are the best example of Belgian-style beers from British brewers.</p>
    <p>British brewers have drawn inspiration from the earliest Belgian ale. Through the <em>Trappist</em> style of <em>dark</em>, <em>sweet</em>
        beers our brewers have added their own twist. Breweries such as
        <a href="<?php echo URL_BREWERY ?>/savour-beer" title="Beer from Savour Beer">Savour Beer</a>
        and <a href="<?php echo URL_BREWERY ?>/ticketybrew" title="Beer from TicketyBrew">TicketyBrew</a> create exciting <em>saison</em>,
        <em>blonde</em> and <em>dubbel</em> styles.</p>
    <p>Our brewers have brewed the beers here with the same passion for <em>quality</em> as the Belgian styles that inspire them.</p>
    <p>Whenever you buy from us you pay <strong>just &pound;<?php echo STORE_DELIVERY_CHARGE_STANDARD ?> for next working day delivery</strong>.</p>
</section>
<h2><a href="<?php echo URL_PRODDETAIL_BEER; ?>/ticketybrew-dubbel" title="Buy TicketyBrew Dubbel online">
        <strong>TicketyBrew Dubbel</strong> from <strong>TicketyBrew</strong></a></h2>
<div class="grid_4">
    <a href="<?php echo URL_PRODDETAIL_BEER; ?>/ticketybrew-dubbel" title="Buy TicketyBrew Dubbel online">
        <img alt="TicketyBrew Dubbel 330ml bottle" height="230" src="<?php echo URL_MEDIA_PRODUCT; ?>/ticketybrew-dubbel_small.jpg" width="140"></a>
</div>
<div class="grid_8">
    <p><em>Dubbel</em> is a classic <em>Trappist</em> style.
        <strong>TicketyBrew</strong> have taken this classic and redubbed it
        with extra-dark <em>candi syrup</em>, easy on the hops and a yeast punch
        that may at first sound imposing but goes down as easily as the
        <em>Trappist</em> original.</p>
    <p><a href="<?php echo URL_PRODDETAIL_BEER; ?>/ticketybrew-dubbel" title="Buy TicketyBrew Dubbel online">Buy <strong>TicketyBrew Dubbel</strong> &rarr;</a></p>
    <p><a href="<?php echo URL_BREWERY; ?>/ticketybrew" title="Explore all TicketyBrew beers">Explore all <strong>TicketyBrew</strong> beers &rarr;</a></p>
</div>
<br class="clear">
<h2><a href="<?php echo URL_PRODDETAIL_BEER; ?>/savour-finesse" title="Buy Savour Finesse Saison online">
        <strong>Finesse Saison</strong> from <strong>Savour Beer</strong></a></h2>
<div class="grid_4">
    <a href="<?php echo URL_PRODDETAIL_BEER; ?>/savour-finesse" title="Buy Savour Finesse Saison online">
        <img alt="Savour Finesse Saison 330ml bottle" height="230" src="<?php echo URL_MEDIA_PRODUCT; ?>/savour-finesse_small.jpg" width="140"></a>
</div>
<div class="grid_8">
    <p>Originally brewed as a seasonal ale in the farmhouses of Wallonia,
        <em>saison</em> style beers have been seeing a resurgence in the UK
        recently after once being described as an &ldquo;endangered&rdquo;
        style. Never one to believe the doom-mongers, Sandy from
        <strong>Savour Beer</strong> has taken this farm-worker&apos;s ale and
        given it delicacy and subtlety by adding <em>lemongrass</em> and floral
        citrusy <em>Crystal hops</em>. <strong>Finesse</strong> is slightly dry
        and effervescent with <em>Champagne yeast</em>.</p>
    <p><a href="<?php echo URL_PRODDETAIL_BEER; ?>/savour-finesse" title="Buy Savour Finesse Saison online">Buy <strong>Savour Finesse Saison</strong> &rarr;</a></p>
    <p><a href="<?php echo URL_BREWERY; ?>/savour-beer" title="Explore all beers from Savour Beer">Explore all beers from <strong>Savour Beer</strong> &rarr;</a></p>
</div>
<br class="clear">
<h2><a href="<?php echo URL_PRODDETAIL_BEER; ?>/savour-progress" title="Buy Savour Progress Blond online">
        <strong>Progress Blond</strong> from <strong>Savour Beer</strong></a></h2>
<div class="grid_4">
    <a href="<?php echo URL_PRODDETAIL_BEER; ?>/savour-progress" title="Buy Savour Progress Blond online">
        <img alt="Savour Progress Blond 330ml bottle" height="230" src="<?php echo URL_MEDIA_PRODUCT; ?>/savour-progress_small.jpg" width="140"></a>
</div>
<div class="grid_8">
    <p>In Belgium a <em>blonde</em> is almost the archetypal ale. Think Belgian
        beer and your mind may come to rest on <em>Pilsner malt</em> and
        <em>Styrian Goldings hops</em>, key ingredients to
        <strong>Progress Blond</strong> that make it almost a British definition
        of the <em>Belgian-style</em> beer.</p>
    <p><a href="<?php echo URL_PRODDETAIL_BEER; ?>/savour-progress" title="Buy Savour Progress Blond online">Buy <strong>Savour Progress Blond</strong> &rarr;</a></p>
    <p><a href="<?php echo URL_BREWERY; ?>/savour-beer" title="Explore all beers from Savour Beer">Explore all beers from <strong>Savour Beer</strong> &rarr;</a></p>
</div>
<br class="clear">
<h2><a href="<?php echo URL_PRODDETAIL_BEER; ?>/rock-saison" title="Buy Kubla Rock Saison online">
        <strong>Rock Saison</strong> from <strong>Kubla Brewery</strong></a></h2>
<div class="grid_4">
    <a href="<?php echo URL_PRODDETAIL_BEER; ?>/rock-saison" title="Buy Kubla Rock Saison online">
        <img alt="Kubla Rock Saison 500ml bottle" height="230" src="<?php echo URL_MEDIA_PRODUCT; ?>/rock-saison_small.jpg" width="140"></a>
</div>
<div class="grid_8">
    <p>Another <em>saison</em> but this time from the rugged landscape of
        Exmoor, <strong>Rock Saison</strong> is a refreshing floral brew with
        <em>lavender</em> and <em>chamomile</em>. The <em>spicy</em>
        <em>woody</em> hops give this ale a mature flavour that stands apart
        from traditional <em>saison</em>.</p>
    <p><a href="<?php echo URL_PRODDETAIL_BEER; ?>/rock-saison" title="Buy Kubla Rock Saison online">Buy <strong>Kubla Rock Saison</strong> &rarr;</a></p>
    <p><a href="<?php echo URL_BREWERY; ?>/kubla-brewery" title="Explore all Kubla Brewery beers">Explore all <strong>Kubla Brewery</strong> beers &rarr;</a></p>
</div>
<br class="clear">
<h2><a href="<?php echo URL_PRODDETAIL_BEER; ?>/ticketybrew-blonde" title="Buy TicketyBrew Blonde online">
        <strong>TicketyBrew Blonde</strong> from <strong>TicketyBrew</strong></a></h2>
<div class="grid_4">
    <a href="<?php echo URL_PRODDETAIL_BEER; ?>/ticketybrew-blonde" title="Buy TicketyBrew Blonde online">
        <img alt="TicketyBrew Blonde 330ml bottle" height="230" src="<?php echo URL_MEDIA_PRODUCT; ?>/ticketybrew-blonde_small.jpg" width="140"></a>
</div>
<div class="grid_8">
    <p>In the way that <strong>Duvel</strong> could be considered the archetypal
        <em>Belgian</em> beer <strong>TicketyBrew Blonde</strong> is its
        <em>spicy</em> British counterpart thanks to the underlying
        <em>ginger</em> flavour from the yeast.</p>
    <p><strong>TicketyBrew Blonde</strong> has a light <em>floral</em> and
        <em>peach</em> aroma that makes this perfect for summer and to reminisce
        of taking a break from working on the farm under the heat of the
        Wallonian sun.</p>
    <p><a href="<?php echo URL_PRODDETAIL_BEER; ?>/ticketybrew-blonde" title="Buy TicketyBrew Blonde online">Buy <strong>TicketyBrew Blonde</strong> &rarr;</a></p>
    <p><a href="<?php echo URL_CATEGORY_STYLE; ?>/blond-ale" title="Explore all Blonde Ales">Explore all <strong>Blonde Ales</strong> &rarr;</a></p>
</div>
<!-- End Belgian Beer promotion page -->
<?php /* content/promo_belgianbeer.php */