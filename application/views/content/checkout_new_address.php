<!-- Begin checkout new address page -->
<br class="clear">
<form action="<?php echo URL_CHECKOUT_ADDRESS_CREATE; ?>" method="post" autocomplete="on">
    <div class="grid_3"><label for="name">Addressee</label></div>
    <div class="grid_3 align-right"><input id="name" name="name" value="<?php echo set_value('name'); ?>" placeholder="Addressee" x-autocompletetype="name"></div>
    <div class="grid_6"><?php echo form_error('name'); ?></div>
    <br class="clear">
    <div class="grid_3"><label for="house">Flat/House Name or Number</label></div>
    <div class="grid_3 align-right"><input id="house" name="house" value="<?php echo set_value('house'); ?>" placeholder="Flat/House Name or Number" x-autocompletetype="address-line1"></div>
    <div class="grid_6"><?php echo form_error('house'); ?></div>
    <br class="clear">
    <div class="grid_3"><label for="street">Street</label></div>
    <div class="grid_3 align-right"><input id="street" name="street" value="<?php echo set_value('street'); ?>" placeholder="Street" x-autocompletetype="address-line2"></div>
    <div class="grid_6"><?php echo form_error('street'); ?></div>
    <br class="clear">
    <div class="grid_3"><label for="town">Town</label></div>
    <div class="grid_3 align-right"><input id="town" name="town" value="<?php echo set_value('town'); ?>" placeholder="Town" x-autocompletetype="locality"></div>
    <div class="grid_6"><?php echo form_error('town'); ?></div>
    <br class="clear">
    <div class="grid_3"><label for="city">City/County</label></div>
    <div class="grid_3 align-right"><input id="city" name="city" value="<?php echo set_value('city'); ?>" placeholder="City/County" x-autocompletetype="administrative-area"></div>
    <div class="grid_6"><?php echo form_error('city'); ?></div>
    <br class="clear">
    <div class="grid_3"><label for="country">Country</label></div>
    <div class="grid_3 align-right"><input class="bold" disabled="disabled" value="<?php echo COUNTRY_UK; ?>"></div>
    <br class="clear">
    <div class="grid_3"><label for="postcode">Postcode</label></div>
    <div class="grid_3 align-right"><input id="postcode" name="postcode" value="<?php echo set_value('postcode'); ?>" placeholder="Postcode" x-autocompletetype="postal-code"></div>
    <div class="grid_6"><?php echo form_error('postcode'); ?></div>
    <br class="clear">
    <div class="grid_6 align-right">
        <button class="button-ptve" type="submit">Continue</button>
    </div>
</form>
<!-- End checkout new address page -->
<?php /* Location: application/views/content/checkout_new_address.php */