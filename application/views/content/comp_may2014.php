<style scoped>
    .comp-sprite {
        background-color: transparent;
        background-image: url(/media/competition-may-2014-assets.png);
        background-repeat: no-repeat;
        display: inline-block;
        overflow: hidden;
        text-indent: -1000%;
    }
    .comp-sprite.leader {
        background-position: 0 0;
        height: 290px;
        width: 940px;
    }
    .comp-sprite.logo {
        background-position: 0 -291px;
        height: 263px;
        width: 300px;
    }
    .comp-sprite.starbeer {
        background-position: -300px -291px;
        height: 263px;
        width: 300px;
    }
    .comp-sprite.featuredbrewery {
        background-position: -600px -291px;
        height: 263px;
        width: 300px;
    }
    .comp-bigtext {
        color: #2185c5;
        font-family: 'Roboto Slab', serif;
        font-size: 3em;
        text-align: center;
    }
    .comp-midtext {
        font-size: 2em;
    }
    .comp-smalltext {
        font-size: 0.9em;
    }
</style>
<div class="grid_12">
    <div class="comp-sprite leader"><h1>Win 12 bottles of real ale this May with our free beer giveaway!*</h1></div>
</div>
<div class="clear"></div>
<div class="grid_8">
    <h2 class="comp-bigtext">Do you like free beer?</h2>
    <p class="comp-midtext">Daft question, of course you do.</p>
    <p>We are giving away 12 bottles of real ale to 5 lucky winners. Place an order with us in May and we will send you a question
        in your confirmation email. It&apos;ll be a toughie but I know you&apos;'re up to the challenge.
        Answer the question correctly and you will be entered in to the prize draw.</p>
    <p>All you have to do is put your feet up with one of your beers and wait to see if you have won another 12 bottles!</p>
    <p>We will email the winners individually and announce them on our
        <a href="<?php echo BRAND_URL_TWITTER; ?>" title="Follow us on Twitter">Twitter</a> and
        <a href="<?php echo BRAND_URL_FACEBOOK; ?>" title="Like us on Facebook">Facebook</a> feeds.</p>
    <p>Follow the links below for some beer buying inspiration...</p>
</div>
<div class="grid_4">
    <h2>* Rules</h2>
    <p class="comp-smalltext">Place an order with us between 00:00:00 BST on 1<sup>st</sup> May &amp; 23:59:59 BST on 31<sup>st</sup> May 2014.
        Your confirmation email will contain a question. Reply to the email with the correct answer by 23:59:59 BST on 1<sup>st</sup> June 2014 to be entered in to the prize draw.</p>
    <p class="comp-smalltext">Five winners will be drawn at random from the correct answers we receive by 23:59:59 BST on 1<sup>st</sup> June 2014.</p>
    <p class="comp-smalltext">You must be aged 18 or over to place an order with <?php echo SITE_NAME; ?> or to enter this competition.</p>
    <p class="comp-smalltext">The judge's (my) decision is final. No alternative prizes will be given.</p>
</div>
<div class="clear"></div>
<div class="grid_4">
    <a class="comp-sprite logo" href="<?php echo URL_HOME; ?>" title="Buy craft beer and real ale from our online selection">Enter our competition for free beer &mdash just <em>buy craft beer and real ale</em> from our online selection</a>
</div>
<div class="grid_4">
    <a class="comp-sprite starbeer" href="<?php echo URL_PRODDETAIL_BEER; ?>/savour-progress" title="Click to buy our star beer &mdash; Savour Progress">Buy our star beer, <em>Savour Progress</em>, and enter our competition to win free beer</a>
</div>
<div class="grid_4">
    <a class="comp-sprite featuredbrewery" href="<?php echo URL_BREWERY; ?>/ellenbergs-brewery" title="Click here to buy beer from Ellenberg&apos;s Brewery">Buy beer from Ellenberg&apos;s Brewery, <em>TicketyBrew</em></a>
</div>
<?php /* content/promo_compmay2014.php */