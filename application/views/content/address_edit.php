<!-- Begin new address page -->
<br class="clear">
<?php if (isset($address)): ?>
<form action="<?php echo URL_ACCT_ADDRESS_UPDATE; ?>" method="post" autocomplete="on">
    <input type="hidden" name="code" value="<?php echo $address->code; ?>">
    <div class="grid_3"><label for="name">Addressee</label></div>
    <div class="grid_3 align-right"><input id="name" name="name" value="<?php echo $address->name; ?>" placeholder="Addressee" x-autocompletetype="name"></div>
    <div class="grid_6"><?php echo form_error('name'); ?></div>
    <br class="clear">
    <div class="grid_3"><label for="house">Flat/House Name or Number</label></div>
    <div class="grid_3 align-right"><input id="house" name="house" value="<?php echo $address->house; ?>" placeholder="Flat/House Name or Number" x-autocompletetype="address-line1"></div>
    <div class="grid_6"><?php echo form_error('house'); ?></div>
    <br class="clear">
    <div class="grid_3"><label for="street">Street</label></div>
    <div class="grid_3 align-right"><input id="street" name="street" value="<?php echo $address->street; ?>" placeholder="Street" x-autocompletetype="address-line2"></div>
    <div class="grid_6"><?php echo form_error('street'); ?></div>
    <br class="clear">
    <div class="grid_3"><label for="town">Town</label></div>
    <div class="grid_3 align-right"><input id="town" name="town" value="<?php echo $address->town; ?>" placeholder="Town" x-autocompletetype="locality"></div>
    <div class="grid_6"><?php echo form_error('town'); ?></div>
    <br class="clear">
    <div class="grid_3"><label for="city">City/County</label></div>
    <div class="grid_3 align-right"><input id="city" name="city" value="<?php echo $address->city; ?>" placeholder="City/County" x-autocompletetype="administrative-area"></div>
    <div class="grid_6"><?php echo form_error('city'); ?></div>
    <br class="clear">
    <div class="grid_3"><label for="country">Country</label></div>
    <div class="grid_3 align-right"><input class="bold" disabled="disabled" value="<?php echo COUNTRY_UK; ?>"></div>
    <br class="clear">
    <div class="grid_3"><label for="postcode">Postcode</label></div>
    <div class="grid_3 align-right"><input id="postcode" name="postcode" value="<?php echo $address->postcode; ?>" placeholder="Postcode" x-autocompletetype="postal-code"></div>
    <div class="grid_6"><?php echo form_error('postcode'); ?></div>
    <br class="clear">
    <div class="grid_6 align-right">
        <button type="submit">Save</button>
    </div>
</form>
<?php else: ?>
<div class="error align-center"><?php echo ERROR_ADDRESS_NOTFOUND; ?></div>
<?php endif; ?>
<!-- End new address page -->
<?php /* content/address_new_page.php */