<h1>Forgot your password?</h1>
<p>We have received a request to reset the password for this email address. If
    the request did not come from you please report this to
    <a href="<?php echo EMAIL_CUST_HELP; ?>"><?php echo EMAIL_CUST_HELP; ?></a></p>
<p>Please follow this link to update your password:
    <a href="<?php echo $link; ?>"><?php echo $link; ?></a></p>
<p>For your security, this link is only valid for the next 10 minutes.</p>
<p>Cheers, <?php echo NAME_CUST_HELP; ?></p>
<?php /* application/views/content/account_detail.php */