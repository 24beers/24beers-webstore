<form id="formSubmitOrder" action="<?php echo URL_CHECKOUT_SUBMIT_ORDER; ?>" method="post">
    <input id="inpStripeToken" name="stripeToken" type="hidden" value="<?php echo set_value('stripeToken'); ?>">
    <h2>About You</h2>
    <div class="grid_3">Name</div>
    <div class="grid_3"><span class="bold"><?php echo $customer->name; ?></span></div>
    <div class="grid_3">Main Phone</div>
    <div class="grid_3"><span class="bold"><?php echo $customer->main_phone; ?></span></div>
    <div class="grid_3">Email Address</div>
    <div class="grid_3"><span class="bold"><?php echo $customer->email; ?></span></div>
    <div class="grid_3">Alternative Phone</div>
    <div class="grid_3"><span class="bold"><?php echo $customer->alternative_phone; ?></span></div>
    <div class="grid_12 padtop">
        <a href="<?php echo URL_CHECKOUT_ABOUTYOU_EDIT; ?>" title="Change Your Details">Change Your Details</a>
    </div>
    <h2>Delivery</h2>
    <div class="grid_10"><h3>Address</h3></div>
    <div class="grid_2 bold align-center">Deliver Here</div>
    <?php
        $i = 0;
        $submitted_address_code = set_value('address_code');
    ?>
    <?php foreach ($customer->addresses as $address): ?>
    <div class="grid_12 address">
        <div class="grid_10 alpha"><?php echo format_address_inline($address); ?></div>
        <div class="grid_2 omega align-center">
            <input
                <?php if ((empty($submitted_address_code) && $i === 0) || ($submitted_address_code === $address->code)): ?>
                checked="checked"
                <?php endif; ?>
                name="address_code" type="radio" value="<?php echo $address->code; ?>">
        </div>
    </div>
    <?php $i++; ?>
    <?php endforeach; ?>
    <?php echo form_error('address_code', '<div class="error">', '</div>'); ?>
    <div class="grid_12 padtop">
        <a href="<?php echo URL_CHECKOUT_ADDRESSBOOK; ?>" title="Edit/Add Addresses">Edit/Add Addresses</a>
    </div>
    <h2>Payment</h2>
    <div class="error" id="msgJsErr">
        To confirm your order you must have JavaScript enabled.
        This is so that we can securely transmit your payment details to our payment provider.
    </div>
    <div class="error" id="msgPayErr">
        <?php if (isset($payment_error)): ?>
        <?php echo $payment_error; ?>
        <?php endif; ?>
    </div>
    <p class="align-center bold">Your payment card will be charged <?php echo format_displayable_price($basket->total); ?></p>
    <div class="grid_3"><label for="number">Card Number</label></div>
    <div class="grid_3 align-right"><input id="number" placeholder="Card Number" class="cardnum"></div>
    <div class="clear"></div>
    <div class="grid_3"><label for="expirymonth">Expiry Date</label></div>
    <div class="grid_3 align-right">
        <input class="cardexp_m" id="expirymonth" maxlength="2" placeholder="MM"> /
        <input class="cardexp_y" maxlength="4" placeholder="YYYY">
    </div>
    <div class="clear"></div>
    <div class="grid_3"><label for="cvc">Security Code</label></div>
    <div class="grid_3 align-right"><input class="cardcvc" id="cvc" maxlength="3" placeholder="Code" type="cvc"></div>
    <div class="grid_6"><span class="hint">3 digit code from the back of the card</span></div>
    <div class="grid_12 align-center padtop">
        <button class="button-ptve" id="btnSubmitOrder" type="submit">Confirm Order</button>
    </div>
</form>
<script src="//js.stripe.com/v2/"></script>
<script defer>Stripe.setPublishableKey('<?php echo $payment_key; ?>');</script>
<?php /* content/checkout_final.php */