<section>
    <h2>About You</h2>
    <div class="grid_3">Name</div>
    <div class="grid_3"><span class="bold"><?php echo $customer->name; ?></span></div>
    <div class="grid_3">Main Phone</div>
    <div class="grid_3"><span class="bold"><?php echo $customer->main_phone; ?></span></div>
    <div class="clear"></div>
    <div class="grid_3">Email</div>
    <div class="grid_3"><span class="bold"><?php echo $customer->email; ?></span></div>
    <div class="grid_3">Alternative Phone</div>
    <div class="grid_3"><span class="bold"><?php echo $customer->alternative_phone; ?></span></div>
    <div class="clear"></div>
    <div class="grid_3">Date of Birth</div>
    <div class="grid_3"><span class="bold"><?php echo $customer->date_of_birth; ?></span></div>
    <div class="grid_3">&nbsp;</div>
    <div class="grid_3">&nbsp;</div>
    <div class="clear"></div>
    <div class="padtop">
        <a href="<?php echo URL_ACCT_DETAIL; ?>" title="Edit Details">Edit Details</a>
    </div>
</section>
<section>
    <h2>Addresses</h2>
    <?php foreach ($customer->addresses as $address): ?>
    <div class="address">
        <div class="grid_8"><?php echo format_address_inline($address); ?></div>
        <div class="grid_4 align-right">
            <a href="<?php echo URL_ACCT_ADDRESS_DELETE . "/$address->code"; ?>" class="button">Delete</a>
        </div>
        <div class="clear"></div>
    </div>
    <?php endforeach; ?>
    <div class="padtop">
        <a href="<?php echo URL_ACCT_ADDRESS_ADD; ?>" class="button">+ Add Address</a>
    </div>
</section>
<?php if (!empty($customer->credit_account)): ?>
<section>
    <h2>Account Credit</h2>
</section>
<?php endif; ?>
<section>
    <h2>Recent Orders</h2>
    <?php if (isset($customer->orders) && count($customer->orders) > 0): ?>
    <table>
        <thead>
            <tr>
                <th>Description</th>
                <th class="align-center">Number of items</th>
                <th class="align-right">Order Total</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($customer->orders as $order): ?>
            <tr>
                <td><?php echo $order->name; ?></td>
                <td class="align-center"><?php echo $order->num_items; ?> items</td>
                <td class="align-right"><?php echo format_displayable_price($order->total_amount); ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else: ?>
    <div class="message">You haven&apos;t placed any orders yet</div>
    <?php endif; ?>
</section>
<?php /* content/account_home.php */