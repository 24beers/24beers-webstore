<!-- Begin account detail page -->
<div class="grid_1">&nbsp;</div>
<div class="grid_10">
    <h2 class="align-center">About You</h2>
    <?php echo validation_errors(); ?>
    <form action="<?php echo URL_ACCT_DETAIL_UPDATE; ?>" method="post" autocomplete="on">
        <div class="grid_2 alpha"><label for="name">Name</label></div>
        <div class="grid_3">
            <input id="name" name="name" placeholder="Name" tabindex="1" value="<?php echo $customer->name; ?>" x-autocompletetype="name"><br>
            <?php echo form_error('name'); ?>
        </div>
        <div class="grid_2"><label for="main_phone">Main Phone</label></div>
        <div class="grid_3 omega">
            <input id="main_phone" name="main_phone" placeholder="Main Phone" tabindex="4" type="tel" value="<?php echo $customer->main_phone; ?>"  x-autocompletetype="tel-national"><br>
            <?php echo form_error('main_phone'); ?>
        </div>
        <br class="clear">
        <div class="grid_2 alpha"><label for="email">Email</label></div>
        <div class="grid_3">
            <input id="email" name="email" placeholder="Email" tabindex="2" type="email" value="<?php echo $customer->email; ?>" x-autocompletetype="email"><br>
            <?php echo form_error('email'); ?>
        </div>
        <div class="grid_2"><label for="alternative_phone">Alternative Phone</label></div>
        <div class="grid_3 omega">
            <input id="alternative_phone" name="alternative_phone" placeholder="Alternative Phone" tabindex="5" type="tel" value="<?php echo $customer->alternative_phone; ?>" x-autocompletetype="tel-national"><br>
            <?php echo form_error('alternative_phone'); ?>
        </div>
        <br class="clear">
        <div class="grid_2 alpha"><label for="dob">Date of Birth</label></div>
        <div class="grid_3">
            <input id="dob" maxlength="10" name="dob" placeholder="<?php echo FORMAT_DATE_INPUT; ?>" tabindex="3" type="date" value="<?php echo $customer->date_of_birth; ?>" x-autocompletetype="birthday"><br>
            <?php echo form_error('dob'); ?>
        </div>
        <div class="grid_5 omega">&nbsp;</div>
        <br class="clear">
        <div class="grid_10 alpha omega align-center padtop">
            <button class="button-ptve" title="Save Changes" type="submit">Save Changes</button>
        </div>
    </form>
</div>
<div class="grid_1">&nbsp;</div>
<br class="clear">
<!-- End account detail page -->
<?php /* content/account_detail.php */