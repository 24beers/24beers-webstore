<form action="<?php echo URL_CHECKOUT_ADDRESS_CREATE; ?>" method="post" autocomplete="on">
    <div class="grid_3"><label for="name">Addressee</label></div>
    <div class="grid_3"><input id="name" name="name" value="<?php echo set_value('name'); ?>" placeholder="Addressee" x-autocompletetype="name"></div>
    <div class="grid_6"><?php echo form_error('name'); ?></div>
    <div class="clear"></div>
    <div class="grid_3"><label for="house">Flat/House Name or Number</label></div>
    <div class="grid_3"><input id="house" name="house" value="<?php echo set_value('house'); ?>" placeholder="Name or Number" x-autocompletetype="address-line1"></div>
    <div class="grid_6"><?php echo form_error('house'); ?></div>
    <div class="clear"></div>
    <div class="grid_3"><label for="street">Street</label></div>
    <div class="grid_3"><input id="street" name="street" value="<?php echo set_value('street'); ?>" placeholder="Street" x-autocompletetype="address-line2"></div>
    <div class="grid_6"><?php echo form_error('street'); ?></div>
    <div class="clear"></div>
    <div class="grid_3"><label for="town">Town</label></div>
    <div class="grid_3"><input id="town" name="town" value="<?php echo set_value('town'); ?>" placeholder="Town" x-autocompletetype="locality"></div>
    <div class="grid_6"><?php echo form_error('town'); ?></div>
    <div class="clear"></div>
    <div class="grid_3"><label for="city">City/County</label></div>
    <div class="grid_3"><input id="city" name="city" value="<?php echo set_value('city'); ?>" placeholder="City/County" x-autocompletetype="administrative-area"></div>
    <div class="grid_6"><?php echo form_error('city'); ?></div>
    <div class="clear"></div>
    <div class="grid_3"><label for="country">Country</label></div>
    <div class="grid_3"><input class="bold" disabled="disabled" value="<?php echo COUNTRY_UK; ?>"></div>
    <div class="clear"></div>
    <div class="grid_3"><label for="postcode">Postcode</label></div>
    <div class="grid_3"><input id="postcode" name="postcode" value="<?php echo set_value('postcode'); ?>" placeholder="Postcode" x-autocompletetype="postal-code"></div>
    <div class="grid_6"><?php echo form_error('postcode'); ?></div>
    <div class="clear"></div>
    <div class="grid_12 align-center padtop">
        <button class="button-ptve" title="Add Address" type="submit">Add Address</button>
    </div>
</form>
<?php /* application/views/content/checkout_address_add.php */