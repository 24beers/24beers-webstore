<!-- Begin brewery page -->
<section class="intro-text">
    <h1><?php echo $brewery->name; ?></h1>
    <?php if (!empty($brewery->text)): ?>
    <p><?php echo $brewery->text; ?></p>
    <?php endif; ?>
</section>
<?php echo $product_grid; ?>
<!-- End brewery page -->
<?php /* application/views/content/brewery_page.php */