<div class="grid_6 acct-new">
    <h2>New to <?php echo BRAND_NAME ?>?</h2>
    <p class="align-center"><a href="<?php echo URL_ACCT_JOIN; ?>" class="button">Continue &rarr;</a></p>
</div>
<div class="grid_6 acct-signin">
    <h2>Sign In</h2>
    <form action="<?php echo URL_ACCT_AUTH; ?>" method="post" accept-charset="utf-8" autocomplete="on">
        <?php if (is_basket_complete()): ?>
        <input name="<?php echo PARAM_REDIRECT; ?>" type="hidden" value="<?php echo URL_CHECKOUT; ?>">
        <?php endif; ?>
        <div class="grid_3 alpha">
            <label for="email">Email</label>
        </div>
        <div class="grid_3 omega">
            <input id="email" type="email" name="email" value="<?php echo set_value('email'); ?>" placeholder="Email" x-autocompletetype="email">
        </div>
        <div class="grid_6 alpha omega">
            <?php echo form_error('email'); ?>
        </div>
        <div class="grid_3 alpha">
            <label for="password">Password</label>
        </div>
        <div class="grid_3 omega">
            <input id="password" type="password" name="password" placeholder="Password" autocomplete="off">
        </div>
        <div class="grid_6 alpha omega">
            <?php echo form_error('password'); ?>
        </div>
        <div class="grid_3 alpha padtop">
            <a href="#forgotpw" id="linkForgotPw">Lost your password?</a>
        </div>
        <div class="grid_3 omega padtop">
            <button class="button-ptve" type="submit">Sign In</button>
        </div>
        <div class="clear"></div>
    </form>
    <?php if (!empty($password_reset_message)): ?>
    <p><?php echo $password_reset_message; ?></p>
    <?php endif; ?>
    <a name="nmForgotpw"></a>
    <form action="<?php echo URL_ACCT_RESETPW; ?>" id="forgotPw" method="post" autocomplete="on">
        <p>If you don't know your password, don't worry. Enter the email address you registered with to reset your password.</p>
        <input type="email" name="reset-email" value="<?php echo set_value('reset-email'); ?>" placeholder="Email" x-autocompletetype="email">
        <button class="button-ptve" type="submit">Reset password</button>
        <?php echo form_error('reset-email'); ?>
    </form>
</div>
<?php /* content/account_signin.php */