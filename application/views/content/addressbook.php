<h2>Your Addresses</h2>
<?php if (count($addresses) >= 1): ?>
<?php foreach ($addresses as $address): ?>
<div class="grid_12 address">
    <div class="grid_10 alpha"><?php echo format_address_inline($address); ?></div>
    <?php /*div class="grid_1"><a href="<?php echo URL_CHECKOUT_ADDRESS_EDIT . "/$address->code"; ?>" title="Change">Change</a></div*/ ?>
    <div class="grid_2 omega align-right"><a href="<?php echo URL_CHECKOUT_ADDRESS_DELETE . "/$address->code"; ?>" title="Delete Address">Delete Address</a></div>
</div>
<?php endforeach; ?>
<?php /* elseif (count($addresses) === 1): ?>
<div class="grid_10 alpha omega"><?php echo format_address_inline($addresses[0]); ?></div>
*/ ?>
<?php else: ?>
<p>You don't have any addresses in your address book.</p>
<?php endif; ?>
<div class="grid_6 padtop">
    <a href="<?php echo URL_CHECKOUT_ADDRESS_ADD; ?>" title="Add Address">Add Address</a>
</div>
<div class="grid_6 align-right padtop">
    <a href="<?php echo URL_CHECKOUT; ?>" title="Continue to Checkout">Continue to Checkout &rarr;</a>
</div>
<?php /* content/addressbook.php */