<h1>Order Confirmation for <?php echo $order->name; ?></h1>
<p>Thanks for placing your order with <?php echo SITE_NAME; ?>. Here&apos;s what you&apos;ll soon be receiving:</p>
<table style="margin: 0 auto; width: 80%">
    <thead>
        <tr>
            <th style="text-align: left">Item Description</th>
            <th>Quantity</th>
            <th>Price</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($order_lines as $line): ?>
        <tr>
            <td><?php echo $line->name; ?></td>
            <td style="text-align: center"><?php echo $line->quantity; ?></td>
            <td style="text-align: right"><?php echo format_displayable_price($line->total_amount); ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <th colspan="2" style="text-align: right">Subtotal</th>
            <td style="text-align: right"><?php echo format_displayable_price($order->subtotal); ?></td>
        </tr>
        <tr>
            <th colspan="2" style="text-align: right">Delivery</th>
            <td style="text-align: right"><?php echo format_displayable_price($order->delivery_total); ?></td>
        </tr>
        <tr>
            <th colspan="2" style="text-align: right">Total</th>
            <td style="text-align: right"><?php echo format_displayable_price($order->total_amount); ?></td>
        </tr>
    </tfoot>
</table>
<p>Cheers, <?php echo NAME_CUST_HELP; ?></p>
<?php /* application/views/content/account_detail.php */