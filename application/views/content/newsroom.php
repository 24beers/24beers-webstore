<p>Get the latest news and information on <?php echo BRAND_NAME; ?></p>
<h2><a href="/media/PressRelease-24BeersLaunch-2014-02-21.pdf" title="Press Release: 24 Beers Launch">24 Beers launches online beer shop</a></h2>
<p><span class="italic">Norwich, Norfolk, 24th February 2014</span> &mdash;
    24 Beers has launched an online beer shop,
    <a href="https://www.24beers.co.uk" title="24 Beers online shop">24beers.co.uk</a>,
    based in Norwich. The purpose is to give small, independent breweries a
    chance to sell their beer to a national audience.  The shop offers a small
    number of beers with a broad range of styles, keeping quality high and
    prices low.  24 Beers selects each brewery for the quality of its beers and
    its niche appeal and only sells beers you do not find in chain supermarkets.</p>
<?php /* content/newsroom.php */