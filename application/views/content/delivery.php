<!-- Begin Delivery information page -->
<h2>Next Day Delivery &mdash; &pound;<?php echo STORE_DELIVERY_CHARGE_STANDARD; ?></h2>
<p><?php echo BRAND_NAME; ?> gives you a next working day delivery service. We
    dispatch and deliver all orders as soon as possible and our courier has a
    great reputation for speed and service. All of this; the custom beer
    packaging, warehouse costs, delivery service, comes at a price but we try
    to absorb that as much as possible so you only pay
    &pound;<?php echo STORE_DELIVERY_CHARGE_STANDARD; ?> for delivery. We think,
    and we hope you agree, that&apos;s a bargain. To make this possible we have
    to impose a minimum order of 12 bottles of beer.</p>
<p>We aim to deliver all orders by the next working day but we can&apos;t
    guarantee it and occasionally there can be delays, especially around busy
    times like Christmas and new year. We try our best to not let that happen
    and, if anything does go wrong, put it right as quickly as possible and with
    no fuss.</p>
<h2>Free Delivery</h2>
<p>We hope to offer free delivery on orders in the future.</p>
<h2>How We Deliver To Your Door</h2>
<p>When you place an order with <?php echo SITE_NAME; ?> the details and
    quantities of the beers you&apos;ve bought along with your delivery address
    are sent to our warehouse on the A11 in Norfolk. Apart from your delivery
    address absolutely no personal details, such as your billing address or
    payment information, are sent to the warehouse. The beers are hand-picked
    and packed into our high quality double-walled boxes. The boxes are
    collected from the warehouse by our courier, <?php echo COURIER_NAME; ?>, at
    midday and 5pm every business day. Your order is delivered to your door the
    next working day.</p>
<p>All deliveries from us must be signed for by a person over the age of 18. The
    courier may ask for proof of age. If there is no one suitable to receive the
    delivery the courier will leave a card with their contact details so you can
    rearrange delivery at no extra cost. The reason we do not leave the goods in
    a safe place is to prevent any loss or a minor to access alcohol.</p>
<!-- End Delivery information page -->
<?php /* content/delivery.php */