<section class="intro-text">
    <h1><?php echo $page_title; ?></h1>
    <?php if (!empty($category_text)): ?>
    <p><?php echo $category_text ?></p>
    <?php endif; ?>
</section>
<div class="grid_12 cat-nav">
    <div class="grid_2 alpha"><?php echo $result_count; ?> beers found</div>
    <?php /*div class="paging">&nbsp;<a href="#">View all</a> | Page <!-- only show previous if on page 2 or higher --><a href="#" class="ir sprites cat-prev">Previous</a> <a href="#" class="selected">1</a> <a href="#">2</a> <a href="#">3</a> <a href="#" class="ir sprites cat-next">Next</a></div>
    <div class="filter">&nbsp;Sort by
        <select name="filter">
            <option value="Popular">Popular</option>
            <option value="Relevence">Relevence</option>
            <option value="Price">Price</option>
        </select>
    </div*/?>
    <div class="clear"></div>
</div>
<?php require_once APPPATH . 'views/include/product_grid.php'; ?>
<?php /* content/search_result_page.php */