<!-- Begin blog home -->
<div class="grid_8 blog-list">
    <?php foreach ($blog_list as $blog): ?>
    <article class="blog-post" itemscope itemtype="http://schema.org/BlogPosting">
        <header>
            <div class="grid_8 alpha omega">
                <h1><a href="/blog/entry/<?php echo $blog->id; ?>" title="<?php echo $blog->title; ?>"><?php echo $blog->title; ?></a></h1>
            </div>
            <br class="clear">
            <div class="grid_4 alpha">Author: <a href="https://plus.google.com/110947345552996147084?rel=author"><?php echo $blog->author; ?></a></div>
            <div class="grid_4 omega">Published: <time><?php echo $blog->date; ?></time></div>
        </header>
        <div class="grid_8 alpha omega">
            <?php echo $blog->text; ?>
        </div>
        <footer>
            <div class="grid_8 alpha omega">
                <a href="/blog/entry/<?php echo $blog->id; ?>" title="Read full article">Read full article &rarr;</a>
            </div>
        </footer>
    </article>
    <?php endforeach; ?>
</div>
<!-- end .grid_8 -->
<div class="grid_4" id="blognav">
    <?php require_once APPPATH . 'views/include/blog_nav.php'; ?>
</div>
<!-- end .grid_4 -->
<!-- End blog home -->
<?php /* Location: application/views/content/blog_home.php */