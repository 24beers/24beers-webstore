<!-- Begin Terms & Conditions page -->
<p>Please read the following carefully as your use of the
    <?php echo COMPANY_NAME; ?> Website ("<?php echo SITE_NAME; ?>") and
    purchase of its products ("Products") means that you agree to these terms
    &amp; conditions.</p>
<ul>
    <li><a href="#products">Products</a></li>
    <li><a href="#ordering">Ordering</a></li>
    <li><a href="#delivery">Delivery</a></li>
    <li><a href="#returns">Returns</a></li>
    <li><a href="#refunds">Refunds</a></li>
    <li><a href="#general">General</a></li>
</ul>
<ol>
    <li>
        <a name="products"></a>
        <h2>Products</h2>
        <p>All prices are quoted in pounds sterling.</p>
        <p>We aim to ensure that all our prices are correct. If any of our prices are found to be incorrect we will try to contact you to confirm if you would like to continue with your order at the correct price.</p>
        <p>We reserve the right to withdraw or change the price of any of our products listed on the Website. Products, prices and specifications are subject to change without notice.</p>
        <p>We make every effort to ensure product specifications are accurate but take no responsibility and shall not be held liable for any information that is inaccurate, incomplete or out of date.</p>
        <p>It is our policy that any products purchased from <?php echo SITE_NAME; ?> are received by you in perfect condition. We will only exchange or refund products that we deem not "fit for consumption". We cannot exchange, replace, return or refund products that are simply not liked.</p>
    </li>
    <li>
        <a name="ordering"></a>
        <h2>Ordering</h2>
        <p>By placing an order with <?php echo SITE_NAME; ?> you are confirming that you are 18 years of age or over. No refund will be considered for orders placed by minors.</p>
        <p>When you place an order with <?php echo SITE_NAME; ?> using your payment card you are confirming that you are either the card holder, or you have permission from the card holder to use the card for the purpose of ordering from <?php echo SITE_NAME; ?>.</p>
        <p>When you place an order with <?php echo SITE_NAME; ?> we do not hold your payment card details, nor have access to them. The card details are sent directly to our payment provider, <?php echo PAYMENT_PROVIDER_NAME; ?>.</p>
        <p>Payment can only be accepted online through our secure payment provider.</p>
        <p>We take payment for your order when the order is placed.</p>
        <p>We reserve the right to cancel your order if for any reason that order cannot be fulfilled. If we cannot fulfill your order we will contact you to explain and make a full refund of any money collected by us for that order or discuss with you providing alternative products.</p>
        <p>If your order is a gift, the recipient must also be 18 years of age or over.</p>
    </li>
    <li>
        <a name="delivery"></a>
        <h2>Delivery</h2>
        <p>We only deliver to addresses within the United Kingdom.</p>
        <p>Our standard delivery charge is <?php echo STORE_DELIVERY_CHARGE_STANDARD; ?>. We aim to deliver all orders within 3-5 working days. Some areas, such as remote parts of the United Kingdom may take longer.</p>
        <p>Although we endeavour to ensure all orders are delivered within 3-5 working days, we cannot accept liability for any delays to the delivery of your order, or any losses incurred by you resulting from a delayed delivery.</p>
        <p>We cannot be held responsible for any delivery failure that occurs due to incorrect address or contact information provided to us by you.</p>
        <p>Deliveries must be signed for by a person 18 years of age or over. Our courier may request some form of identification from the consignee to prove their age.</p>
        <p>Signature at the point of delivery acknowledges receipt of the goods. Goods will not be left with third parties.</p>
        <p>It is the responsibility of the customer to report goods that are damaged in transit. The consignee has the responsibility to check the delivery and not accept a delivery that is damaged.</p>
    </li>
    <li>
        <a name="returns"></a>
        <h2>Returns</h2>
        <p>Any unopened bottles may be returned to us within 28 days of placing the order. It is the customer's responsibility to ensure that the products are packaged appropriately when returning to us as we cannot accept damaged good to be returned.</p>
        <p>Returns can be sent to the following address:<br>
            <?php echo COMPANY_NAME; ?> (Returns),<br>
            <?php echo WAREHOUSE_ADDRESS_INLINE; ?></p>
    </li>
    <li>
        <a name="refunds"></a>
        <h2>Refunds</h2>
        <p>If some or all of your delivery is damaged when you receive it, please inform us immediately at <?php echo EMAIL_CUST_HELP; ?>. We will refund the value of the portion of the order that is spoiled or damaged.</p>
        <p>We will provide a refund within 28 days of our receipt of the returned goods.</p>
    </li>
    <li>
        <a name="general"></a>
        <h2>General</h2>
        <h3>Web Site Access</h3>
        <p>It is not necessary to register with us in order to use most parts of this Website. However, particular areas of this Website will only be accessible only if you have registered.</p>
        <h3>Use of Web Site</h3>
        <p>This Website may be used for your own private purposes and in accordance with these terms of use. You may print and download material from this Website provided that you do not modify or reproduce any content without our prior written consent.</p>
        <h3>Site Uptime</h3>
        <p>All reasonable measures are taken by us to ensure that this Website is operational all day, every day. However, occasionally technical issues may result in some downtime and accordingly we will not be liable if this Website is unavailable at any time.</p>
        <p>Where possible we always try to give advance warning of maintenance issues that may result in Website down time but we shall not be obliged to provide such notice.</p>
        <h3>VISITOR PROVIDED MATERIAL</h3>
        <p>Any material that a visitor to this Website sends or posts to this Website shall be considered non-proprietary and non-confidential. We shall be entitled to copy, disclose, distribute or use for such other purpose as we deem appropriate all material provided to us, with the exception of personal information, the use of which is covered under our <a href="<?php echo URL_PRIVACY_POLICY; ?>" title="Privacy Policy">Privacy Policy</a>.</p>
        <p>When using this Website you shall not post or send to or from this Website any material:</p>
        <ul>
            <li>for which you have not obtained all necessary consents;</li>
            <li>that is discriminatory, obscene, pornographic, defamatory, liable to incite racial hatred, in breach of confidentiality or privacy, which may cause annoyance or inconvenience to others, which encourages or constitutes conduct that would be deemed a criminal offense, give rise to a civil liability, or otherwise is contrary to the law in the United Kingdom;</li>
            <li>which is harmful in nature including, and without limitation, computer viruses, Trojan horses, corrupted data, or other potentially harmful software or data.</li>
        </ul>
        <h3>Links To and From Other Websites</h3>
        <p>Throughout this Website you may find links to third party Websites. The provision of a link to such a Website does not mean that we endorse that Website. If you visit any Website via a link on this Website you do so at your own risk.</p>
        <p>Any party wishing to link to this Website is entitled to do so provided that the conditions below are observed:</p>
        <ul>
            <li>you do not seek to imply that we are endorsing the services or products of another party unless this has been agreed with us in writing;</li>
            <li>you do not misrepresent your relationship with this Website; and</li>
            <li>the Website from which you link to this Website does not contain offensive or otherwise controversial content or, content that infringes any intellectual property rights or other rights of a third party.</li>
        </ul>
        <p>By linking to this Website in breach of these clauses you shall indemnify us for any loss or damage suffered to this Website as a result of such linking.</p>
        <h2>Disclaimer</h2>
        <p>Whilst we do take all reasonable steps to make sure that the information on this Website is up to date and accurate at all times we do not guarantee that all material is accurate and, or up to date.</p>
        <p>All material contained on this Website is provided without any or warranty of any kind. You use the material on this Website at your own discretion.</p>
        <h3>Exclusion of Liability</h3>
        <p>We do not accept liability for any loss or damage that you suffer as a result of using this Website.</p>
        <p>Nothing in these Terms of Use shall exclude or limit liability for death or personal injury caused by negligence which cannot be excluded or under the law of the United Kingdom.</p>
        <h3>Law and Jurisdiction</h3>
        <p>These terms of use are governed by English law. Any dispute arising in connection with these terms of use shall be subject to the exclusive jurisdiction of the Courts of England and Wales.</p>
        <h3>Our Details</h3>
        <p>Our business' name is: <?php echo COMPANY_NAME; ?><br>
            Our business address is: <?php echo COMPANY_ADDRESS_INLINE; ?><br>
            Our company number is: <?php echo COMPANY_NUMBER; ?><br>
            Our contact details are: <?php echo EMAIL_CUST_HELP; ?></p>
    </li>
</ol>
<!-- End Terms & Conditions page -->
<?php /* content/terms.php */