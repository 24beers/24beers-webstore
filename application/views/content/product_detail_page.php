<!-- Begin product detail page -->
<article id="product-detail" itemscope itemtype="http://schema.org/Product">
    <div class="grid_4 breadcrumb">
        <?php echo $breadcrumb_content; ?>
    </div>
    <div class="grid_8 alpha omega">
        <h1 itemprop="name"><?php echo $product->name; ?>
            <span>from <a href="<?php echo URL_BREWERY . "/$product->brewery_code"; ?>" title="<?php echo $product->brewery_name; ?>">
                    <?php echo $product->brewery_name; ?></a></span></h1>
    </div>
    <div class="clear"></div>
    <div class="grid_4 bigbeer">
        <img alt="<?php echo $product->name; ?> <?php echo $product->size_container ?>" height="493" itemprop="image" width="300" src="<?php echo $image_url; ?>" title="<?php echo $product->name; ?> <?php echo $product->size_container ?>">
    </div>
    <div class="grid_8">
        <div class="grid_8 alpha omega">
            <p itemprop="description"><?php echo $product->text; ?></p>
        </div>
        <!-- end .grid_8 -->
        <br class="clear">
        <div class="grid_4 alpha review">
            <?php if ($product->review_count > 0): ?>
            <p itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                Rated <span class="bold" itemprop="ratingValue"><?php echo $product->review_avg; ?></span>/5
                based on <a href="#reviews" id="reviewsLink" title="Customer Reviews for <?php echo $product->name; ?>">
                    <span class="bold" itemprop="reviewCount"><?php echo $product->review_count; ?></span> customer reviews</a>.
            </p>
            <?php else: ?>
            <p>This beer has not yet been reviewed.<br>
                <?php if (is_authed()): ?>
                If you&apos;ve tried it, why not review it now.
                <?php endif; ?>
            </p>
            <?php endif; ?>
            <?php if (is_authed()): ?>
            <h2>Review This Beer</h2>
            <form action="<?php echo URL_PRODUCT_REVIEW; ?>" method="post" autocomplete="off">
                <input name="product_code" type="hidden" value="<?php echo $product->code; ?>">
                <input id="inRating" name="rating" type="hidden" value="0">
                <input id="inComment" name="comment" type="hidden" value="">
                <h3>Rating</h3>
                <?php echo form_error('rating'); ?>
                <div id="rating"></div>
                <h3>Comment</h3>
                <?php echo form_error('comment'); ?>
                <textarea class="fullwidth" name="comment" placeholder="Comment"></textarea><br>
                <p><button class="button" onclick="_gaq.push(['_trackEvent', '<?php echo APP_GA_EVENT_CAT_REVIEW; ?>', '<?php echo APP_GA_EVENT_ACT_ADD; ?>', '<?php echo $product->code; ?>']);">Submit Review</button></p>
            </form>
            <?php endif; ?>
        </div>
        <!-- end .grid_4 -->
        <div class="grid_4 omega">
            <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <p>
                    <span class="price" itemprop="price"><?php echo $product->displayable_sale_price; ?></span>
                    <?php if ($product->sale_price < $product->list_price): ?>
                    (Usually <?php echo $product->displayable_list_price; ?>)
                    <?php endif; ?>
                </p>
                <?php if ($product->stock_level > 0): ?>
                <form id="formAddToBasket" action="<?php echo URL_BASKET_ADD; ?>" method="post" onsubmit="return addToBasket('<?php echo URL_BASKET_ADD_ASYNC; ?>')">
                    <input id="skuCode" type="hidden" name="sku_code" value="<?php echo $product->sku_code; ?>">
                    <span itemprop="availability" href="http://schema.org/InStock">
                        <label for="txtQuantity">Quantity:</label>
                        <input id="txtQuantity" min="1" max="99" maxlength="2" name="quantity" pattern="([0-9]+)" size="1" step="1" type="number" value="1">
                    </span>
                    <button class="button-ptve" id="btnAddToBasket" onclick="_gaq.push(['_trackEvent', '<?php echo APP_GA_EVENT_CAT_BASKET; ?>', '<?php echo APP_GA_EVENT_ACT_ADD; ?>', '<?php echo $product->code; ?>']);" rel="nofollow" type="submit">Add to basket</button>
                </form>
                <p id="stock_level">
                    <?php if ($product->stock_level <= 10): ?>
                    Hurry, just <?php echo $product->stock_level ?> left in stock!
                    <?php elseif ($product->stock_level <= 50): ?>
                    Only <?php echo $product->stock_level ?> left in stock
                    <?php endif; ?>
                </p>
                <?php else: ?>
                <p>Sorry, this product is out of stock. Let us know if you&apos;d like more of this beer.</p>
                <?php endif; ?>
            </div>
        </div>
        <br class="clear">
        <div class="grid_4 alpha highlightbox">
            <h2>Factsheet</h2>
            <ul>
                <?php if (!empty($product->style_name)): ?>
                <li><span class="bold">Style:</span> <?php echo $product->style_name; ?></li>
                <?php endif; ?>
                <?php if (!empty($product->abv)): ?>
                <li><span class="bold">ABV:</span> <?php echo $product->abv; ?></li>
                <?php endif; ?>
                <?php if (!empty($product->hops)): ?>
                <li><span class="bold">Hops:</span> <?php echo $product->hops; ?></li>
                <?php endif; ?>
                <?php if (!empty($product->malts)): ?>
                <li><span class="bold">Malts:</span> <?php echo $product->malts; ?></li>
                <?php endif; ?>
                <?php if (!empty($product->size_container)): ?>
                <li><span class="bold">Container:</span> <?php echo $product->size_container; ?></li>
                <?php endif; ?>
            </ul>
        </div>
        <div class="grid_4 omega highlightbox">
            <h2>About <?php echo $product->brewery_name; ?></h2>
            <?php if (empty($product->brewery_description)): ?>
            <p>We don't yet have any information about this brewery.</p>
            <?php else: ?>
            <p><?php echo $product->brewery_description; ?></p>
            <?php endif; ?>
            <a href="<?php echo URL_BREWERY . "/$product->brewery_code"; ?>">
                    See more beers from <?php echo $product->brewery_name; ?></a>
        </div>
        <br class="clear">
        <div class="grid_8 alpha omega highlightbox">
            <h2>Share This Beer</h2>
            <div class="grid_2 alpha">
                <a href="<?php echo "//www.pinterest.com/pin/create/button/?url=$encoded_page_url&media=$encoded_image_url&description=$product->name%20from%2024%20Beers"; ?>" data-pin-do="buttonPin" data-pin-config="beside" title="Pin <?php echo $product->name; ?>">
                    <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" alt="Pin <?php echo $product->name; ?>"></a>
            </div>
            <div class="grid_2">
                <a href="https://twitter.com/share" class="twitter-share-button" data-via="24BeersUK" data-hashtags="24Beers">Tweet</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
            </div>
            <div class="grid_2">
                <div class="fb-like" data-href="<?php echo $page_url; ?>" data-width="450" data-layout="button_count" data-action="recommend" data-show-faces="true" data-send="false"></div>
            </div>
            <div class="grid_2 omega">
                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
            </div>
            <?php /* Removed because SU doesn't yet support HTTPS.
                <su:badge layout="2"></su:badge>
                <script type="text/javascript">window._loadCalled = true;</script>*/ ?>
            <br class="clear">
        </div>        
    </div>
    <div class="clear"></div>
    <h2>Similar Beers</h2>
    <?php if (empty($cross_sell_content)): ?>
    <p>We don't yet have any similar beers to show you.</p>
    <?php else: ?>
        <?php echo $cross_sell_content; ?>
    <?php endif; ?>
    <div class="clear"></div>
    <div class="grid_12">
        <a name="reviews"></a>
        <div id="reviews" title="Customer Reviews">
            <h2>Reviews for <?php echo $product->name; ?></h2>
            <ul>
                <?php foreach ($product->reviews as $review): ?>
                <li>
                    <h3>Comment</h3>
                    <?php if (empty($review->comment)): ?>
                    No comment.
                    <?php else: ?>
                    <?php echo $review->comment; ?>
                    <?php endif; ?><br>
                    <h3>Rating</h3>
                    <?php echo $review->rating; ?>/5
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</article>
<!-- End product detail page -->
<?php /* content/product_detail_page.php */