<!-- Begin category navigation -->
<br class="clear">
<div class="grid_4" id="sidenav">
    <h2>Categories</h2>
    <ul>
        <?php foreach ($category_list as $category): ?>
        <li><a href="/category/style/<?php echo $category->code; ?>"><?php echo "$category->name ($category->size)"; ?></a></li>
        <?php endforeach; ?>
    </ul>
    <h2>Categories</h2>
    <ul>
        <li><input type="checkbox" name="yyy" id="aa" value="xxx"> <label for="aa">Black Sheep</label> <span class="quantity">(2)</span></li>
        <li><input type="checkbox" name="yyy" id="a" value="xxx"> <label for="a">Burton Bridge</label>  <span class="quantity">(6)</span></li>
        <li><input type="checkbox" name="yyy" id="b" value="xxx"> <label for="b">Coniston</label>  <span class="quantity">(10)</span></li>
        <li><input type="checkbox" name="yyy" id="c" value="xxx"> <label for="c">Conway</label>  <span class="quantity">(57)</span></li>
        <li><input type="checkbox" name="yyy" id="d" value="xxx"> <label for="d">Downton</label>  <span class="quantity">(4)</span></li>
        <li><input type="checkbox" name="yyy" id="e" value="xxx"> <label for="e">Harviston</label>  <span class="quantity">(2)</span></li>
        <li><input type="checkbox" name="yyy" id="f" value="xxx"> <label for="f">Ilkley</label>  <span class="quantity">(5)</span></li>
        <li><input type="checkbox" name="yyy" id="g" value="xxx"> <label for="g">Skye</label>  <span class="quantity">(5)</span></li>
        <li><input type="checkbox" name="yyy" id="h" value="xxx"> <label for="h">Marble</label>  <span class="quantity">(8)</span></li>
        <li><input type="checkbox" name="yyy" id="i" value="xxx"> <label for="i">Nethergate</label>  <span class="quantity">(8)</span></li>
        <li><input type="checkbox" name="yyy" id="j" value="xxx"> <label for="j">Purity </label>  <span class="quantity">(8)</span></li>
        <li><input type="checkbox" name="yyy" id="k" value="xxx"> <label for="k">Robinsons</label>  <span class="quantity">(28)</span></li>
    </ul>
    <h2>Strength (ABV)</h2>
    <ul class="strength">
        <li><input type="checkbox" name="yyy" id="l" value="xxx"> <label for="l">Very Low <b>&lt;1.9%</b></label> <span class="quantity">(2)</span></li>
        <li><input type="checkbox" name="yyy" id="m" value="xxx"> <label for="m">Low <b>2-2.9%</b></label>  <span class="quantity">(6)</span></li>
        <li><input type="checkbox" name="yyy" id="n" value="xxx"> <label for="n">Mid strength <b>2-3.9%</b></label>  <span class="quantity">(10)</span></li>
        <li><input type="checkbox" name="yyy" id="o" value="xxx"> <label for="o">Mid to strong <b>4-4.9%</b></label>  <span class="quantity">(57)</span></li>
        <li><input type="checkbox" name="yyy" id="p" value="xxx"> <label for="p">Strong <b>5-6.9%</b></label>  <span class="quantity">(4)</span></li>
        <li><input type="checkbox" name="yyy" id="q" value="xxx"> <label for="q">Very strong <b>7% +</b></label>  <span class="quantity">(2)</span></li>
    </ul>
    <h2>Price range</h2>
    <div class="layout-slider" style="width: 100%">
        <span style="display: inline-block; width: 290px; padding: 0 5px;"><input id="Slider1" type="slider" name="price" value="0;30"></span>
    </div>
    <script type="text/javascript" charset="utf-8"><!-- documentation at http://egorkhmelev.github.com/jslider/ -->
        jQuery("#Slider1").slider( {from: 1,to: 30,heterogeneity: ['50/5', '75/15'],scale: [],limits: false,step: 1,dimension: '&pound;',skin: "blue"  });
    </script>
</div>
<!-- End category navigation -->