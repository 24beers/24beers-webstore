<form id="simple_search" action="/search" method="get">
    <input id="search_query" name="q" placeholder="Search all our beers" autocomplete="off">
    <button class="sprites search-button" type="submit" title="Click to search">Go</button>
</form>