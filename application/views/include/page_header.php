<header id="pageheader" itemscope itemtype="http://schema.org/WPHeader">
    <div class="container_12">
        <div class="grid_4 sitetitle" itemscope itemtype="http://schema.org/Organization">
            <h1>
                <a class="sprites logo" href="/" itemprop="url" title="<?php echo BRAND_NAME; ?> Home">Buy Beer Online from <?php echo BRAND_NAME; ?></a>
            </h1>
        </div>
        <div class="grid_4 searchbox">
            <?php require_once 'search_form_simple.php'; ?>
        </div>
        <div class="grid_2 social">
            <a class="sprites twitter-button" href="<?php echo BRAND_URL_TWITTER; ?>" target="_blank" title="<?php echo TWITTER_BRAND; ?> on Twitter">Read our beer tweets on Twitter</a>
            <a class="sprites facebook-button" href="<?php echo BRAND_URL_FACEBOOK; ?>" target="_blank" title="<?php echo FACEBOOK_BRAND; ?> on Facebook">Like our beer news on Facebook</a>
            <a class="sprites pinterest-button" href="<?php echo BRAND_URL_PINTEREST; ?>" target="_blank" title="<?php echo PINTEREST_BRAND; ?> on Pinterest">Follow our beer boards on Pinterest</a>
            <a class="sprites instagram-button" href="<?php echo BRAND_URL_INSTAGRAM; ?>" target="_blank" title="<?php echo INSTAGRAM_BRAND; ?> on Instagram">See our craft beer and ale pictures on Instagram</a>
            <a class="sprites gplus-button" href="<?php echo BRAND_URL_GPLUS; ?>" rel="publisher" target="_blank" title="<?php echo GPLUS_BRAND; ?> on Google+">Follow our beer information on Google+</a>
        </div>
        <div class="grid_2 youraccount">
            <?php /*div class="grid_2 alpha small align-center">
                <?php if (is_authed()): ?>
                Welcome back<br>to <?php echo BRAND_NAME; ?>!
                <?php else: ?>
                Welcome to<br><?php echo BRAND_NAME; ?>
                <?php endif; ?>
            </div */?>
            <div class="grid_1 alpha">
                <!-- Begin mini-basket -->
                <div id="hdrBasket">
                    <a href="<?php echo URL_BASKET; ?>" title="Basket">
                        <img alt="Basket" height="20" src="/media/basket-icon.svg" width="34"></a>
                    <?php /*<span id="basketTotal"></span>*/ ?>
                    <div class="minibasket">
                        <div class="items"></div>
                        <button class="button-ptve" data-href="<?php echo URL_BASKET; ?>" id="btnGotoBasket">Check out<br>securely &rarr;</button>
                    </div>
                </div>
                <!-- End mini-basket -->
            </div>
            <div class="grid_1 omega">
                <div class="accountmenu">
                    <a class="menu-icon" href="<?php echo URL_ACCT_HOME; ?>" rel="nofollow" title="Account Menu">
                        <img alt="Menu Navigation" height="20" src="/media/menu-icon.svg" width="34"></a>
                    <ul>
                        <?php if (is_authed()): ?>
                        <li><a href="<?php echo URL_ACCT_HOME; ?>" title="Your Account">Your Account</a></li>
                        <li><a href="<?php echo URL_ACCT_SIGNOUT; ?>" title="Sign Out">Sign Out</a></li>
                        <?php else: ?>
                        <li><a href="<?php echo URL_ACCT_JOIN; ?>" title="Join <?php echo BRAND_NAME; ?>">Join</a></li>
                        <li><a href="<?php echo URL_ACCT_SIGNIN; ?>" title="Sign In">Sign In</a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php require_once 'confidence_bar.php'; ?>
    </div>
</header>
<?php /* include/page_header.php */