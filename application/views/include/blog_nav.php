<!-- Begin blog navigation -->
<h2>Read more posts</h2>
<ul>
    <?php foreach ($blog_nav as $nav): ?>
    <li><a href="/blog/entry/<?php echo $nav->id; ?>" title="<?php echo $nav->title; ?>"><?php echo $nav->title; ?></a></li>
    <?php endforeach; ?>
</ul>
<!-- End blog navigation -->
<?php /* application/views/include/blog_nav.php */