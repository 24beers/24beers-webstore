<!-- Begin minimal page header -->
<header id="pageheader" itemscope itemtype="http://schema.org/WPHeader">
    <div class="container_12">
        <div class="grid_4 sitetitle" itemscope itemtype="http://schema.org/Organization">
            <h1>
                <a class="sprites logo" href="/" itemprop="url" title="<?php echo BRAND_NAME; ?> Home">Buy Beer Online from <?php echo BRAND_NAME; ?></a>
            </h1>
        </div>
        <div class="grid_4">
            <h1 class="page-title"><?php echo $page_title; ?></h1>
        </div>
        <div class="grid_4">
            <span id="siteseal" title="Shop Securely with <?php echo BRAND_NAME; ?>">
                <script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=59yWdJIYGMCa5DO6PuTNeGglH6ONQTf4m3q7KcavrKyEZSuPbg"></script></span>
        </div>
    </div>
</header>
<!-- End minimal page header -->
<?php /* include/page_header_min.php */