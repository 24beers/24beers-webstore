<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', '<?php echo APP_GA_ACCT; ?>', '<?php echo APP_DOMAIN; ?>', { 'siteSpeedSampleRate': 100 });
    ga('send', 'pageview');
    <?php if (isset($track_ecommerce) && $track_ecommerce === TRUE): ?>
    ga('require', 'ecommerce', 'ecommerce.js');
    ga('ecommerce:addTransaction', { 'id': '<?php echo $order->code; ?>', 'affiliation': '<?php echo BRAND_NAME; ?>', 'revenue': '<?php echo format_decimal_price($order->subtotal); ?>', 'shipping': '<?php echo format_decimal_price($order->delivery_total); ?>', 'tax': '0.00' });
    <?php foreach ($order_lines as $line): ?>
    ga('ecommerce:addItem', { 'id': '<?php echo $order->code; ?>', 'name': '<?php echo $line->name; ?>', 'sku': '<?php echo $line->code; ?>', 'category': 'Beer', 'price': '<?php echo format_decimal_price($line->unit_price); ?>', 'quantity': '<?php echo $line->quantity; ?>' });
    <?php endforeach; ?>
    ga('ecommerce:send');
    <?php endif; ?>
</script>