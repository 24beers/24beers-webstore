<meta charset="utf-8">
<?php if (isset($meta_description) && !empty($meta_description)): ?>
<meta content="<?php echo $meta_description; ?>" name="description">
<?php endif; ?>
<meta content="width=device-width,initial-scale=1.0,user-scalable=0,minimum-scale=1.0,maximum-scale=1.0" name="viewport">
<link href="<?php echo current_url(); ?>" rel="canonical">
<link href="/media/24beers-logo-main.png" rel="icon" sizes="any" type="image/png">
<link href="<?php echo site_url('/media/24beers-logo-main.png'); ?>" rel="logo" type="image/png">
<link href="<?php echo site_url(URL_BLOG_FEED); ?>" rel="alternate" title="24 Beers Blog" type="application/rss+xml">
<?php require_once 'ga.php'; ?>
<link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Roboto+Slab|Roboto" rel="stylesheet">
<link href="/media/master.min.css?ver=1404292317" rel="stylesheet">
<script>
var ADAPT_CONFIG = {
    path: '/media/',
    dynamic: true,
    range: [
        '0px   to 760px = mobile.min.css?ver=1404292317',
        '760px to 980px = 720.min.css?ver=1404292317',
        '980px          = 960.min.css?ver=1404292317'
    ]
};
</script>
<script async src="/media/adapt.min.js"></script>
<?php /* include/header_meta.php */