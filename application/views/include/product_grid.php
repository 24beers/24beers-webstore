<?php if (!empty($grid_products)): ?>
<section id="productgrid">
    <?php foreach ($grid_products as $product): ?>
    <div class="grid_4 product" itemscope itemtype="http://schema.org/Product">
        <h2><a href="<?php echo URL_PRODDETAIL_BEER . "/$product->code"; ?>" itemprop="url" title="<?php echo "More info on $product->name"; ?>">
                <span itemprop="name"><?php echo $product->name; ?></span></a></h2>
        <a href="<?php echo URL_PRODDETAIL_BEER . "/$product->code"; ?>" itemprop="url" title="<?php echo "More info on $product->name"; ?>">
            <img alt="<?php echo $product->name; ?> <?php echo $product->size_container ?>" height="230" itemprop="image" src="<?php echo URL_MEDIA_PRODUCT . "/$product->code"; ?>_small.jpg" width="140"></a>
        <div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
            <span itemprop="price"><?php echo $product->displayable_sale_price; ?></span>
        </div>
        <ul>
            <li><?php echo $product->style_name; ?></li>
            <li>ABV: <?php echo $product->abv; ?></li>
            <li><?php echo $product->size_container; ?></li>
        </ul>
    </div>
    <?php endforeach; ?>
</section>
<?php endif; ?>
<?php /* Location: application/views/include/product_grid.php */