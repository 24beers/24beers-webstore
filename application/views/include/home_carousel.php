<!-- Begin carousel -->
<div id="slider">
    <ul>
        <?php foreach ($carousel_products as $slide): ?>
        <li>
            <!-- Begin slide -->
            <div class="slide">
                <?php $i = 1; $class = ''; ?>
                <?php
                foreach ($slide as $product):
                    if ($i % 6 === 0) {
                        $class = 'omega';
                    }
                ?>
                <!-- Begin product slot -->
                <div class="grid_2<?php echo (empty($class)) ? "" : " $class"; ?> bottle-display bottle<?php echo $i; ?>">
                    <img src="<?php echo URL_MEDIA_PRODUCT . "/$product->code"; ?>_small.jpg" alt="<?php echo $product->name; ?> <?php echo $product->size_container ?>" width="140" height="275">
                    <div class="price">
                        <p><?php echo $product->displayable_sale_price ?></p>
                    </div>
                    <h5><a href="<?php echo URL_PRODDETAIL_BEER . "/$product->code"; ?>"><?php echo $product->name; ?></a></h5>
                    <p class="desc"><?php echo $product->style_name; ?><br>
                        ABV: <?php echo $product->abv; ?><br>
                        <?php echo $product->size_container; ?></p>
                </div>
                <!-- End product slot -->
                <?php
                    $i++;
                    $class = '';
                    if ($i % 6 === 0) {
                        $i = 1;
                    }
                endforeach;
                ?>
            </div>
            <!-- End slide -->
        </li>
        <?php endforeach; ?>
    </ul>
</div>
<!-- End carousel -->
<?php /* Location: application/views/include/home_carousel.php */