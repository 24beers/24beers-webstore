<footer id="pagefooter" itemscope itemtype="http://schema.org/WPFooter">
    <div class="container_12">
        <div class="grid_12">
            <h1>Handy stuff to know...</h1>
        </div>
        <div class="grid_4">
            <h2>Subscribe to Our Newsletter</h2>
            <p>Get the <?php echo BRAND_NAME; ?> newsletter and keep up to date with our news and promotions.</p>
            <?php require_once 'newsletter_signup_form.php'; ?>
            <br>
            <p>Please always drink responsibly.<br>
                <a class="bold" href="http://www.drinkaware.co.uk/" target="_blank" title="Drink Aware">drinkaware.co.uk</a></p>
        </div>
        <div class="grid_4">
            <h2>Get in touch</h2>
            <h3>Email</h3>
            <p><script>document.write('<'+'a'+' '+'h'+'r'+'e'+'f'+'='+'"'+'m'+'a'+'i'+'l'+'t'+'o'+'&'+'#'+'5'+'8'+';'+'c'+'a'+'r'+'e'+'&'+'#'+'6'+'4'+';'+'2'+'4'+'b'+'%'+'&'+'#'+'5'+'4'+';'+'&'+'#'+'5'+'3'+';'+'&'+'#'+'3'+'7'+';'+'6'+'5'+'&'+'#'+'3'+'7'+';'+'7'+'2'+'&'+'#'+'1'+'1'+'5'+';'+'&'+'#'+'3'+'7'+';'+'2'+'E'+'%'+'6'+'3'+'%'+'6'+'F'+'&'+'#'+'3'+'7'+';'+'2'+'E'+'u'+'k'+'"'+'>'+'c'+'a'+'r'+'e'+'&'+'#'+'6'+'4'+';'+'2'+'4'+'b'+'e'+'&'+'#'+'1'+'0'+'1'+';'+'&'+'#'+'1'+'1'+'4'+';'+'s'+'&'+'#'+'4'+'6'+';'+'c'+'o'+'&'+'#'+'4'+'6'+';'+'&'+'#'+'1'+'1'+'7'+';'+'&'+'#'+'1'+'0'+'7'+';'+'<'+'/'+'a'+'>');</script>
                <noscript>[Turn on JavaScript to see the email address]</noscript></p>
            <h3>Post</h3>
            <div itemscope itemtype="http://schema.org/Organization">
                <div itemprop="name"><?php echo COMPANY_NAME; ?></div>
                <div itemscope itemtype="http://schema.org/PostalAddress">
                    <div itemprop="streetAddress">Norwich Enterprise Centre, 4b Guildhall Hill</div>
                    <div itemprop="addressLocality">Norwich</div>
                    <div itemprop="postalCode">NR2 1JH</div>
                </div>
            </div>
        </div>
        <div class="grid_4">
            <h2>About Us</h2>
            <nav>
                <ul>
                    <li><a href="<?php echo URL_BLOG; ?>" title="Blog">Blog</a></li>
                    <li><a href="<?php echo URL_DELIVERY_INFO; ?>" title="Delivery Information">Delivery Information</a></li>
                    <li><a href="<?php echo URL_FAQ; ?>" title="Frequently Asked Questions">FAQ</a></li>
                    <li><a href="<?php echo URL_PRIVACY_POLICY; ?>" title="Privacy Policy">Privacy Policy</a></li>
                    <li><a href="<?php echo URL_TERMS_CONDITIONS; ?>" title="Terms &amp; Conditions">Terms &amp; Conditions</a></li>
                </ul>
            </nav>
        </div>
        <div class="grid_12 align-center">
            Copyright &copy; <?php echo COMPANY_NAME ?>.
            All rights reserved.<br>
            Registered in England <?php echo COMPANY_NUMBER; ?>
        </div>
    </div>
</footer>
<?php require_once 'footer_javascript.php'; ?>
<?php /* include/page_footer.php */