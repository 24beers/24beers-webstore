<?php if (isset($breadcrumb)): ?>
<ul itemprop="breadcrumb">
    <?php foreach ($breadcrumb as $crumb): ?>
    <li>
        <?php if (isset($crumb->link)): ?>
        <a href="<?php echo $crumb->link; ?>"><?php echo $crumb->label; ?></a>
        <?php else: ?>
        <strong><?php echo $crumb->label; ?></strong>
        <?php endif; ?>
    </li>
    <?php endforeach; ?>
</ul>
<?php endif;
/* End of file breadcrumb.php */
/* Location: ./application/views/include/breadcrumb.php */