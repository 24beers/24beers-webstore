<!-- Begin category navigation -->
<br class="clear">
<div class="grid_4" id="sidenav">
    <?php foreach ($facet_categories as $category): ?>
    <h2><?php echo $category->label ?></h2>
    <ul>
        <?php foreach ($category->facets as $facet): ?>
        <li>
            <?php if ($facet->count > 0): ?>
            <h3><a href="<?php echo URL_CATEGORY . "/$category->code/$facet->term"; ?>" title="<?php echo $facet->term; ?>"><?php echo $facet->term; ?> <span class="quantity">(<?php echo $facet->count; ?>)</span></a></h3>
            <?php else: ?>
            <h3><?php echo $facet->term; ?> <span class="quantity">(0)</span></h3>
            <?php endif; ?>
        </li>
        <?php endforeach; ?>
    </ul>
    <?php endforeach; ?>
</div>
<!-- End category navigation -->
<?php /* include/facet_nav.php */