<header id="pageheader" itemscope itemtype="http://schema.org/WPHeader">
    <div class="container_12">
        <div class="grid_4 sitetitle" itemscope itemtype="http://schema.org/Organization">
            <h1>
                <a class="ir logo" href="/" itemprop="url" title="<?php echo BRAND_NAME; ?> Home">
                    <?php echo BRAND_NAME; ?></a>
            </h1>
        </div>
        <div class="grid_4 searchbox">
            <?php require_once 'search_form_simple.php'; ?>
        </div>
        <div class="grid_2 social">
            <span>
                <a href="<?php echo BRAND_URL_TWITTER; ?>" target="_blank" title="<?php echo TWITTER_BRAND; ?> on Twitter">
                    <img alt="Twitter badge" height="20" src="/media/twitter-logo-24x20.png" width="24"></a>
            </span>
            <span>
                <a href="<?php echo BRAND_URL_FACEBOOK; ?>" target="_blank" title="<?php echo FACEBOOK_BRAND; ?> on Facebook">
                    <img alt="Facebook badge" height="20" src="/media/facebook-logo-20x20.png" width="20"></a>
            </span>
            <span>
                <a href="<?php echo BRAND_URL_PINTEREST; ?>" target="_blank" title="<?php echo PINTEREST_BRAND; ?> on Pinterest">
                    <img alt="Pinterest badge" height="20" src="/media/pinterest-logo-20x20.png" width="20"></a>
            </span>
            <span>
                <a href="<?php echo BRAND_URL_INSTAGRAM; ?>" target="_blank" title="<?php echo INSTAGRAM_BRAND; ?> on Instagram">
                    <img alt="Instagram badge" height="20" src="/media/instagram-logo-20x20.png" width="20"></a>
            </span>
            <span>
                <a href="<?php echo BRAND_URL_GPLUS; ?>" rel="publisher" target="_blank" title="<?php echo GPLUS_BRAND; ?> on Google+">
                    <img alt="Google+ badge" height="20" src="//ssl.gstatic.com/images/icons/gplus-16.png" width="20"></a>
            </span>
        </div>
        <div class="grid_2 youraccount">
            <?php /*div class="grid_2 alpha small align-center">
                <?php if (is_authed()): ?>
                Welcome back<br>to <?php echo BRAND_NAME; ?>!
                <?php else: ?>
                Welcome to<br><?php echo BRAND_NAME; ?>
                <?php endif; ?>
            </div */?>
            <div class="grid_1 alpha">
                <!-- Begin mini-basket -->
                <div id="hdrBasket">
                    <a href="<?php echo URL_BASKET; ?>" title="Basket">
                        <img alt="Basket" height="20" src="/media/basket-icon.svg" width="34"></a>
                    <?php /*<span id="basketTotal"></span>*/ ?>
                    <div class="minibasket">
                        <div class="items"></div>
                        <button class="button-ptve" data-href="<?php echo URL_BASKET; ?>" id="btnGotoBasket">Check out<br>securely &rarr;</button>
                    </div>
                </div>
                <!-- End mini-basket -->
            </div>
            <div class="grid_1 omega">
                &nbsp;
            </div>
        </div>
        <?php require_once 'confidence_bar.php'; ?>
    </div>
</header>
<?php /* include/page_header.php */