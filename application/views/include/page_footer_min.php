<!-- Begin minimal page footer -->
<footer class="min" id="pagefooter" itemscope itemtype="http://schema.org/WPFooter">
    <div class="container_12">
        <div class="grid_12 align-center">
            Copyright &copy; <?php echo COMPANY_NAME ?>.
            All rights reserved.<br>
            Registered in England <?php echo COMPANY_NUMBER; ?>
        </div>
    </div>
</footer>
<!-- End minimal page footer -->
<?php require_once 'footer_javascript.php'; ?>
<?php /* include/page_footer_min.php */