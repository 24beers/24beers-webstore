<!-- Begin confidence bar -->
<div class="grid_12" id="confidencebar">
    <div class="grid_4 alpha">
        <h2 class="confidencebutton">
            <a href="<?php echo URL_24BEERS_INFO; ?>" title="Twenty Four Beers">Twenty Four Beers</a>
        </h2>
    </div>
    <!-- end .grid_4 -->
    <div class="grid_4">
        <h2 class="confidencebutton">
            <a href="<?php echo URL_BLOG; ?>" title="<?php echo BRAND_NAME; ?> Blog"><?php echo BRAND_NAME; ?> Blog</a>
        </h2>
    </div>
    <!-- end .grid_4 -->
    <div class="grid_4 omega">
        <h2 class="confidencebutton">
            <a href="<?php echo URL_DELIVERY_INFO; ?>" title="Delivery Information">UK Delivery Information</a>
        </h2>
    </div>
    <!-- end .grid_4 -->
</div>
<!-- End confidence bar -->
<?php /* include/confidence_bar.php */