<!-- Begin product grid -->
<?php $i = 1; $class = 'alpha'; ?>
<?php foreach ($carousel_products as $product): ?>
<?php
$mod = $i % 6;
if ($mod === 0):
    $class = 'omega';
elseif ($mod === 1):
    $class = 'alpha';
else:
    $class = '';
endif;

$url = URL_PRODDETAIL_BEER . "/$product->code";
?>
<!-- Begin product slot -->
<div class="grid_2<?php echo " $class"; ?> bottle-display bottle<?php echo $mod; ?>">
    <h2><a href="<?php echo $url; ?>" title="<?php echo $product->name; ?>"><?php echo $product->name; ?></a></h2>
    <div class="bottle-img">
        <a href="<?php echo $url; ?>" title="<?php echo $product->name; ?> <?php echo $product->size_container ?>">
            <img alt="<?php echo $product->name; ?> <?php echo $product->size_container ?>" height="230" src="<?php echo URL_MEDIA_PRODUCT . "/$product->code"; ?>_small.jpg" width="140"></a>
    </div>
    <ul>
        <li><span><?php echo $product->style_name; ?></span></li>
        <li><span>ABV: <?php echo $product->abv; ?></span></li>
        <li><span><?php echo $product->size_container; ?></span></li>
    </ul>
    <div class="price">
        <p><?php echo $product->displayable_sale_price; ?></p>
    </div>
</div>
<!-- End product slot -->
<?php $i++; ?>
<?php endforeach; ?>
<!-- End product grid -->
<?php /* Location: application/views/include/home_grid.php */