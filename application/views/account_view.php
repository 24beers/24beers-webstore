<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once 'include/header_meta.php'; ?>
        <title><?php echo $page_title; ?> | Account | 24 Beers</title>
        <meta name="robots" content="noindex">
    </head>
    <body itemscope itemtype="http://schema.org/WebPage">
        <?php require_once 'include/page_header_min.php'; ?>
        <div class="container_12" id="account">
            <?php echo $content; ?>
        </div>
        <!-- End 12 column container -->
        <?php require_once 'include/page_footer_min.php'; ?>
        <script>
            $(window).load(function() {
                if ($('#linkForgotPw').length > 0) {
                    $('#linkForgotPw').click(function() {
                        $('#linkForgotPw').fadeToggle();
                        $('#forgotPw').fadeToggle();
                        return false;
                    });
                }
            });
        </script>
    </body>
</html>