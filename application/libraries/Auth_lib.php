<?php if (!defined('BASEPATH')) exit;
/**
 * <p>Library that provides authorisation methods.</p>
 * <p>When this library is called it checks if the calling controller is in
 * a list of controllers that require authorisation. If it is the request is
 * redirected to the sign-in page.</p>
 *
 * @author Ben Mullard <ben@24beers.co.uk>
 * @copyright (c) 2013, 24 Beers Limited.
 * @see application/config/auth.php
 */
class Auth_lib {

    /**
     * CodeIgniter instance.
     * @access private
     * @var resource
     */
    private $_ci;

    public function __construct() {
        $this->_ci = get_instance();

        /* Load configuration for auth. */
        $this->_ci->load->config('auth');

        $current_controller = $this->_ci->uri->slash_segment(1, 'leading');

        if ($this->_ci->uri->segment(2)) {
            $current_controller .= $this->_ci->uri->slash_segment(2, 'leading');
        }

        $this->onload_check_auth_redirect($current_controller);
    }

    /**
     * <p>Check that the visitor's session is authorised.  If the session is not
     * auth'ed and the current controller is in a list of secure controllers,
     * redirect the request to the sign-in page.</p>
     * <p>This method is private and called from the constructor so that it runs
     * when the class is instantiated.  It's designed to be used when the class
     * is autoloaded.</p>
     * @access private
     */
    private function onload_check_auth_redirect($controller) {

        /* Do nothing if visitor is already auth'ed. */
        if (is_authed()) {
            return;
        }

        /* Do nothing if current controller doesn't need auth. */
        if (in_array($controller,
                $this->_ci->config->item('public_controllers'))) {
            return;
        }

        /* Do nothing if the current controller is in a list of public
         * controller patterns. */
        if (list_matches($this->_ci->config->item('public_controller_patterns'),
                $controller)) {
            return;
        }

        /* Clear out anything that may be left in the session that
         * shouldn't be there. */
        $this->deauth_session();

        $this->_ci->load->library('user_agent');

        /* If a referring URL is available set it into flash session storage. */
        if ($this->_ci->agent->is_referral()) {
            $this->_ci->session->set_flashdata(
                    SESSION_REDIR, $this->_ci->agent->referrer());
        }

        /* Redirect to the sign-in page. */
        redirect(URL_ACCT_SIGNIN);
    }

    /**
     * Authorise the session by adding the customer code and an "authed" flag to
     * the customer's session.
     */
    public function auth_session($customer_code) {
        $this->_ci->session->set_userdata(
                SESSION_CUSTOMER_CODE, $customer_code);
        $this->_ci->session->set_userdata(SESSION_IS_AUTHED, 'true');
    }

    /**
     * De-authorise a session by unsetting the "authed" flag and removing any
     * data associated with an athorised session.
     */
    public function deauth_session() {
        $this->_ci->session->unset_userdata(SESSION_IS_AUTHED);
        $this->_ci->session->unset_userdata(SESSION_CUSTOMER_CODE);
    }
}
/* End of file Auth_lib.php */
/* Location: application/libraries/Auth_lib.php */
