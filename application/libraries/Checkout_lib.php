<?php if (!defined('BASEPATH')) { exit; }
/**
 * Library of functions for use in the checkout process.
 * 
 * @author Ben Mullard <ben@24beers.co.uk>
 * @copyright (c) 2013, 24 Beers Limited.
 */
class Checkout_lib {

    /**
     * CodeIgniter instance.
     * @access private
     * @var resource
     */
    private $_ci;

    public function __construct() {
        $this->_ci = get_instance();
    }

    /**
     * Complete the successful order.  Store the address used for the order,
     * create the shipment details.
     *
     * @param object $order
     * @param object $payment_response
     * @param string $address_code
     */
    public function complete_order($order, $payment_response, $address_code) {

        $customer_code = get_custcode_from_session();

        /* Store the order details and set the resulting UUID. */
        $new_order = $this->_order_persist($order, $customer_code);

        /* Store the transaction details. */
        $this->_transaction_persist($new_order, $payment_response);

        /* Store the customer address. */
        $delivery_address = $this->_address_persist($new_order, $customer_code, $address_code);

        /* Create a shipment from the order and persist it. */
        $this->_shipment_persist($new_order, $delivery_address);

        /* Update stock levels. */
        $this->_update_stock_levels($order);
    }

    /**
     *
     * @param type $address_code
     * @return null
     */
    public function delete_address($address_code) {

        $customer_code = get_custcode_from_session();

        $this->_ci->load->model('customer_model');

        $this->_ci->customer_model->delete_address($customer_code, $address_code);
    }

    /**
     * Generate an order object from the basket details.
     * Details that the order contains are:
     * <ul>
     *     <li>UUID</li>
     *     <li>Created by, created datetime, modified by, modified datetime</li>
     * </ul>
     *
     * @return object Order details object.
     */
    public function generate_order() {

        $this->_ci->load->library('basket_lib');
        $basket = $this->_ci->basket_lib->get_basket();

        if (sizeof($basket->items) === 0) {
            log_message(LOG_BASKET_EMPTY);
            show_error(ERROR_BASKET_EMPTY);
        }

        $subtotal = $basket->subtotal;
        $delivery_discount = get_delivery_discount();
        $delivery_total = STORE_DELIVERY_CHARGE_STANDARD - $delivery_discount;

        /* Calculate the order total.
         * Ensure order total is formatted to 2 decimal places. */
        $total_amount = number_format($subtotal + $delivery_total, 2);

        $order_lines = array();

        foreach ($basket->items as $item) {
            array_push($order_lines, (object) array(
                'line_code'    => $item->code,
                'line_name'    => $item->name,
                'line_total'   => $item->item_total,
                'product_code' => $item->product_code,
                'quantity'     => $item->quantity,
                'unit_price'   => $item->sale_price
            ));
        }

        return (object) array(
            'delivery_discount' => $delivery_discount,
            'delivery_total'    => $delivery_total,
            'discount_total'    => 0,
            'num_items'         => $basket->num_items,
            'order_lines'       => $order_lines,
            'subtotal'          => $subtotal,
            'total_amount'      => $total_amount
        );
    }

    /**
     * Get an address for the current customer by order code.
     *
     * @param  string $address_code
     * @return object Address detail object.
     */
    public function get_order_address($order_code, $customer_code='') {

        if (empty($customer_code)) {
            $customer_code = get_custcode_from_session();
        }

        $this->_ci->load->model('order_model');

        return $this->_ci->order_model->get_order_address($customer_code, $order_code);
    }

    /**
     * Get an order for the current customer by the given order code.
     */
    public function get_order_by_code($order_code) {

        $this->_ci->load->model('order_model');

        return $this->_ci->order_model->get_customer_order_by_code(get_custcode_from_session(), $order_code);
    }

    /**
     * Get an order for the current customer by the given transaction ID.
     *
     * @param  string $transaction_id
     * @return object Order detail object.
     */
    public function get_order_by_transaction($transaction_id) {

        $this->_ci->load->model('order_model');

        return $this->_ci->order_model->get_customer_order_by_transaction(
                get_custcode_from_session(), $transaction_id);
    }

    public function get_order_lines($order_code, $customer_code='') {

        if (empty($customer_code)) {
            $customer_code = get_custcode_from_session();
        }

        $this->_ci->load->model('order_model');

        return $this->_ci->order_model->get_order_lines($customer_code, $order_code);
    }

    /**
     * Get an <code>array</code> of the payment cards belonging to the current
     * customer in session.  If no array is returned, return zero, otherwise
     * return size of <code>array</code>.
     *
     * @access public
     * @return int
     */
    public function get_payment_card_count() {
        $cards = $this->get_payment_cards();

        if ($cards === NULL || !isset($cards) || empty($cards)) {
            return 0;
        }

        return sizeof($cards);
    }

    /**
     * Return an <code>array</code> of the payment cards belonging to the
     * current customer in session.  If there's no customer in session or they
     * have no payment cards, return <code>NULL</code>.
     * @return array
     */
    public function get_payment_cards() {

        $customer_code = get_custcode_from_session();

        $this->_ci->load->model('customer_model');

        $cards = $this->_ci->customer_model->get_payment_cards($customer_code);

        if (!isset($cards) || empty($cards)) {
            return NULL;
        }

        return $cards;
    }

    public function process_post_order_promotions($order) {
        $this->_ci->load->library('promotion_lib');
        $this->_ci->promotion_lib->run_reward($order);
    }

    /**
     * Send an order confirmation email to the current customer for the given
     * order code.
     *
     * @param string $order_code
     */
    public function send_order_confirmation_email($order_code) {

        $customer_code = get_custcode_from_session();

        $this->_ci->load->model('customer_model');

        $customer = $this->_ci->customer_model->get_customer_by_code(
                $customer_code);

        $this->_ci->load->model('order_model');

        $order = $this->_ci->order_model->get_customer_order_by_code(
                $customer_code, $order_code);

        $data['order'] = $order;

        $data['order_lines'] = $this->get_order_lines($order->code, $customer_code);

        $data['content'] = $this->_ci->load->view('content/email_order_confirmation', $data, TRUE);

        $this->_ci->load->library('email');

        $this->_ci->email->from(EMAIL_CUST_HELP, NAME_CUST_HELP);
        $this->_ci->email->to($customer->email);
        $this->_ci->email->cc(EMAIL_CUST_HELP);

        $subject = 'Order Confirmation for Order ' . $order->code;

        $this->_ci->email->subject($subject);

        $data['subject'] = $subject;

        $this->_ci->email->message($this->_ci->load->view('email_view', $data, TRUE));

        $this->_ci->email->send();
    }

    /**
     * Submits a payment for the given amount to the card payment provider for the card represented by the given token.
     * 
     * @param  string  $token Token that represents the card.
     * @param  object  $order Order object.
     * @return boolean True if the payment is successful.
     */
    public function submit_payment($token, $order) {

        $customer_code = get_custcode_from_session();

        /* Get the customer for this session. */
        $this->_ci->load->model('customer_model');
        $customer = $this->_ci->customer_model->get_customer_by_code($customer_code);

        if ($customer === NULL || !isset($customer)) {
            show_error(LOG_CUSTOMER_NOTFOUND);
        }

        /* Send the order to the payment provider. */
        $transaction_response = $this->_transaction_submit($order->total_amount, $token, $customer->email);

        /* Check the status of the transaction response. */
        $payment_error = $this->_process_transaction_response($transaction_response);

        /* Create a new object to hold payment reponse details. */
        $payment_response = (object) array();

        if (empty($payment_error)) {
            $payment_response->amount         = $transaction_response->amount;
            $payment_response->create_dt      = $transaction_response->created;
            $payment_response->currency       = $transaction_response->currency;
            $payment_response->fee            = $transaction_response->fee;
            $payment_response->livemode       = $transaction_response->livemode;
            $payment_response->message        = '';
            $payment_response->refund_amount  = $transaction_response->amount_refunded;
            $payment_response->success        = $transaction_response->paid;
            $payment_response->transaction_id = $transaction_response->id;
        } else {
            $payment_response->success = FALSE;
            $payment_response->message = $payment_error;
        }

        /* If there are no errors up to this point, check that the order
         * transaction has been paid. */
        if ($payment_response->success === TRUE && (!isset($transaction_response->paid) || !$transaction_response->paid)) {
            $payment_response->success = FALSE;
            $payment_response->message = 'Payment not paid.';
        }

        return $payment_response;
    }

    /**
     * Persist the delivery address for an order to the database.
     *
     * @param object $order         Order object
     * @param string $customer_code
     * @param string $address_code
     * @return object New address object.
     */
    private function _address_persist($order, $customer_code, $address_code) {

        $this->_ci->load->model('order_model');
        $this->_ci->load->model('customer_model');

        $address = $this->_ci->customer_model->get_address($customer_code, $address_code);

        $this->_ci->order_model->create_order_address($order->uuid, $address);

        return $address;
    }

    /**
     * Persist the details of the order.
     * 
     * @param  object  $order Order object.
     * @param  string  $customer_code Customer code.
     * @return object  New order object.
     */
    private function _order_persist($order, $customer_code) {

        $this->_ci->load->model('order_model');

        $new_order = $this->_ci->order_model->create_order($customer_code,
                STATUS_PAID, $order->subtotal, $order->discount_total,
                $order->delivery_total, $order->delivery_discount,
                $order->total_amount, $order->num_items);

        if (!$new_order->uuid) {
            log_message(LOG_DB_FAILED_CREATE_ORDER);
            show_error(ERROR_DB_FAILED_CREATE_OBJECT);
        }

        $order_lines = $this->_ci->order_model->create_order_lines($new_order, $order->order_lines);

        $new_order->order_lines = $order_lines;

        return $new_order;
    }

        /**
     * Parses the response from the transaction process for errors.
     *
     * @param object $response
     * @return string
     */
    private function _process_transaction_response($response) {

        log_message(LOG_LEVEL_ERROR, print_r($response, TRUE));
        if (!isset($response->error)):
            return '';
        endif;

        $notify_admin = FALSE;
        $err_title = '';
        $err_msg = '';

        switch ($response->error->type):
            case 'card_error':
                /* Card was declined. */
                $err_msg = $response->error->message;
                break;
            case 'api_error':
                /* Error communicating with API. */
                $err_title = '[Payment Error]: ' . $response->error->code;
                $err_msg = $response->error->message;
                $notify_admin = TRUE;
                break;
            case 'invalid_request_error':
                /* Programmer error. Incorrect request parameters. */
                $err_title = '[Payment Error]: ' . $response->error->code;
                $err_msg = $response->error->message;
                $notify_admin = TRUE;
                break;
            default:
                $err_title = '[Payment Error]: Unknown error.';
                $err_msg = 'Unkown payment error.';
                $notify_admin = TRUE;
                break;
        endswitch;

        if ($notify_admin):
            log_message(LOG_LEVEL_ERROR, "$err_title\n$err_msg");
            send_admin_email($err_title, $err_msg);
        endif;

        return $err_msg;
    }

    /**
     * Persist the shipment record from the given order details.
     *
     * @param object $order Order object.
     * @param object $delivery_address Address object.
     */
    private function _shipment_persist($order, $delivery_address) {

        $this->_ci->load->model('shipment_model');
        $success = $this->_ci->shipment_model->create_shipment($order, $delivery_address);

        if (!$success) {
            log_message(LOG_LEVEL_ERROR, LOG_DB_FAILED_CREATE_SHIPMENT);
            show_error(ERROR_TECHNICAL);
        }

        return $success;
    }

    /**
     * Persist the details of the transaction.
     *
     * @param  object  $order Order object.
     * @param  object  $response
     * @return integer Transaction UUID.
     */
    private function _transaction_persist($order, $response) {

        $this->_ci->load->model('order_model');

        $txn_uuid = $this->_ci->order_model->create_transaction(TXN_TYPE_CHARGE,
                $response->transaction_id, $response->create_dt, $order->uuid,
                $response->amount, $response->fee, $response->refund_amount,
                $response->currency, $response->livemode);

        if (!$txn_uuid) {
            log_message(LOG_DB_FAILED_CREATE_TRANSACTION);
            show_error(ERROR_DB_FAILED_CREATE_TRANSACTION);
        }

        return $txn_uuid;
    }

    /**
     * Submit the transaction to the payment provider and return the response.
     * 
     * @param float $amount Order amount.
     * @param string $token Payment card token.
     * @param string $description Order description.
     * @return object Response from payment provider.
     */
    private function _transaction_submit($amount, $token, $description) {

        $this->_ci->load->library('stripe_lib');

        /* Amount is submitted in GBP and needs to be converted to pennies.
         * Customer email is used as the order description. */
        $response = $this->_ci->stripe_lib->charge_card($amount * 100, $token, $description);

        /* Throw an exception if there is no response from the transaction. */
        if (empty($response)) {
            log_message(LOG_LEVEL_ERROR, LOG_TXN_NO_RESPONSE);
            show_error(ERROR_PAYMENT_FAILED);
        }

        return json_decode($response);
    }

    /**
     * Update the stock level for each item on the order.
     * @param object $order
     */
    private function _update_stock_levels($order) {
        $this->_ci->load->model('product_model');
        foreach ($order->order_lines as $order_line) {
            $this->_ci->product_model->update_stock_level($order_line->product_code, $order_line->quantity * -1);
        }
    }
}
/* End of file Checkout_lib.php */
/* Location: ./application/libraries/Checkout_lib.php */