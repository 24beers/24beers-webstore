<?php if (!defined('BASEPATH')) exit;
/**
 * Library of product handling methods.
 */
class Product_lib {

    /**
     * CodeIgniter instance.
     * @access private
     * @var resource
     */
    private $_ci = NULL;

    public function __construct() {
        $this->_ci = get_instance();
    }

    /**
     *
     * @param string  $product_code
     * @param integer $rating
     * @param string  $comment
     */
    public function add_review($product_code, $rating, $comment='') {

        if (empty($product_code)) {
            log_message(LOG_LEVEL_ERROR, LOG_PRODUCT_CODE_NOTSPECIFIED);
            return;
        }

        if (empty($rating)) {
            log_message(LOG_LEVEL_ERROR, LOG_RATING_NOTSPECIFIED);
            return;
        }

        $customer_code = get_custcode_from_session();

        /* Add review to product. */
        $this->_ci->load->model('product_model');
        $this->_ci->product_model->add_review(
                $customer_code, $product_code, $rating, $comment);

        /* Add review for customer. */
        $this->_ci->load->model('customer_model');
        $this->_ci->customer_model->add_product_review(
                $customer_code, $product_code);
    }

    /**
     * Get the product details for a given product code.
     * 
     * @access public
     * @param type $code
     * @return object Product details as an object.
     */
    public function get_product($product_code) {

        $this->_ci->load->model('product_model');

        /* Get product detail. */
        $product =
                $this->_ci->product_model->get_product_by_code($product_code);

        $product_review =
                $this->_ci->product_model->get_product_review($product_code);

        $review_sum = $product_review->sum;
        $review_count = $product_review->count;

        /* Add review data to product. */
        $product->review_avg = format_rating_avg($review_sum, $review_count);
        $product->review_count = $review_count;
        $product->reviews = $product_review->reviews;

        /* Add stock level to product. */
        $product->stock_level =
                $this->_ci->product_model->get_stock_level($product_code);

        return $product;
    }
}
/* End of file Product_lib.php */
/* Location: ./application/libraries/Product_lib.php */
