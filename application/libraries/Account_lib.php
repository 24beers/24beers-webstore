<?php if (!defined('BASEPATH')) exit;
/**
  * @author Ben Mullard <ben@24beers.co.uk>
 * @copyright (c) 2013, 24 Beers Limited.
 */
class Account_lib {

    private $_ci;

    public function __construct() {
        $this->_ci = get_instance();

        $this->_ci->load->model('customer_model');
    }

    /**
     * <p>Create a new address for a customer.</p>
     * <p>If there is no customer code in session an error is logged and
     * function exits</p>
     *
     * @param string $name
     * @param string $house
     * @param string $street
     * @param string $town
     * @param string $city
     * @param string $country
     * @param string $postcode
     * @param string $owner_code
     */
    public function create_address($name, $house, $street,
                $town, $city, $country, $postcode) {

        $customer_code = get_custcode_from_session();

        /* Generate a code to use as a key for the address. */
        $address_code = generate_address_code();

        $existing_address = $this->_ci->customer_model->get_address(
                $customer_code, $address_code);

        /**
         * TODO: This check should be part of the validation but creating the
         * address code is part of the logic. It works here as the address
         * creation will silently fail but failure means the customer already
         * has the same address anyway.
         */
        if (isset($existing_address)) {
            log_message(LOG_LEVEL_INFO, LOG_ADDRESS_EXISTS);
            return;
        }

        $this->_ci->customer_model->create_address($address_code, $name, $house,
                $street, $town, $city, $country, $postcode, $customer_code);
    }

    /**
     * Create and store information for a password reset and send an email to
     * the given email address with a link.
     *
     * @param  string  $email
     * @return boolean True if password reset lookup was successfully created
     *                 and email sent, otherwise false.
     */
    public function create_password_reset($email) {

        /* Generate a new token for the customer. */
        $token = generate_token();

        /* Store the token in a lookup with the customer email address. */
        $this->store_password_reset_token($token, $email);

        /* Email content. */
        $data['link'] = site_url(URL_ACCT_NEWPW . "/$token");
        $data['content'] = $this->_ci->load->view(
                'content/email_reset_password', $data, TRUE);

        /* Send an email with the link to reset the customer's password. */
        $this->_ci->load->library('email');

        $this->_ci->email->from(EMAIL_CUST_HELP, NAME_CUST_HELP);
        $this->_ci->email->to($email);
        $this->_ci->email->cc(EMAIL_CUST_HELP);

        $subject = 'Password reset request for ' . BRAND_NAME;

        $this->_ci->email->subject($subject);

        $data['subject'] = $subject;

        $this->_ci->email->message(
                $this->_ci->load->view('email_view', $data, TRUE));

        $this->_ci->email->send();

        return TRUE;
    }

    /**
     *
     * @param type $address_code
     * @return null
     */
    public function delete_address($address_code) {

        $customer_code = get_custcode_from_session();

        $this->_ci->customer_model->delete_address(
                $customer_code, $address_code);
    }

    /**
     * Get a given address for the current customer in session. If there is no
     * customer in session nothing is returned.
     *
     * @param string $address_code
     */
    public function get_address($address_code) {

        $customer_code = get_custcode_from_session();

        return $this->_ci->customer_model->get_address(
                $customer_code, $address_code);
    }

    /**
     * Get the details of a customer, including addresses, and return
     * encapsulated in an object.
     * 
     * @return object Customer details object encapsulation.
     */
    public function get_customer() {

        $customer_code = get_custcode_from_session();

        /* Get customer details for customer in session. */
        $customer = $this->_ci->customer_model->get_customer_by_code(
                $customer_code);

        if (!isset($customer)) {
            log_message(LOG_LEVEL_ERROR, LOG_CUSTOMER_NOTFOUND);
            show_error(ERROR_CUSTOMER_NOTFOUND);
        }

        /* Format the date of birth. */
        $customer->date_of_birth = format_date($customer->date_of_birth);

        /* Add addresses to customer details. */
        $customer->addresses =
                $this->_ci->customer_model->get_addresses($customer->code);

        /* Add orders to customer details. */
        $this->_ci->load->model('order_model');
        $customer->orders =
                $this->_ci->order_model->get_orders($customer->code);

        return $customer;
    }

    /**
     * Get addresses for the current customer in session.
     * 
     * @return array Array list of address objects belonging to the customer.
     */
    public function get_customer_addresses() {
        return $this->_ci->customer_model->get_addresses(
                get_custcode_from_session());
    }

    public function get_password_reset_email($token) {
        return $this->_ci->customer_model->get_password_reset_lookup($token);
    }

    /**
     * Store the customer's email and token in a lookup.
     * 
     * @param string $email
     * @param string $token
     */
    public function store_password_reset_token($token, $email) {
        $this->_ci->customer_model->create_password_reset_lookup(
                $token, $email);
    }

    /**
     * Update the customer's personal details.
     *
     * @param string $name
     * @param string $email
     * @param string $phone
     * @param string $alt_phone
     */
    public function update_customer($name, $email, $phone, $alt_phone, $dob) {

        $customer_code = get_custcode_from_session();

        $success = $this->_ci->customer_model->update_customer(
                $customer_code, $name, $email, $phone, $alt_phone, $dob);

        if (!$success) {
            show_error(ERROR_FAILED_UPDATE_CUSTOMER);
        }
    }

    /**
     * Update customer's password.
     * 
     * @param string $email
     * @param string $password
     */
    public function update_password($email, $password) {

        $customer = $this->_ci->customer_model->get_customer_by_email($email);

        return $this->_ci->customer_model->update_password(
                $customer->code, $password);
    }
}
/* End of file Account_lib.php */
/* Location: ./application/libraries/Account_lib.php */