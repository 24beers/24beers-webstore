<?php if (! defined('BASEPATH')) { exit; }
/**
 * CodeIgniter Redis
 *
 * A library to interact with the Redis server.
 *
 * @see ../config/redis.php
 */
class Redis {

    /**
     * CodeIgniter instance
     * @var object
     */
    private $_ci;

    /**
     * Socket handle to the Redis server
     * @var handle
     */
    private $_connection;

    /**
     * User to delimiter arguments in the Redis unified request protocol
     * @var string
     */
    const CRLF = "\r\n";

    public function __construct() {

        $this->_ci = get_instance();
        $this->_ci->load->config('redis');

        $db = $this->_ci->config->item('redis_db');
        $host = $this->_ci->config->item('redis_host');
        $port = $this->_ci->config->item('redis_port');
        $errno = 0;
        $errstr = '';
 
        // Connect to Redis
        $this->_connection = fsockopen($host, $port, $errno, $errstr, 3);

        // Display an error message if connection failed
        if (!$this->_connection) {
            $this->connection_error($host, $port);
        }

        $this->_auth();

        if (!empty($db)) {
            $this->command("select $db");
        }
    }

    /**
     * Catches all undefined methods
     * @param	string	method that was called
     * @param	mixed	arguments that were passed
     * @return 	mixed
     */
    public function __call($method, $arguments) {
        $request = $this->_encode_request($method, $arguments);
        return $this->_write_request($request);
    }

    /**
     * Generic command function, just like redis-cli
     * @param	string	full command as a string
     * @return 	mixed
     */
    public function command($string) {
        $slices = explode(' ', $string);
        $request = $this->_encode_request($slices[0], array_slice($slices, 1));

        return $this->_write_request($request);
    }

    /**
     * Runs the AUTH command when password is set
     * @return 	void
     */
    private function _auth() {
        $password = $this->_ci->config->item('redis_password');
        if (!empty($password)) {
            $request = $this->_encode_request('AUTH ' . $password);
            if (!$this->_write_request($request)) {
                show_error('Could not connect to Redis, invalid password');
            }
        }
    }

    /**
     * Write the formatted request to the socket
     * @param	string 	request to be written
     * @return 	mixed
     */
    private function _write_request($request) {
        fwrite($this->_connection, $request);
        return $this->_read_request();
    }

    /**
     * Route each response to the appropriate interpreter
     * @return 	mixed
     */
    private function _read_request() {

        $type = fgetc($this->_connection);

        switch ($type) {
            case '+': return $this->_single_line_reply();
            case '-': return $this->_error_reply();
            case ':': return $this->_integer_reply();
            case '$': return $this->_bulk_reply();
            case '*': return $this->_multi_bulk_reply();
            default: return FALSE;
        }
    }

    /**
     * Reads the reply before the EOF
     * @return 	mixed
     */
    private function _single_line_reply() {
        $value = trim(fgets($this->_connection));
        return $value;
    }

    /**
     * Write error to log and return false
     * @return 	bool
     */
    private function _error_reply() {
        // Extract the error message
        $error = substr(fgets($this->_connection), 4);
        log_message(LOG_LEVEL_ERROR,
                'Redis server returned an error: ' . $error);

        return FALSE;
    }

    /**
     * Returns an integer reply
     * @return 	int
     */
    private function _integer_reply() {
        return (int) fgets($this->_connection);
    }

    /**
     * Reads to amount of bits to be read and returns value within
     * the pointer and the ending delimiter
     * @return 	string
     */
    private function _bulk_reply() {
        // Get the amount of bits to be read
        $value_length = (int) fgets($this->_connection);

        if ($value_length <= 0) { return NULL; }
        $response = rtrim(fread($this->_connection, $value_length + 1));

        // Make sure to remove the new line and carriage from the socket buffer
        fgets($this->_connection);
        return isset($response) ? $response : FALSE;
    }

    /**
     * Reads n bulk replies and return them as an array
     * @return 	array
     */
    private function _multi_bulk_reply() {
        $total_values = (int) fgets($this->_connection);
        for ($i = 0; $i < $total_values; $i++) {
            fgets($this->_connection, 2);
            $response[] = $this->_bulk_reply();
        }
        return isset($response) ? $response : FALSE;
    }

    /**
     * Encode plain-text request to Redis protocol format
     * @link 	http://redis.io/topics/protocol
     * @param 	string 	request in plain-text
     * @param   string  additional data (string or array, depending on the request)
     * @return 	string 	encoded according to Redis protocol
     */
    private function _encode_request($method, $arguments = array()) {
        $argument_count = $this->_count_arguments($arguments);
        $request = '*' . $argument_count . self::CRLF;
        $request .= '$' . strlen($method) . self::CRLF . $method . self::CRLF;

        if ($argument_count === 1) { return $request; }

        foreach ($arguments as $argument) {
            if (!is_array($argument)) {
                $request .= '$' . strlen($argument) . self::CRLF . $argument .
                        self::CRLF;
                continue;
            }

            $is_associative_array = self::is_associative_array($argument);
            foreach ($argument as $key => $value) {
                if ($is_associative_array) {
                    $request .= '$' . strlen($key) . self::CRLF . $key .
                            self::CRLF;
                }
                $request .= '$' . strlen($value) . self::CRLF . $value .
                        self::CRLF;
            }
        }
        return $request;
    }

    /**
     * Count the amount of arguments we need to pass to Redis while taking
     * into consideration lists, hashes and strings
     */
    private function _count_arguments($arguments) {
        $argument_count = 1;
        foreach ($arguments as $argument) {
            if (is_array($argument) AND self::is_associative_array($argument)) {
                $argument_count += (count($argument) * 2);
            } elseif (is_array($argument)) {
                $argument_count += count($argument);
            } else {
                $argument_count++;
            }
        }
        return $argument_count;
    }

    public function exec() {
        return $this->command("exec");
    }

    /**
     * Overrides the default Redis response, so we can return a nice array
     * of the server info instead of a nasty string.
     * @return 	array
     */
    public function info() {
        $response = $this->command('INFO');
        $data = array();
        $lines = explode(self::CRLF, $response);
        foreach ($lines as $line) {
            $parts = explode(':', $line);
            if (isset($parts[1])) {
                $data[$parts[0]] = $parts[1];
            }
        }
        return $data;
    }

    public function multi() {
        $this->command("multi");
    }

    public function watch($key) {
        $this->command("watch $key");
    }

    /**
     * Kill the connection
     * @return 	void
     */
    function __destruct() {
        if ($this->_connection) {
            fclose($this->_connection);
        }
    }

    /**
     * Checkes whether the array has only intergers as key, starting at
     * index 0, untill the array length - 1.
     * @param 	array 	the array to be checked
     * @return 	bool
     */
    public static function is_associative_array($array) {
        $keys = array_keys($array);

        if (min($keys) === 0 AND max($keys) === count($array) - 1) {
            return FALSE;
        }
        return TRUE;
    }

    private function connection_error($host, $port) {
        log_message(
                LOG_LEVEL_ERROR, sprintf(LOG_FAILED_CONNECT, "$host:$port"));
        show_error(ERROR_FAILED_CONNECT);
    }
}