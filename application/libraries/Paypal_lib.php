<?php if (!defined('BASEPATH')) exit;

/**
 * PayPal library to interact with PayPal payment gateway.
 */
class Paypal_lib {

    const METHOD = 'SetExpressCheckout';

    private $_ci;

    private $endpoint_url;

    public function __construct() {
        $this->_ci = get_instance();
        $this->_ci->load->config('paypal');

        $this->endpoint_url = 'https://' .
                $this->config->item('pp_api_endpoint') . '/nvp';
    }

    private function call($data) {

        $ch = curl_init($this->endpoint_url);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
        ));

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        return json_decode(curl_exec($ch));
        /*curl -s --insecure https://api-3t.sandbox.paypal.com/nvp -d
"USER=<callerID>                         # User ID of the PayPal caller account
&PWD=<callerPswd>                        # Password of the caller account
&SIGNATURE=<callerSig>                   # Signature of the caller account
&METHOD=SetExpressCheckout
&VERSION=93
&PAYMENTREQUEST_0_PAYMENTACTION=SALE     # type of payment
&PAYMENTREQUEST_0_AMT=19.95              # amount of transaction
&PAYMENTREQUEST_0_CURRENCYCODE=USD       # currency of transaction
&RETURNURL=http://www.example.com/success.html  # URL of your payment confirmation page
&CANCELURL=http://www.example.com/cancel.html"  # URL redirect if customer cancels payment*/
    }
}
/* End of file Paypal_lib.php */
/* Location: ./application/libraries/Paypal_lib.php */