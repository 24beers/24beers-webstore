<?php if (!defined('BASEPATH')) { exit; }
/**
 * Library of functions for use with promotions.
 *
 * @author Ben Mullard <ben@24beers.co.uk>
 * @copyright (c) 2014, 24 Beers Limited.
 */
class Promotion_lib {

    /**
     * @access private
     * @var object CodeIgniter instance.
     */
    private $ci;

    public function __construct() {
        $this->ci =& get_instance();
    }

    public function get_basket_promotion() {
        $promotion = array();

        $partner_code = $this->ci->session->userdata(SESSION_PARTNER_CODE);

        if (!empty($partner_code)) {
            switch ($partner_code):
                case (PARTNER_CODE_NORFOLKMAG):
                    $promotion = $this->get_basket_promotion_norfolkmag();
                    break;
                default: break;
            endswitch;
        }

        return $promotion;
    }

    /**
     * <p>Run the "Beer Kitty" promotion.</p>
     * <p>If the customer's order value is greater than the average order value
     * across all orders pay the difference or 5% of the order total (whichever
     * is less) into a credit account owned by the customer.</p>
     * 
     * @param string $customer_code
     * @param string $order_code
     * @return void
     */
    public function run_kitty($customer_code, $order_code) {
        $this->ci->load->model('order_model');
        $order = $this->ci->order_model->get_customer_order_by_code(
                $customer_code, $order_code);

        if (empty($order)) {
            log_message(LOG_LEVEL_ERROR, format_message(
                    LOG_PRODUCT_NOTFOUND, array($customer_code, $order_code)));
            return;
        }

        $avg_order_value = $this->get_avg_order_value();

        if ($this->order_is_eligible($order, $avg_order_value)) {
            $credit = $this->get_credit($order->subtotal, $avg_order_value);
            $this->apply_credit($order->owned_by, $credit);
        }
    }

    public function run_reward($order) {
        // TODO: Not yet in use.
        //print_r($order);
        //$this->ci->load->model('order_model');
    }

    /**
     * Apply the given credit to the correct account for the customer.
     * @param string $customer_code
     * @param real $credit
     */
    private function apply_credit($customer_code, $credit) {
        $this->ci->load->model('account_model');
        $this->ci->account_model->load_customer_account(
                $customer_code, ACCT_TYPE_KITTY);
        $this->ci->account_model->credit_account($credit);
        $this->ci->account_model->save();
    }

    /**
     * Get the average order value from the dashboard.
     * @return real The average order value.
     */
    private function get_avg_order_value() {
        $this->ci->load->library('elasticsearch');
        $this->ci->elasticsearch->set_index(DSO_INDEX_DASHBOARD);
        $this->ci->elasticsearch->set_type(DSO_TYPE_ORDER);
        $customer_spend = $this->ci->elasticsearch->get(DSO_FIELD_CUSTSPEND);
        return $customer_spend->_source->average_spend;
    }

    /**
     * <p>Get the amount to credit to the customer. Either difference between order
     * value and average value of all orders or 5% of order value, whichever is
     * lowest.</p>
     * @param  real $order_value
     * @param  real $avg_order_value
     * @return real Amount to credit to customer.
     */
    private function get_credit($order_value, $avg_order_value) {
        $diff = $order_value - $avg_order_value;
        $pc = $order_value * 0.05;
        return ($diff < $pc) ? $diff : $pc;
    }

    private function get_basket_promotion_norfolkmag() {
        $promotion = array();
        $this->ci->load->helper('date');
        if (now() <= 1396310399) {
            $promotion['delivery_discount'] = STORE_DELIVERY_CHARGE_STANDARD;
        }
        return $promotion;
    }

    /**
     * Compare this order value to average value of all orders.
     * @param  object  $order Order object.
     * @return boolean True if order value is greater than average order value.
     */
    private function order_is_eligible($order, $avg_order_value) {
        if (isset($order) && is_object($order) && isset($order->subtotal)) {
            return $order->subtotal > $avg_order_value;
        }
        return FALSE;
    }
}
/* End of file Promotion_lib.php */
/* Location: ./application/libraries/Promotion_lib.php */