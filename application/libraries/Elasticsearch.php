<?php if (!defined('BASEPATH')) { exit; }
/**
 * Elasticsearch
 * A CodeIgniter library to interact with ElasticSearch
 */
class Elasticsearch {

    /**
     * CodeIgniter instance.
     * @var object
     */
    private $_ci;

    /**
     * Name of search index to query.
     * @var string
     */
    private $index;

    /**
     * Search query.
     * @var string
     */
    private $query;

        /**
     * Server protocol, hostname and port.
     * @var string
     */
    private $server;

    /**
     * Item type to query.
     * @var string
     */
    private $type;

    function __construct() {
        log_message('debug', 'ElasticSearch class initialized');

        $this->_ci =& get_instance();
        $this->_ci->load->config('elasticsearch');

        $this->server = 'http://' . $this->_ci->config->item('es_host') .
                ':' . $this->_ci->config->item('es_port');
        $this->index = $this->_ci->config->item('es_index');
    }

    function set_index($index) {
        $this->index = $index;
    }

    function set_query($value) {
        $this->query = (empty($value)) ? '*' : $value;
    }

    function set_type($value) {
        $this->type = $value;
    }

    /**
     * Call the ElasticSearch REST api.
     * 
     * @param  string $path
     * @param  string $method
     * @param  string $data   Data to send to the API in JSON format.
     * @return object JSON data deserialised to a standard object.
     */
    function call($path, $method='GET', $data='') {
        if (!$this->index) { show_error('Index name not specified'); }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "$this->server/$this->index/$path");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->get_http_headers($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);

        switch ($method):
            case 'GET':
                curl_setopt($ch, CURLOPT_POST, FALSE);
                break;
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                break;
            case 'DELETE':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
        endswitch;

        return json_decode(curl_exec($ch));
    }

    /**
     * Get the corresponding document for a given ID.
     * @param string $id
     * @return object
     */
    function get($id) {
        return $this->call($this->type . '/' . $id);
    }

    //curl -X PUT http://localhost:9200/{INDEX}/
    //this function is to create an index
    function create() {
        $this->call(NULL, 'PUT');
    }

    //curl -X GET http://localhost:9200/{INDEX}/_status
    function status() {
        return $this->call('_status');
    }

    //curl -X GET http://localhost:9200/{INDEX}/{TYPE}/_count -d {matchAll:{}}
    function count() {
        return $this->call($this->type . '/_count?' .
                http_build_query(array(NULL => '{matchAll:{}}')));
    }

    //curl -X PUT http://localhost:9200/{INDEX}/{TYPE}/_mapping -d ...
    function map($data) {
        return $this->call($this->type . '/_mapping', 'PUT', $data);
    }

    //curl -X PUT http://localhost:9200/{INDEX}/{TYPE}/{ID} -d ...
    function add($id, $data) {
        return $this->call($this->type . '/' . $id, 'PUT', $data);
    }

    //curl -X DELETE http://localhost:9200/{INDEX}/
    //delete an indexed item by ID
    function delete($id) {
        return $this->call($this->type . '/' . $id, 'DELETE');
    }

    //curl -X GET http://localhost:9200/{INDEX}/{TYPE}/_search?q= ...
    function query() {
        return $this->call($this->type . '/_search?' .
                http_build_query(array('q' => $this->query)));
    }

    function query_wresultSize($size = 999) {
        return $this->call($this->type . '/_search?' .
                http_build_query(array('q' => $this->query, 'size' => $size)));
    }

    function query_all() {
        return $this->call('_search?' .
                http_build_query(array('q' => $this->query)));
    }

    function query_all_wresultSize($size = 999) {
        return $this->call('_search?' .
                http_build_query(array('q' => $this->query, 'size' => $size)));
    }

    /**
     * Get faceted search results based on given query and facet data.
     * @param array $query_data
     * @param array $facet_data
     * @return object
     */
    function facet_search($query_data, $facet_data) {
        $data = json_encode(array(
            'query' => $query_data,
            'facets' => $facet_data
        ));
        return $this->call($this->type . '/_search?size=999', 'POST', $data);
    }

    function term_query($field, $term, $filters=NULL) {
        $query = array('query' => array('term' => array($field => $term)));

        $filtered_query = NULL;
        if (!is_null($filters)) {
            $filtered_query = array('filtered' => array(
                $query,
                array('filter' => array('and' => array(
                    $filters
                )))
            ));
        }

        $query_data = is_null($filtered_query) ? $query : $filtered_query;

        return $this->call("$this->type/_search?size=999", 'POST', json_encode($query_data));
    }

    private function get_http_headers($data) {
        $headers = array('Accept: application/json',
            'Content-Type: application/json; charset=utf-8');
        if (!empty($data)) {
            array_push($headers, 'Content-Length: ' . strlen($data));
        }
        return $headers;
    }
}
/* End of file elasticsearch.php */
/* Location: ./application/libraries/elasticsearch.php */