<?php
class Basket_lib {

    /**
     * CodeIgniter instance.
     * @access private
     * @var resource
     */
    private $_ci;

    /**
     * Basket key to identify customer's peristed basket.
     * @access private
     * @var string
     */
    private $basket_key;

    public function __construct() {
        $this->_ci = get_instance();

        $this->_ci->load->model('basket_model');

        /* Initialise the basket key. */
        $this->init_basket_key();
    }

    /**
     * Add SKU item to the customer's basket. If the customer is logged in their
     * basket will be persisted to the database, otherwise it's just held
     * in their session.
     *
     * TODO: Store subtotals in basket and recalculate when new item is added.
     * 
     * @param  string  $code
     * @param  integer $quantity
     * @return boolean True if item is successfully added to basket,
     *                 otherwise false.
     */
    public function add_item_to_basket($code, $quantity) {

        $this->_ci->load->model('product_model');
        $product = $this->_ci->product_model->get_product_by_sku_code($code);

        if (isset($product)) {
            return $this->_ci->basket_model->add_item_to_basket(
                    $this->basket_key, $code, $product->name, $quantity,
                    $product->sale_price);
        }

        return FALSE;
    }

    /**
     * Decrease the quantity of given SKU in the basket by one. Does nothing
     * if given SKU code does not exist.
     *
     * @param string $code
     */
    public function decrease_item_in_basket($code) {

        $this->_ci->load->model('product_model');
        $product = $this->_ci->product_model->get_product_by_sku_code($code);

        if (isset($product)) {
            $this->_ci->basket_model->decrease_item_quantity(
                    $this->basket_key, $code);
        }
    }

    /**
     * Empty contents of the basket and remove basket key from datastore.
     */
    public function empty_basket() {
        $this->_ci->basket_model->delete_basket($this->basket_key);
    }

    /**
     * <p>Gets the basket data from the datasource using the basket key and returns
     * it as an object.</p>
     * TODO: Move this into <code>Basket_model</code>.
     * @return object Basket contents as an object.
     */
    public function get_basket() {

        $basket_items =
                $this->_ci->basket_model->get_basket_items($this->basket_key);
        $num_items = 0;
        $subtotal = 0;

        /* Load product model to get product detail for basket items. */
        $this->_ci->load->model('product_model');

        foreach ($basket_items as $item) {
            $num_items += $item->quantity;
            $item->item_total = $item->sale_price * $item->quantity;
            $subtotal += $item->item_total;

            /* Get product relating to basket item. */
            $product = $this->_ci->product_model->get_product_by_sku_code(
                    $item->code);

            /* Add product details to basket item. */
            $item->abv = $product->abv;
            $item->brewery_name = $product->brewery_name;
            $item->description = $product->description;
            $item->product_code = $product->code;
            $item->size_container = $product->size_container;
            $item->style_name = $product->style_name;
        }

        $delivery_discount = get_delivery_discount();

        /* TODO: Allow customers to choose their delivery method. */
        $delivery_charge = STORE_DELIVERY_CHARGE_STANDARD - $delivery_discount;

        return (object) array(
            'delivery_charge'   => $delivery_charge,
            'delivery_discount' => $delivery_discount,
            'items'             => $basket_items,
            'num_items'         => $num_items,
            'subtotal'          => $subtotal,
            'total'             => $subtotal + $delivery_charge
        );
    }

    /**
     * Gets the content of a customer's basket in JSON format.
     * @return string Basket contents data in JSON format.
     */
    public function get_basket_data() {

        /* This code is disabled for now. */
        /*if (is_authed()) {
            $this->load->model('basket_model');
            return json_encode(
                    $this->basket_model->get_basket_items($this->basket_key));
        }*/

        return json_encode($this->get_basket());
    }

    /**
     * Generate a unique basket key with high entropy, making it very difficult
     * to predict.
     *
     * @access private
     * @return string Unique basket key.
     */
    private function generate_basket_key() {

        /* Generate random number. */
        $key = mt_rand(0, mt_getrandmax());

        /* Make key even more secure by adding the visitor's IP. */
	$key .= $this->_ci->input->ip_address();

        /* Make the key portable by converting it to a high-entropy MD5 hash. */
        return md5(uniqid($key, TRUE));
    }

    /**
     * Increase the quantity of given SKU in the basket by one. Does nothing
     * if given SKU code does not exist.
     *
     * @param string $code
     */
    public function increase_item_in_basket($code) {

        $this->_ci->load->model('product_model');
        $product = $this->_ci->product_model->get_product_by_sku_code($code);

        if (isset($product)) {
            $this->_ci->basket_model->increase_item_quantity(
                    $this->basket_key, $code);
        }
    }

    /**
     * Delete a single item from the basket.
     * 
     * @param string $code
     */
    public function remove_basket_item($code) {
        $this->_ci->basket_model->delete_basket_item(
                $this->basket_key, $code);
    }

    /**
     * Initialise the basket key.  If the customer is authed use their customer
     * code as the basket key, otherise use their session ID.
     *
     * @access private
     */
    private function init_basket_key() {

        $basket_key = $this->_ci->session->userdata(SESSION_BASKET_KEY);

        /* If there's no basket key in session use the current session ID. */
        if (empty($basket_key)) {
            $basket_key = $this->generate_basket_key();
            $this->_ci->session->set_userdata(SESSION_BASKET_KEY, $basket_key);
        }

        $this->basket_key = $basket_key;
    }
}
/* End of file Basket_lib.php */
/* Location: ./application/libraries/Basket_lib.php */