<?php if (!defined('BASEPATH')) { exit; }
/**
 * Library of functions for use in the shop search.
 *
 * @author Ben Mullard <ben@24beers.co.uk>
 * @copyright (c) 2013, 24 Beers Limited.
 */
class Search_lib {

    /**
     * CodeIgniter instance.
     * @access private
     * @var    resouce
     */
    private $_ci;

    public function __construct() {
        $this->_ci = get_instance();
    }

    public function get_facet_nav() {
        $this->_ci->load->model('search_model');
        $facet_result = $this->_ci->search_model->get_all_facet_result();
        return $facet_result;
    }
}
/* End of file Search_lib.php */
/* Location: ./application/models/Search_lib.php */