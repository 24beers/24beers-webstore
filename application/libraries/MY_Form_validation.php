<?php if (!defined('BASEPATH')) exit;

/**
 * Class extends CodeIgniter class to provide extra form validation.
 *
 * @author Ben Mullard <ben@24beers.co.uk>
 * @copyright (c) 2013, 24 Beers Limited.
 */
class MY_Form_validation extends CI_Form_validation {

    public function __construct($rules = array()) {

        parent::__construct($rules);

        $this->_error_prefix = '<span class="error">';
        $this->_error_suffix = '</span>';
    }

    /**
     * Check that a given address code relates to an existing address and that
     * address is owned by the current customer.
     *
     * @param  string  $address_code
     * @return boolean True if address exists and is owned by current customer.
     */
    public function address_exists_ownedbycust($address_code) {

        $this->CI->load->library('account_lib');
        $address = $this->CI->account_lib->get_address($address_code);

        if (isset($address)) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Returns the inverse of <code>email_exists</code>. For use when you want
     * to validate that an email address doesn't exist, for example when
     * registering a new email address.
     * @param string $email
     * @return boolean
     */
    public function email_not_exists($email) {

        if ($this->email_exists($email)) {
            $this->CI->form_validation->set_message(
                    'email_not_exists', ERROR_EMAIL_EXISTS);
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Check if the email already exists in the datastore. Return true if it
     * does exist or set an error and return false if it doesn't. For use when
     * you want to validate that an email address already exists, for example
     * when requesting a password reset.
     * @param string $email
     * @return boolean True if email exists, otherwise false.
     */
    public function email_exists($email) {
        $this->CI->load->model('Customer_model');

        $customer = $this->CI->Customer_model->get_customer_by_email($email);

        if (empty($customer)) {
            $this->CI->form_validation->set_message(
                    'email_exists', ERROR_EMAIL_NOTEXISTS);
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Validate that the password reset token exists.
     * 
     * @param  string  $token Password reset token.
     * @return boolean false if token does not exist, otherwise true.
     */
    public function password_reset_token_exists($token) {

        $this->CI->load->model('customer_model');
        $customer_code =
                $this->CI->customer_model->get_password_reset_lookup($token);

        if (empty($customer_code)) {
            $this->CI->form_validation->set_message(
                    'password_reset_token_exists', ERROR_RESET_TOKEN_NOTEXISTS);
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Check that the given password confirmation matches the MD5 of the posted
     * <code>password</code> form field.
     * 
     * @param string $passwordconf
     * @return boolean False if the passwords do not match, otherwise true.
     */
    public function passwords_match($passwordconf) {

        if ($passwordconf != md5($this->CI->input->post('password'))) {
            $this->CI->form_validation->set_message(
                    'passwords_match', ERROR_PASSWORDS_NOTMATCH);
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Check if the given payment token already exists in the customer's
     * session.  Returns <code>TRUE</code> if there's no token in session or it
     * doesn't match the given token.
     * 
     * @param string $token Payment token to validate.
     * @return boolean True if there is no payment token in session or existing
     * token doesn't match given token.
     */
    public function payment_token_not_exists($token) {
        $existing_token = $this->CI->session->userdata(SESSION_PAYMENT_TOKEN);

        if (empty($existing_token) || $existing_token != $token) {
            return TRUE;
        }

        $this->CI->form_validation->set_message(
                'payment_token_not_exists', ERROR_PAYMENT_TOKEN_EXISTS);

        return FALSE;
    }

    /**
     * <p>Check that the given address code doesn't already exist. Returns
     * false if the address code already exists.</p>
     * <p>If the customer code isn't available in the session the function also
     * returns false.</p>
     *
     * @param  string  $address_code
     * @return boolean False if an address isn't found for the given code,
     *                 otherwise true.
     */
    public function unique_address($address_code) {
        $customer_code = get_custcode_from_session();

        $this->CI->load->model('customer_model');

        $address = $this->CI->customer_model->get_address(
                $customer_code, $address_code);

        if (isset($address)) {
            return FALSE;
        }

        return TRUE;
    }

    /**
     *
     * @param  string  $cardnum
     * @return boolean True if card number is valid.
     */
    public function valid_card_number($cardnum) {
        $this->CI->load->heper('payment_card');

        if (card_number_valid(card_number_clean($cardnum))) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Validate that the given date of birth is correct and they are over 18.
     * @access protected
     * @param  string  $dob
     * @return boolean True if date of birth is a valid date and over 18.
     */
    public function valid_dob($dob) {
        $dt_grp_d = '(0?[1-9]|[12][0-9]|3[01])';
        $dt_grp_m = '(0?[1-9]|1[012])';
        $dt_grp_y = '((19|20)[0-9]{2})';

        if (!preg_match("/^$dt_grp_y\-$dt_grp_m\-$dt_grp_d$/", $dob) &&
                !preg_match("/^$dt_grp_d\/$dt_grp_m\/$dt_grp_y$/", $dob)) {
            $this->set_message('valid_dob', ERROR_DATE_WRONG_FORMAT);
            return FALSE;
        }

        if (strpos($dob, '-') !== FALSE) {
            list($y, $m, $d) = explode('-', $dob);
        } elseif (strpos($dob, '/') !== FALSE) {
            list($d, $m, $y) = explode('/', $dob);
        }

        if (empty($d) || empty($m) || empty($y)) {
            $this->set_message('valid_dob', ERROR_DATE_WRONG_FORMAT);
            return FALSE;
        }

        if (strlen($d) === 1) {
            $d = 0 . $d;
        }

        if (!checkdate($m, $d, $y)) {
            $this->set_message('valid_dob', ERROR_DATE_INVALID);
            return FALSE;
        }

        if (strtotime($dob) > strtotime('-18 year', time())) {
            $this->set_message('valid_dob', ERROR_DOB_TOO_YOUNG);
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Checks that a product exists for the given code.
     *
     * @access public
     * @param  string  $product_code
     * @return boolean True if product exists, otherwise false.
     */
    public function valid_product($product_code) {

        $this->CI->load->model('product_model');
        $product = $this->CI->product_model->get_product_by_code($product_code);

        if (isset($product)) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Checks that a product exists for the given SKU code.
     *
     * @access public
     * @param  string  $sku_code
     * @return boolean True if product exists, otherwise false.
     */
    public function valid_sku($sku_code) {
        $this->CI->load->model('product_model');
        $product = $this->CI->product_model->get_product_by_sku_code($sku_code);

        if (isset($product)) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * <p>Check that the given string is a valid UK phone number.</p>
     * <p>Remove everything from phone number that isn't a digit and match it
     * against a regex pattern.</p>
     *
     * @access protected
     * @param string $phone
     * @return boolean Return true if given value is a valid UK phone number.
     */
    public function valid_uk_phone($phone) {

        if (preg_match("/^0[0-9]{10}$/",
                preg_replace('/[^0-9]/', '', $phone))) {
            return TRUE;
        }

        $this->set_message('valid_uk_phone', ERROR_PHONE_INVALID);

        return FALSE;
    }

    /**
     * Validate a UK postcode by matching a regular expression.
     * Expression from Wikipedia:
     * @link: http://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom
     *
     * Note: Remember to strtoupper() the postcode before inserting it into
     * the database.
     */
    public function valid_uk_postcode($postcode) {

        if (preg_match('/^(GIR 0AA)|(((A[BL]|B[ABDHLNRSTX]?|C[ABFHMORTVW]|D[ADEGHLNTY]|E[HNX]?|F[KY]|G[LUY]?|H[ADGPRSUX]|I[GMPV]|JE|K[ATWY]|L[ADELNSU]?|M[EKL]?|N[EGNPRW]?|O[LX]|P[AEHLOR]|R[GHM]|S[AEGKLMNOPRSTY]?|T[ADFNQRSW]|UB|W[ADFNRSV]|YO|ZE)[1-9]?[0-9]|((E|N|NW|SE|SW|W)1|EC[1-4]|WC[12])[A-HJKMNPR-Y]|(SW|W)([2-9]|[1-9][0-9])|EC[1-9][0-9])[0-9][ABD-HJLNP-UW-Z]{2})$/',
                preg_replace('/\W+/', '', strtoupper($postcode)))) {
            return TRUE;
        }

        $this->set_message('valid_uk_postcode', ERROR_POSTCODE_INVALID);
        return FALSE;
    }
}
/* End of file MY_Form_validation.php */
/* Location: application/libraries/MY_Form_validation.php */