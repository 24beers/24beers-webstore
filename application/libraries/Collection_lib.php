<?php if (!defined('BASEPATH')) { exit; }
/**
 * Library of functions for use with product collections.
 * 
 * @author Ben Mullard <ben@24beers.co.uk>
 * @copyright (c) 2013, 24 Beers Limited.
 */
class Collection_lib {

    /**
     * CodeIgniter instance.
     * @access private
     * @var resource
     */
    private $_ci;

    public function __construct() {

        $this->_ci = get_instance();

        $this->_ci->load->model('search_model');
    }

    /**
     * Get the product collection for the given collection code.
     *
     * @param string $collection_code
     * @return type Description
     */
    public function get_collection($collection_code) {
        $this->_ci->load->model('search_model');
        $products = $this->_ci->search_model->get_result_by_collection(
                $collection_code);

        if (count($products) === 0) {
            $msg = 'No products found for collection: ' . $collection_code;
            log_message(LOG_LEVEL_ERROR, $msg);
            send_admin_email('Empty Collection!', $msg);
        }

        return $products;
    }

    /**
     * Get the 24 Beers product collection.
     * @return type
     */
    public function get_collection_24beers() {
        $collection = $this->get_collection(COLLECTION_24BEERS);

        if (($count = count($collection)) != 24) {
            log_message(LOG_LEVEL_ERROR,
                    '24 Beers collection does not contain 24 products.');

            if ($count > 24) {
                $collection = array_slice($collection, 0, 24);
            }
        }

        return $collection;
    }
}
/* End of file Collection_lib.php */
/* Location: application/libraries/Collection_lib.php */
