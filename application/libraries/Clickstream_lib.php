<?php if (!defined('BASEPATH')) exit;
/**
 * <p>Library that provides access to clickstream data and methods.</p>
 * <p>When this library is called it analyses visitor session and browser data
 * and tracks the path of visitors through the web site.</p>
 *
 * @author Ben Mullard <ben@24beers.co.uk>
 * @copyright (c) 2013, 24 Beers Limited.
 * @see application/config/clickstream.php
 */
class Clickstream_lib {

    /**
     * CodeIgniter instance.
     * @access private
     * @var resource
     */
    private $_ci;

    /**
     *
     * @var type 
     */
    private $session_uuid;

    public function __construct() {
        $this->_ci = get_instance();

        /* Load clickstream configuration. */
        $this->_ci->load->config('clickstream');

        $current_controller = $this->_ci->uri->slash_segment(1);

        if ($this->_ci->uri->segment(2)) {
            $current_controller .= $this->_ci->uri->slash_segment(2);
        }

        $this->get_session_uuid();
    }

    /**
     * <p>Check that the visitor's session is authorised.  If the session is not
     * auth'ed and the current controller is in a list of secure controllers,
     * redirect the request to the sign-in page.</p>
     * <p>This method is private and called from the constructor so that it runs
     * when the class is instantiated.  It's designed to be used when the class
     * is autoloaded.</p>
     * @access private
     */
    private function get_session_uuid() {

        $session_uuid = '';
    }
}
/* End of file Auth_lib.php */
/* Location: application/libraries/Auth_lib.php */
