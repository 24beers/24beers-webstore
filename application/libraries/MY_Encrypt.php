<?php if (!defined('BASEPATH')) exit;

class MY_Encrypt extends CI_Encrypt {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Encrypt a given string using SHA512 encryption.
     * @param String $str
     */
    public function encrypt($str, $salt='') {

        if (empty($salt)) {
            $salt = $this->generate_sha512_salt();
        }

        return crypt($str, $salt);
    }

    /**
     * Generate a random alpha-numeric string to the given length.
     * @param int $len
     * @return string
     */
    public function generate_random_string($len) {
        $chr = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $str = '';

        for ($i = 0; $i < $len; $i++) {
            $str .= $chr[rand(0, strlen($chr) - 1)];
        }

        return $str;
    }

    /**
     * Genrate a salt for the SHA512 encryption algorithm.
     * @return string
     */
    public function generate_sha512_salt() {
        return '$6$rounds=10000$' . $this->generate_random_string(16) . '$';
    }
}
?>
