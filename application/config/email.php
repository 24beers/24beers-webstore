<?php if (!defined('BASEPATH')) { exit; }
/**
 * Configuration for the Email library.
 */
$config['mailtype'] = 'html';
$config['protocol'] = 'smtp';
$config['smtp_host'] = 'smtp.mandrillapp.com';
$config['smtp_pass'] = '';
$config['smtp_port'] = 587;
$config['smtp_user'] = 'ben@24beers.co.uk';
$config['useragent'] = '24 Beers Mail';
$config['validate'] = TRUE;

/* End of file email.php */
/* Location: ./application/config/email.php */
