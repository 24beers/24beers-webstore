<?php  if (!defined('BASEPATH')) { exit; }
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|    example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|    http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|    $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|    $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['default_controller'] = 'home';
$route['404_override'] = '40x.php';

/* Blog routes */
$route['blog.xml'] = 'blog/feed';

/* Checkout process route. */
$route['checkout/paymentcard/add'] = 'checkout/paymentcard_add';
$route['checkout/paymentcard/create'] = 'checkout/paymentcard_create';

/* Information page routes. */
$route['competition/free-beer-may2014'] = 'information/competition/' . COMP_NAME_MAY2014;
$route['twenty-four-beers'] = 'information/twentyfourbeers';
$route['delivery-info'] = 'information/delivery';
$route['faq'] = 'information/faq';
$route['newsroom'] = 'information/newsroom';
$route['privacy-policy'] = 'information/privacy';
$route['terms-conditions'] = 'information/terms';

/* Partner tracking routes. */
$route['partner/(:any)'] = 'partner/index/$1';

/* Product detail routes. */
$route['beer/(:any)'] = 'product/index/$1';

/* Brewery detail routes. */
$route['brewery/(:any)'] = 'brewery/index/$1';

/* Promotion routes. */
$route["belgian-beer"] = "information/promo/" . PROMO_NAME_BELGIANBEER;
$route["st-georges-day-beer"] = "information/promo/" . PROMO_NAME_STGEORGESDAY;
$route["valentines-day-gifts"] = "information/promo/" . PROMO_NAME_VALENTINESGIFTS;

/* Search routes. */
$route['category/(:any)/(:any)'] = 'search/category_filter/$1/$2';

/* End of file routes.php */
/* Location: ./application/config/routes.php */