<?php  if ( ! defined('BASEPATH')) { exit; }

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',                          'rb');
define('FOPEN_READ_WRITE',                    'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',      'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',                  'ab');
define('FOPEN_READ_WRITE_CREATE',             'a+b');
define('FOPEN_WRITE_CREATE_STRICT',           'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',      'x+b');

/**
 * Account parameters.
 */
define('ACCT_TYPE_CREDIT', 'credit');
define('ACCT_TYPE_KITTY', 'kitty');

/**
 * Application parameters.
 */
define('APP_ADMIN_EMAIL', 'ben@24beers.co.uk');
define('APP_ADMIN_NAME', '24 Beers Admin');
define('APP_DOMAIN', '24beers.co.uk');
define('APP_EPOCH_MS', 1356998400000);
define('APP_GA_ACCT', 'UA-38955058-1');
define('APP_GA_EVENT_ACT_ADD', 'Add');
define('APP_GA_EVENT_ACT_DECR', 'Decrement');
define('APP_GA_EVENT_ACT_INCR', 'Increment');
define('APP_GA_EVENT_ACT_REMOVE', 'Remove');
define('APP_GA_EVENT_CAT_BASKET', 'Basket');
define('APP_GA_EVENT_CAT_REVIEW', 'Basket');
define('APP_NUM_SHARDS', 100);

/**
 * Page cache times.  Cache timeout values are in minutes.
 */
define('CACHE_INFORMATION_PAGE', 1440);
define('CACHE_PRODUCT_PAGE', 60);

/**
 * Collection codes
 */
define('COLLECTION_24BEERS', 'twenty-four-beers');

/**
 * Configuration parameters.
 */
define('CONFIG_STRIPE_KEY_LIVE_PUBLIC', 'stripe_key_live_public');
define('CONFIG_STRIPE_KEY_LIVE_SECRET', 'stripe_key_live_secret');
define('CONFIG_STRIPE_KEY_TEST_PUBLIC', 'stripe_key_test_public');
define('CONFIG_STRIPE_KEY_TEST_SECRET', 'stripe_key_test_secret');
define('CONFIG_STRIPE_TEST_MODE', 'stripe_test_mode');

/**
 * CRM mailer parameters.
 */
define('CRM_MAILER_NAME', 'MailChimp');

/**
 * Custom constants.
 */
define('BRAND_NAME', '24 Beers');
define('BRAND_URL_FACEBOOK', 'https://www.facebook.com/24BeersUK');
define('BRAND_URL_GPLUS', 'https://www.24beers.co.uk/g+');
define('BRAND_URL_INSTAGRAM', 'http://instagram.com/twentyfourbeers');
define('BRAND_URL_PINTEREST', 'https://www.pinterest.com/24Beers/');
define('BRAND_URL_TWITTER', 'https://www.twitter.com/24BeersUK');
define('COMPANY_ADDRESS_INLINE', 'Norwich Enterprise Centre, 4b Guildhall Hill, Norwich, NR2 1JH');
define('COMPANY_NAME', '24 Beers Limited');
define('COMPANY_NUMBER', '08376629');
define('COUNTRY_UK', 'United Kingdom');
define('COURIER_NAME', 'UK Mail');
define('EMAIL_CUST_HELP', 'care@24beers.co.uk');
define('FACEBOOK_BRAND', '24BeersUK');
define('GPLUS_BRAND', '24 Beers');
define('INSTAGRAM_BRAND', 'twentyfourbeers');
define('NAME_CUST_HELP', '24 Beers Care');
define('PINTEREST_BRAND', '24Beers');
define('SITE_NAME', '24Beers.co.uk');
define('SITE_TITLE', 'Buy beer and ale online, UK next day delivery');
define('TWITTER_BRAND', '@24BeersUK');
define('TWITTER_CUST_HELP', '@24BeersCare');
define('WAREHOUSE_ADDRESS_INLINE', 'Norwich Enterprise Centre, 4b Guildhall Hill, Norwich, NR2 1JH');

/**
 * Database objects.
 */
define('DB_COLLECTION_SHIPMENT', 'shipment');
define('DB_TABLE_ADDRESS', 'address');
define('DB_TABLE_OBJECT', 'object');
define('DB_TABLE_ORDER', 'order');
define('DB_TABLE_ORDER_ADDRESS', 'order_address');
define('DB_TABLE_ORDER_LINE', 'order_line');
define('DB_TABLE_PRODUCT', 'product');
define('DB_TABLE_TRANSACTION', 'transaction');
define('DB_TABLE_USER', 'user');
define('DB_VIEW_ADDRESS', 'v_address');
define('DB_VIEW_CUSTOMER', 'v_customer');
define('DB_VIEW_ORDER', 'v_order');
define('DB_VIEW_PRODUCT', 'v_product');
define('DB_VIEW_STAFF', 'v_staff');

/**
 * Datasource object parameters.
 */
define('DSO_COLLECTION_ACCOUNT', 'account');
define('DSO_FIELD_ACCT_TXN', 'account_transaction');
define('DSO_FIELD_ACCT_VALUE', 'account_value');
define('DSO_FIELD_ACCT_VALUEPREV', 'account_value_previous');
define('DSO_FIELD_AMOUNT', 'amount');
define('DSO_FIELD_CREATEDT', 'create_datetime');
define('DSO_FIELD_CUSTSPEND', 'customer_spend');
define('DSO_FIELD_ISLOADED', 'is_loaded');
define('DSO_FIELD_MODIFYDT', 'modify_datetime');
define('DSO_FIELD_OWNEDBY', 'owned_by');
define('DSO_FIELD_STATUS', 'status');
define('DSO_FIELD_TYPE', 'type');
define('DSO_INDEX_BLOG', 'blog');
define('DSO_INDEX_DASHBOARD', 'dashboard');
define('DSO_INDEX_STORE', 'store');
define('DSO_TYPE_ORDER', 'order');
define('DSO_TYPE_PRODUCT', 'product');
define('DSO_TYPE_PROMOTION', 'promotion');

/**
 * Error messages.
 */
define('ERROR_ADDRESS_NOTFOUND', 'Oops, we cannot find that address.');
define('ERROR_AUTH_FAILED', 'Could not authorise account. Either your email or password is wrong.');
define('ERROR_BASKET_EMPTY', 'There are no items in your basket.');
define('ERROR_BLOG_NOTFOUND', 'Sorry, this blog entry does not exist.');
define('ERROR_CUSTOMER_NOTFOUND', 'For some reason we cannot find your customer account.');
define('ERROR_DATE_INVALID', 'Date you entered is not valid.');
define('ERROR_DATE_WRONG_FORMAT', 'Date you entered is in the wrong format.');
define('ERROR_DB_FAILED_CREATE_ADDRESS', 'Could not create address.');
define('ERROR_DB_FAILED_CREATE_CUSTOMER', 'Could not create customer account.');
define('ERROR_DB_FAILED_CREATE_OBJECT', 'Could not create object record.');
define('ERROR_DB_FAILED_CREATE_ORDER', 'Could not create order record.');
define('ERROR_DB_FAILED_CREATE_SHIPMENT', 'Could not create shipment record.');
define('ERROR_DB_FAILED_CREATE_TRANSACTION', 'Could not create transaction record.');
define('ERROR_DOB_TOO_YOUNG', 'You must be over 18 to shop with us.');
define('ERROR_EMAIL_EXISTS', 'This email address has already been registered.');
define('ERROR_EMAIL_NOTEXISTS', 'This email address is not registered.');
define('ERROR_FAILED_CONNECT', "There's a problem connecting to a remote service. We're aware and will fix the problem. Sorry about this.");
define('ERROR_FAILED_UPDATE_CUSTOMER', 'Sorry, something went wrong and we could not update your details. It has been logged and we will fix the problem ASAP.');
define('ERROR_PASSWORD_NOTSET', 'Your must enter a password.');
define('ERROR_PASSWORDS_NOTMATCH', 'Your password and confirmation do not match.');
define('ERROR_PAYMENT_FAILED', 'Sorry, your payment has failed. Please contact us.');
define('ERROR_PAYMENT_TOKEN_EXISTS', 'Looks like you tried to submit the same order again.');
define('ERROR_PHONE_INVALID', 'Please enter a valid UK phone number.');
define('ERROR_POSTCODE_INVALID', 'Please enter a valid UK postcode.');
define('ERROR_PRODUCT_NOTFOUND', 'Oops, we cannot find details of that product.');
define('ERROR_RESET_TOKEN_NOTEXISTS', 'Sorry, the reset link has expired. Please request to reset your password again as the link is only valid for 10 minutes.');
define('ERROR_REVIEWS_NOTAVAILABLE', 'Sorry, reviews are not currently available.');
define('ERROR_SEARCH_NORESULT', 'Your search did not find anything.');
define('ERROR_SESSION_NOCUSTCODE', 'Oops, something seems to be wrong with your session.');
define('ERROR_TECHNICAL', "Sorry, there is a technical problem. We're on it though and will have it fixed ASAP.");

/**
 * Information messages.
 */
define('INFO_CASE_REMAINDER', "Just %d more bottles and you're ready to check out.<br><small>You need at least %d bottles to place an order and we sell our beers in multiples of %d. This is to make delivery cheaper for you.</small>");
define('INFO_PASSWORD_RESET_SENT', 'A link to reset your password has been sent to the email address you entered.');

/**
 * Log levels.
 */
define('LOG_LEVEL_DEBUG', 'debug');
define('LOG_LEVEL_ERROR', 'error');
define('LOG_LEVEL_INFO', 'info');

/**
 * Log messages.
 */
define('LOG_ADDRESS_EXISTS', 'Address already exists.');
define('LOG_ACCT_FOUNDTOOMANY', 'Too many accounts found, should only be 1 per customer per type.');
define('LOG_BASKET_EMPTY', 'No product items in customer basket.');
define('LOG_BASKET_KEY_NOTSPECIFIED', 'Basket key not specified.');
define('LOG_CUSTOMER_NOTFOUND', 'No customer found for given code.');
define('LOG_DB_FAILED_CREATE_OBJECT', 'Failed to create object record.');
define('LOG_DB_FAILED_CREATE_ORDER', 'Failed to create order record.');
define('LOG_DB_FAILED_CREATE_ORDER_ADDRESS', 'Failed to create order address record.');
define('LOG_DB_FAILED_CREATE_ORDERLINE', 'Failed to create order line record.');
define('LOG_DB_FAILED_CREATE_SHIPMENT', 'Failed to create shipment record.');
define('LOG_DB_FAILED_CREATE_TRANSACTION', 'Failed to create transaction record.');
define('LOG_DB_FAILED_UPDATE_CUSTOMER', 'Failed to update customer record.');
define('LOG_DB_FAILED_UPDATE_OBJECT', 'Failed to update object record.');
define('LOG_DB_FAILED_UPDATE_PASSWORD', 'Failed to update customer password.');
define('LOG_DB_SHIPMENT_EXISTS', 'Shipment already exists for this order.');
define('LOG_FAILED_CONNECT', 'Failed to connect to host: %s.');
define('LOG_FAILED_DELETE_ADDRESS', 'Could not delete address.');
define('LOG_ITEM_CODE_NOTSPECIFIED', 'Product code not specified.');
define('LOG_ORDER_NOTFOUND', 'No order found for given customer and product code: %s, %s');
define('LOG_PRODUCT_CODE_NOTSPECIFIED', 'No product found for product code: ');
define('LOG_PRODUCT_NOTFOUND', 'No product found for given code: ');
define('LOG_QUANTITY_NOTSPECIFIED', 'Item quantity not specified.');
define('LOG_RATING_NOTSPECIFIED', 'No rating specified.');
define('LOG_SESSION_NOCUSTCODE', 'No customer code in session.');
define('LOG_SKU_MULTIMATCH', 'Multiple matches found for given SKU code: ');
define('LOG_SKU_NOTFOUND', 'No SKU found for given code: ');
define('LOG_UNIQUE_CODE_NOTFOUND', 'Last used unique code not found in datastore, potential problem unless this is expected.');
define('LOG_UPDATE_STOCK_WRONG_PARAM', 'Wrong parameter was given to update stock. Check that parameters are not reversed.');
define('LOG_TXN_NO_RESPONSE', 'Transaction failed, no response.');

/**
 * Meta description text.
 */
define('META_DESCR_BLOG_HOME', "Read our blog about real ale, craft beer and the stuff we get up to at 24 Beers.");
define('META_DESCR_COMP_MAY2014', "Enter our competition to win free beer every time you buy real ale and craft beer online.");
define('META_DESCR_BREWERY_HOME', "Read about the passionate brewers, and independent British breweries and microbreweries, 24 Beers is proud to supply beer from and be associated with.");
define('META_DESCR_HOME', "Buy real ale and craft beer online from independent UK breweries at 24 Beers. Delivery only &pound;%01.2f");
define('META_DESCR_PROMO_BELGIANBEER', "Buy Belgian-style beer and ale online from small independent British breweries, inspired by Belgium. Delivered to your door next working day for only &pound;%01.2f");
define('META_DESCR_PROMO_STGEORGESDAY', "Buy English beer and ale for St. George's Day from our recommendations online at 24 Beers and delivered next working day to your door for only &pound;%01.2f");
define('META_DESCR_PROMO_VALENTINESDAY', "Buy Valentine's Day beer and ale gifts from our recommendations online at 24 Beers and delivered next working day to your door for only &pound;%01.2f");

/**
 * String formats.
 */
define('FORMAT_DATE', 'Y-m-d');
define('FORMAT_DATE_INPUT', 'DD/MM/YYYY');

/**
 * URL parameters.
 */
define('PARAM_CAMPAIGN', 'utm_campaign');
define('PARAM_MEDIUM', 'utm_medium');
define('PARAM_REDIRECT', 'redir');
define('PARAM_SOURCE', 'utm_source');

/* Partner codes */
define('PARTNER_CODE_NORFOLKMAG', 'norfolkmag');

/**
 * Payment provider parameters.
 */
define('PAYMENT_PROVIDER_NAME', 'Stripe');

/**
 * Promotion names.
 */
define('COMP_NAME_MAY2014', 'free-beer-may2014');
define("PROMO_NAME_BELGIANBEER", 'belgianbeer');
define("PROMO_NAME_STGEORGESDAY", 'stgeorgesday');
define("PROMO_NAME_VALENTINESGIFTS", 'valentinesgifts');

/**
 * Status codes
 */
define('STATUS_ACTIVE', 'active');
define('STATUS_DELETED', 'deleted');
define('STATUS_DISABLED', 'disabled');
define('STATUS_FAILED', 'failed');
define('STATUS_INACTIVE', 'inactive');
define('STATUS_NEW', 'new');
define('STATUS_PAID', 'paid');
define('STATUS_PENDING', 'pending');
define('STATUS_SHIPPED', 'shipped');

/**
 * Session parameters.
 */
define('SESSION_BASKET_DATA', 'basket_data');
define('SESSION_BASKET_KEY', 'basket_key');
define('SESSION_CUSTOMER_CODE', 'customer_code');
define('SESSION_ID', 'session_id');
define('SESSION_IS_AUTHED', 'is_authed');
define('SESSION_PARTNER_CODE', 'partner_code');
define('SESSION_PAYMENT_TOKEN', 'payment_token');
define('SESSION_REDIR', 'redir');

/**
 * Store parameters.
 */
/* Minimum number of items in the basket required before checkout. */
define('STORE_DELIVERY_CHARGE_NEXTDAY', 6.99);
define('STORE_DELIVERY_CHARGE_STANDARD', 5.99);
define('STORE_MIN_CASE_SIZE', 6);
define('STORE_MIN_ORDER_SIZE', 6);
define('STORE_THRESHOLD_ORDER_VALUE', 500);

/**
 * Transaction types.
 */
define('TXN_TYPE_CHARGE', 'charge');

/**
 * Record types.
 */
define('TYPE_ADDRESS_BILLING', 'billing');
define('TYPE_ADDRESS_DELIVERY', 'delivery');
define('TYPE_CREDIT', 'credit');
define('TYPE_DEBIT', 'debit');
define('TYPE_OBJECT_ADDRESS', 'address');
define('TYPE_OBJECT_BREWERY', 'brewery');
define('TYPE_OBJECT_ORDER', 'order');
define('TYPE_OBJECT_PRODUCT', 'product');
define('TYPE_OBJECT_USER', 'user');
define('TYPE_USER_CUSTOMER', 'customer');
define('TYPE_USER_STAFF', 'customer');

/**
 * Page URLs.
 */
define('URL_ACCT_ADDRESS_ADD', '/account/address_add');
define('URL_ACCT_ADDRESS_CREATE', '/account/address_create');
define('URL_ACCT_ADDRESS_DELETE', '/account/address_delete');
define('URL_ACCT_ADDRESS_UPDATE', '/account/address_update');
define('URL_ACCT_AUTH', '/account/auth');
define('URL_ACCT_CREATE', '/account/create');
define('URL_ACCT_DETAIL', '/account/detail');
define('URL_ACCT_DETAIL_UPDATE', '/account/detail_update');
define('URL_ACCT_HOME', '/account');
define('URL_ACCT_JOIN', '/account/join');
define('URL_ACCT_NEWPW', '/account/new_password');
define('URL_ACCT_RESETPW', '/account/reset_password');
define('URL_ACCT_SIGNIN', '/account/signin');
define('URL_ACCT_SIGNOUT', '/account/signout');
define('URL_ACCT_UPDATEPW', '/account/update_password');
define('URL_BASKET', '/basket');
define('URL_BASKET_ADD', '/basket/add');
define('URL_BASKET_ADD_ASYNC', '/basket/add_async');
define('URL_BASKET_CHECKOUT', '/basket/checkout');
define('URL_BASKET_GET', '/basket/get');
define('URL_BASKET_LESS', '/basket/less');
define('URL_BASKET_MORE', '/basket/more');
define('URL_BASKET_REMOVE', '/basket/remove');
define('URL_24BEERS_INFO', '/twenty-four-beers');
define('URL_BLOG', '/blog');
define('URL_BLOG_FEED', '/blog.xml');
define('URL_BREWERY', '/brewery');
define('URL_CATEGORY', '/category');
define('URL_CATEGORY_STYLE', '/category/style');
define('URL_CHECKOUT', '/checkout');
define('URL_CHECKOUT_ABOUTYOU_EDIT', '/checkout/aboutyou_edit');
define('URL_CHECKOUT_ABOUTYOU_UPDATE', '/checkout/aboutyou_update');
define('URL_CHECKOUT_ADDRESS_ADD', '/checkout/address_add');
define('URL_CHECKOUT_ADDRESS_EDIT', '/checkout/address_edit');
define('URL_CHECKOUT_ADDRESS_CREATE', '/checkout/address_create');
define('URL_CHECKOUT_ADDRESS_DELETE', '/checkout/address_delete');
define('URL_CHECKOUT_ADDRESS_UPDATE', '/checkout/address_update');
define('URL_CHECKOUT_ADDRESSBOOK', '/checkout/addressbook');
define('URL_CHECKOUT_CONFIRMATION', '/checkout/confirmation');
define('URL_CHECKOUT_PAYPAL', '/checkout/paypal');
define('URL_CHECKOUT_PAYCARD_CREATE', '/checkout/paycard/create');
define('URL_CHECKOUT_SUBMIT_ORDER', '/checkout/submit_order');
define('URL_DELIVERY_INFO', '/delivery-info');
define('URL_FAQ', '/faq');
define('URL_HOME', '/');
define('URL_MEDIA', '/media');
define('URL_MEDIA_PRODUCT', '/media/product');
define('URL_PRIVACY_POLICY', '/privacy-policy');
define('URL_PRODDETAIL_BEER', '/beer');
define('URL_PRODUCT_GET', '/product/get');
define('URL_PRODUCT_GETSTOCK', '/product/get_stock');
define('URL_PRODUCT_REVIEW', '/product/review');
define('URL_TERMS_CONDITIONS', '/terms-conditions');
/* End of file constants.php */
/* Location: application/config/constants.php */