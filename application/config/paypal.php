<?php if (!defined('BASEPATH')) exit;

$config['pp_use_sandbox'] = TRUE;
$config['pp_api_endpoint'] = $config['pp_use_sandbox'] ? 'api-3t.sandbox.paypal.com' : '';
$config['pp_client_id'] = '';
$config['pp_secret'] = '';
$config['pp_merchant_id'] = 'PWEA9B4M2EETA';
$config['pp_api_username'] = 'ben_api1.24beers.co.uk';
$config['pp_api_password'] = '';
$config['pp_api_signature'] = '';

/* End of file paypal.php */
/* Location: ./system/application/config/paypal.php */
