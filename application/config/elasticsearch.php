<?php  if ( ! defined('BASEPATH')) { exit; }
/**
 * Config for the CodeIgniter ElasticSearch library
 *
 * @see ../libraries/ElasticSearch.php
 */
$config['es_host']  = get_server_variable('ES_HOST', FILTER_VALIDATE_IP);
$config['es_port']  = get_server_variable('ES_PORT', FILTER_VALIDATE_INT);
$config['es_index'] = get_server_variable('ES_INDEX');

/* End of file elasticsearch.php */
/* Location: ./application/config/elasticsearch.php */