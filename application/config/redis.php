<?php  if (!defined('BASEPATH')) { exit; }
/**
 * Config for the CodeIgniter Redis library
 *
 * @see ../libraries/Redis.php
 */

// Connection details
$config['redis_db'] = get_server_variable('REDIS_DB', FILTER_VALIDATE_INT);
$config['redis_host'] = get_server_variable('REDIS_HOST', FILTER_VALIDATE_IP);
$config['redis_port'] = get_server_variable('REDIS_PORT', FILTER_VALIDATE_INT);
$config['redis_password'] = '';

/* End of file redis.php */
/* Location: ./application/config/redis.php */