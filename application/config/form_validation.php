<?php if (! defined('BASEPATH')) { exit; }
$config = array(
    'account/address_create' => array(
        array(
            'field' => 'name',
            'label' => 'Addressee',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'house',
            'label' => 'Flat/House Name or Number',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'street',
            'label' => 'Street',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'town',
            'label' => 'Town',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'city',
            'label' => 'City/County',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'postcode',
            'label' => 'Postcode',
            'rules' => 'trim|required|xss_clean|valid_uk_postcode'
        )
    ),
    'account/address_update' => array(
        array(
            'field' => 'name',
            'label' => 'Addressee',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'house',
            'label' => 'Flat/House Name or Number',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'street',
            'label' => 'Street',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'town',
            'label' => 'Town',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'city',
            'label' => 'City/County',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'postcode',
            'label' => 'Postcode',
            'rules' => 'trim|required|xss_clean|valid_uk_postcode'
        )
    ),
    'account/auth' => array(
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|xss_clean|valid_email'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required|min_length[6]|md5'
        )
    ),
    'account/create' => array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'phone',
            'label' => 'Phone',
            'rules' => 'trim|required|xss_clean|valid_uk_phone'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|xss_clean|valid_email|email_not_exists'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required|min_length[6]|md5'
        ),
        array(
            'field' => 'dob',
            'label' => 'Date of Birth',
            'rules' => 'trim|required|xss_clean|valid_dob'
        )
    ),
    'account/detail_update' => array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|xss_clean|valid_email'
        ),
        array(
            'field' => 'main_phone',
            'label' => 'Main Phone',
            'rules' => 'trim|required|xss_clean|valid_uk_phone'
        ),
        array(
            'field' => 'alternative_phone',
            'label' => 'Alternative Phone',
            'rules' => 'trim|xss_clean|valid_uk_phone'
        ),
        array(
            'field' => 'dob',
            'label' => 'Date of Birth',
            'rules' => 'trim|required|xss_clean|valid_dob'
        )
    ),
    'account/reset_password' => array(
        array(
            'field' => 'reset-email',
            'label' => 'Email',
            'rules' => 'trim|required|xss_clean|valid_email|email_exists'
        )
    ),
    'account/update_password' => array(
        array(
            'field' => 'token',
            'label' => 'Token',
            'rules' => 'trim|required|xss_clean|password_reset_token_exists'
        ),
        array(
            'field' => 'password',
            'label' => 'New Password',
            'rules' => 'trim|required|xss_clean|min_length[6]|md5'
        ),
        array(
            'field' => 'passwordconf',
            'label' => 'Confirm Password',
            'rules' => 'trim|required|xss_clean|min_length[6]|md5|passwords_match'
        )
    ),
    'basket/add' => array(
        array(
            'field' => 'sku_code',
            'label' => 'Product',
            'rules' => 'trim|required|xss_clean|valid_sku'
        ),
        array(
            'field' => 'quantity',
            'label' => 'Quantity',
            'rules' => 'trim|required|xss_clean|integer|greater_than[0]'
        )
    ),
    'basket/add_async' => array(
        array(
            'field' => 'sku_code',
            'label' => 'Product',
            'rules' => 'trim|required|xss_clean|valid_sku'
        ),
        array(
            'field' => 'quantity',
            'label' => 'Quantity',
            'rules' => 'trim|required|xss_clean|integer|greater_than[0]'
        )
    ),
    'checkout/aboutyou_update' => array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|xss_clean|valid_email'
        ),
        array(
            'field' => 'main_phone',
            'label' => 'Main Phone',
            'rules' => 'trim|required|xss_clean|valid_uk_phone'
        ),
        array(
            'field' => 'alternative_phone',
            'label' => 'Alternative Phone',
            'rules' => 'trim|xss_clean|valid_uk_phone'
        ),
        array(
            'field' => 'dob',
            'label' => 'Date of Birth',
            'rules' => 'trim|required|xss_clean|valid_dob'
        )
    ),
    'checkout/address_create' => array(
        array(
            'field' => 'name',
            'label' => 'Addressee',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'house',
            'label' => 'Flat/House Name or Number',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'street',
            'label' => 'Street',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'town',
            'label' => 'Town',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'city',
            'label' => 'City/County',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'postcode',
            'label' => 'Postcode',
            'rules' => 'trim|required|xss_clean|valid_uk_postcode'
        )
    ),
    'checkout/address_update' => array(
        array(
            'field' => 'name',
            'label' => 'Addressee',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'house',
            'label' => 'Flat/House Name or Number',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'street',
            'label' => 'Street',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'town',
            'label' => 'Town',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'city',
            'label' => 'City/County',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'postcode',
            'label' => 'Postcode',
            'rules' => 'trim|required|xss_clean|valid_uk_postcode'
        )
    ),
    'checkout/paycard_create' => array(
        array(
            'field' => 'name',
            'label' => 'Name on Card',
            'rules' => 'trim|required|xss_clean'
        ),
        array(
            'field' => 'number',
            'label' => 'Card Number',
            'rules' => 'trim|required|xss_clean|valid_card_number'
        )
    ),
    'checkout/submit_order' => array(
        array(
            'field' => 'address_code',
            'label' => 'Address',
            'rules' => 'trim|required|xss_clean|address_exists_ownedbycust'
        ),
        array(
            'field' => 'stripeToken',
            'label' => 'Payment Token',
            'rules' => 'trim|required|xss_clean|payment_token_not_exists'
        )
    ),
    'product/review' => array(
        array(
            'field' => 'product_code',
            'label' => 'Product',
            'rules' => 'trim|required|xss_clean|valid_product'
        ),
        array(
            'field' => 'rating',
            'label' => 'Rating',
            'rules' => 'trim|required|xss_clean|integer|greater_than[0]|less_than[6]'
        ),
        array(
            'field' => 'comment',
            'label' => 'Comment',
            'rules' => 'trim|xss_clean'
        )
    )
);
/* End of file form_validation.php */
/* Location: ./application/config/form_validation.php */
