<?php if (!defined('BASEPATH')) { exit; }
/**
 * Configuration for the authorisation library.
 */

/**
 * List of each controller that does not require authentication to access.
 */
$config['public_controllers'] = array(
    /* Account controller functions. */
    '/account/auth', '/account/create', '/account/join',
    '/account/new_password', '/account/reset_password', '/account/signin',
    '/account/update_password',
    /* Basket controller functions. */
    '/basket', '/basket/add', '/basket/add_async', '/basket/checkout',
    '/basket/get', '/basket/less', '/basket/more', '/basket/remove',
    /* Blog controller functions. */
    '/blog', '/blog/entry', '/blog.xml',
    /* Brewery page */
    '/brewery',
    /* Home controller functions. */
    '/', '/home',
    /* Information controller functions. */
    '/twenty-four-beers', '/delivery-info', '/faq', '/privacy-policy',
    '/terms-conditions',
    /* Product detail controller functions. */
    '/product/get', '/product/get_sku', '/product/get_stock', '/product/review',
    /* Promotion page controller routes. */
     '/belgian-beer', '/st-georges-day-beer', '/valentines-day-gifts',
    /* Search result controller functions. */
    '/search', '/search/category_filter',
    '/test');

/**
 * List of controller patterns that do not require authentication to access.
 * This is so that dynamic controller URLs, such as the product detail pages,
 * can be listed.
 */
$config['public_controller_patterns'] = array(
    '/beer/.+', '/brewery/.+', '/category/.+', '/competition/.+',
    /* Partner controller functions */
    '/partner/.+'
);
/* End of file auth.php */
/* Location: ./application/config/auth.php */
