<?php if ( ! defined('BASEPATH')) { exit; }
/**
 * Config for the CodeIgniter MongoDB library.
 *
 * @see ../libraries/Mongo_db.php
 */
$config['mongo_host'] = get_server_variable('MONGO_HOST', FILTER_VALIDATE_IP);
$config['mongo_port'] = get_server_variable('MONGO_PORT', FILTER_VALIDATE_INT);
$config['mongo_db'] = get_server_variable('MONGO_DB');
$config['mongo_user'] = '';
$config['mongo_pass'] = '';
$config['mongo_persist'] = TRUE;
$config['mongo_persist_key'] = 'ci_mongo_persist';

/* End of file mongo_db.php */
/* Location: ./application/config/mongo_db.php */