<?php if (!defined('BASEPATH')) { exit; }
/**
 * Stripe payment card processor configuration options
 */
$config['stripe_api_endpoint']    = 'https://api.stripe.com/v1/';
$config['stripe_key_test_public'] = '';
$config['stripe_key_test_secret'] = '';
$config['stripe_key_live_public'] = '';
$config['stripe_key_live_secret'] = '';
$config['stripe_test_mode']       = get_server_variable('PAYMENT_MODE') !== 'live';
$config['stripe_verify_ssl']      = FALSE;
/* End of file stripe.php */
/* Location: ./application/config/stripe.php */
