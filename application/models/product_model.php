<?php if (! defined('BASEPATH')) { exit; }

class Product_model extends CI_Model {

    const PRODUCT_KEY_PREFIX = 'store:product:';

    /**
     * <p>Add a rating and comment that make up a review to a product.<br>
     * Increment the rating count and rating total for the product by the
     * current rating.</p>
     *
     * @param  string  $customer_code
     * @param  string  $product_code
     * @param  integer $rating
     * @param  string  $comment
     * @return void
     */
    public function add_review(
            $customer_code, $product_code, $rating, $comment='') {

        $product_key = Product_model::PRODUCT_KEY_PREFIX . $product_code;

        $this->load->library('redis');

        /* Add the rating and comment to the head of the review list. */
        $this->redis->lpush("$product_key:review",
                json_encode(array(
                    'customer_code' => $customer_code,
                    'rating'        => $rating,
                    'comment'       => $comment,
                    'timestamp'     => database_datetime()
                )));

        /* Get any previous rating from this customer. */
        $prev_cust_rating =
                $this->redis->get("$product_key:rating:$customer_code");

        if (!empty($prev_cust_rating)) {
            /* Decrement the product rating by customer's previos rating. */
            $this->redis->decrby("$product_key:rating_sum", $prev_cust_rating);
        } else {
            /* Increment the count of ratings. */
            $this->redis->incr("$product_key:rating_count");
        }

        /* Set product rating for customer. */
        $this->redis->set("$product_key:rating:$customer_code", $rating);

        /* Increment the sum of ratings by the customer's rating. */
        $this->redis->incrby("$product_key:rating_sum", $rating);
    }

    /**
     * Get a list of cross-sells for the given product.
     * @param  string $product_code
     * @return array  List of cross-sells.
     */
    public function get_cross_sells($product_code) {
        $this->load->library('elasticsearch');
        $this->elasticsearch->set_type('cross_sell');

        $result = $this->elasticsearch->term_query('parent.code', $product_code);
        $sells = array();

        if ($result->hits->total === 0) {
            return $sells;
        }

        foreach ($result->hits->hits as $hit) {
            array_push($sells, $this->generate_cross_sell_from_source($hit->_source));
        }

        return $sells;
    }

    /**
     * Get details of the product by the given product code.
     * @param string $product_code
     */
    public function get_product_by_code($product_code) {
        $this->load->library('elasticsearch');
        $this->elasticsearch->set_type('product');

        $result = $this->elasticsearch->get($product_code);

        if ($result->exists != 1) {
            log_message(LOG_LEVEL_ERROR, LOG_PRODUCT_NOTFOUND . $product_code);
            return NULL;
        }

        return $this->generate_from_source($result->_source);
    }

    /**
     * <p>Get details of the product by searching on given SKU code.<br>
     * Helper function is used to translate search result data into product
     * object.</p>
     * <p>Log an error and return NULL if no SKU is found for given code. Log an
     * error if there are multiple matches for given SKU code.</p>
     * @param string $sku_code
     */
    public function get_product_by_sku_code($sku_code) {
        $this->load->library('elasticsearch');
        $this->elasticsearch->set_type('product');

        $result = $this->elasticsearch->term_query(
                'product.sku_code', $sku_code);

        if ($result->hits->total === 0) {
            log_message(LOG_LEVEL_ERROR, LOG_SKU_NOTFOUND . $sku_code);
            return NULL;
        }

        if ($result->hits->total > 1) {
            log_message(LOG_LEVEL_ERROR, LOG_SKU_MULTIMATCH . $sku_code);
        }

        return $this->generate_from_source($result->hits->hits[0]->_source);
    }

    /**
     * Get the review data for a given product.
     * 
     * @param string $product_code
     * @return object The review data encapsulated in an object.
     */
    public function get_product_review($product_code) {

        $product_key = Product_model::PRODUCT_KEY_PREFIX . $product_code;

        $this->load->library('redis');

        return (object) array(
            'sum'     => $this->redis->get("$product_key:rating_sum"),
            'count'   => $this->redis->get("$product_key:rating_count"),
            'reviews' => $this->_get_lastn_reviews($product_code, 5)
        );
    }

    public function get_products_by_brewery($brewery_code) {
        $this->load->library('elasticsearch');
        $this->elasticsearch->set_type('product');

        $result = $this->elasticsearch->term_query(
                'brewery.code', $brewery_code);
        $products = array();

        if ($result->hits->total === 0) {
            return $products;
        }

        foreach ($result->hits->hits as $hit) {
            array_push($products, $this->generate_from_source($hit->_source));
        }

        return $products;
    }

    /**
     * Get the current stock level for the given product.  If the stock level is
     * below the threshold stock level 
     *
     * @param  string  $product_code
     * @return integer Amount of stock for the given product code.
     */
    public function get_stock_level($product_code) {

        $stock_key = Product_model::PRODUCT_KEY_PREFIX . "$product_code:stock";

        $this->load->library('redis');

        $stock = $this->redis->get($stock_key);

        return empty($stock) ? 0 : $stock;
    }

    /**
     * Update the stock level for the given product code by the adjustment.
     * The adjustment is always added to the current stock level so must be a
     * negative number for a reduction in stock.
     * 
     * @param string  $product_code
     * @param integer $adjustment
     */
    public function update_stock_level($product_code, $adjustment) {

        /* Create an error if the method parameters look wrong. */
        if (!is_string($product_code) || !is_integer($adjustment)) {
            log_message(LOG_LEVEL_ERROR, LOG_UPDATE_STOCK_WRONG_PARAM);
            show_error(ERROR_TECHNICAL);
        }

        $this->load->library('redis');

        $this->redis->set(
                Product_model::PRODUCT_KEY_PREFIX . "$product_code:stock",
                $this->get_stock_level($product_code) + $adjustment);
    }

    /**
     * Gets a specified number of the last reviews for a given product.
     * 
     * @param  string  $product_code
     * @param  integer $n
     * @return array
     */
    private function _get_lastn_reviews($product_code, $n) {

        $this->load->library('redis');

        $review_data = $this->redis->lrange(
                "store:product:$product_code:review", 0, $n-1);

        $reviews = array();

        if (!empty($review_data)) {
            foreach ($review_data as $json_review) {
                array_push($reviews, json_decode($json_review));
            }
        }

        return $reviews;
    }

    /**
     * Generate a product object from a search result source.
     *
     * @access private
     * @param  object  $source Source object returned from search server.
     * @return object  Product object.
     */
    private function generate_from_source($source) {
        $product = $source->product;
        $style = $source->style;
        $brewery = $source->brewery;

        return (object) array(
            'abv'                    => $product->abv,
            'brewery_code'           => $brewery->code,
            'brewery_description'    => $brewery->description,
            'brewery_name'           => $brewery->name,
            'code'                   => $product->code,
            'description'            => $product->description,
            'displayable_list_price' => format_displayable_price(
                    $product->list_price),
            'displayable_sale_price' => format_displayable_price(
                    $product->sale_price),
            'hops'                   => $product->hops,
            'list_price'             => $product->list_price,
            'malts'                  => $product->malts,
            'name'                   => $product->name,
            'sale_price'             => $product->sale_price,
            'size_container'         => $product->size . ' ' . $product->container,
            'sku_code'               => $product->sku_code,
            'style_name'             => $style->name,
            'style_code'             => $style->code,
            'text'                   => $product->text
        );
    }

    /**
     * Generate a cross-sell object from a search result source.
     * @param  object $source Source object returned from search server.
     * @return object Cross-sell object.
     */
    private function generate_cross_sell_from_source($source) {
        $child = $source->child;
        $style = $child->style;

        return (object) array(
            'abv'                    => $child->abv,
            'code'                   => $child->code,
            'displayable_sale_price' => format_displayable_price($child->sale_price),
            'name'                   => $child->name,
            'parent_code'            => $source->parent->code,
            'size_container'         => "$child->size $child->container",
            'style_code'             => $style->code,
            'style_name'             => $style->name
        );
    }
}
/* End of file product_model.php */
/* Location: ./application/models/product_model.php */