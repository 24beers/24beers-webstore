<?php if (! defined('BASEPATH')) exit;

class Order_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->database();
    }

    /**
     * Create an order record in the database and return the UUID.
     *
     * @param  integer $customer_uuid
     * @param  float   $subtotal
     * @param  float   $order_discount
     * @param  float   $delivery_total
     * @param  float   $delivery_discount
     * @param  float   $total_amount
     * @param  integer $num_items
     * @return mixed   Return new order object if insert is successful,
     *                 otherwise return false.
     */
    public function create_order($customer_code, $status, $subtotal,
            $order_discount, $delivery_total, $delivery_discount, $total_amount,
            $num_items) {

        $this->load->model('object_model');

        /* Create the object record and get the UUID. */
        $obj_uuid = $this->object_model->create_object(
                $this->generate_order_code(),
                'Order created: ' . get_displayable_datetime(),
                TYPE_OBJECT_ORDER,
                $status,
                $customer_code,
                $customer_code);

        /* Begin transaction. */
        $this->db->trans_begin();

        /* Insert into order table. */
        $this->db->insert(DB_TABLE_ORDER, array(
            'ord_uuid'              => $obj_uuid,
            'ord_subtotal'          => $subtotal,
            'ord_discount'          => $order_discount,
            'ord_delivery_total'    => $delivery_total,
            'ord_delivery_discount' => $delivery_discount,
            'ord_total_amount'      => $total_amount,
            'ord_num_items'         => $num_items
        ));

        if ($this->db->trans_status() === TRUE) {
            /* Commit transaction and return UUID if insert was successful. */
            $this->db->trans_commit();
            return $this->get_customer_order_by_id($customer_code, $obj_uuid);
        } else {
            /* Rollback transaction and log message if insert fails. */
            $this->db->trans_rollback();
            log_message(LOG_LEVEL_ERROR, LOG_DB_FAILED_CREATE_ORDER);
        }

        return FALSE;
    }

    /**
     * Insert an address for an order.
     * 
     * @param  integer $order_uuid
     * @param  object  $address
     * @return boolean True if inserting the address is successful,
     *                 otherwise false.
     */
    public function create_order_address($order_uuid, $address) {

        /* Begin transaction. */
        $this->db->trans_begin();

        /* Insert into order address table. */
        $this->db->insert(DB_TABLE_ORDER_ADDRESS, array(
            'oad_ord_uuid'     => $order_uuid,
            'oad_code'         => $address->code,
            'oad_address_type' => TYPE_ADDRESS_DELIVERY,
            'oad_name'         => $address->name,
            'oad_house'        => $address->house,
            'oad_street'       => $address->street,
            'oad_town'         => $address->town,
            'oad_city'         => $address->city,
            'oad_country'      => $address->country,
            'oad_postcode'     => $address->postcode
        ));

        /* Commit transaction and return UUID if insert was successful. */
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return TRUE;
        } 

        /* Rollback transaction and log message if insert fails. */
        $this->db->trans_rollback();
        log_message(LOG_LEVEL_ERROR, LOG_DB_FAILED_CREATE_ORDER_ADDRESS);

        return FALSE;
    }

    /**
     * Create order line records in the database and associate with given order.
     * If creating any order line records fails all inserts are rolled back.
     *
     * @param  integer $order_uuid
     * @param  array   $order_lines
     * @return mixed   New order line arrary if inserts is successful,
     *                 otherwise false.
     */
    public function create_order_lines($order, $order_lines) {

        $data = array();

        foreach ($order_lines as $line) {
            array_push($data, array(
                'orl_code'         => $line->line_code,
                'orl_name'         => $line->line_name,
                'orl_ord_uuid'     => $order->uuid,
                'orl_product_code' => $line->product_code,
                'orl_quantity'     => $line->quantity,
                'orl_total_amount' => $line->line_total,
                'orl_unit_price'   => $line->unit_price
            ));
        }

        /* Begin transaction. */
        $this->db->trans_begin();

        /* Insert order lines as a batch. */
        $this->db->insert_batch(DB_TABLE_ORDER_LINE, $data);

        /* Commit transaction and return UUID if insert was successful. */
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $this->get_order_lines($order->owned_by, $order->code);
        }

        /* Rollback transaction if insert fails. */
        $this->db->trans_rollback();

        log_message(LOG_LEVEL_ERROR, LOG_DB_FAILED_CREATE_ORDERLINE);

        return FALSE;
    }

    /**
     * Creates a transaction record in the database and returns the UUID.
     * 
     * @param  string  $transaction_type
     * @param  string  $transaction_id
     * @param  integer $transaction_date
     * @param  integer $order_uuid
     * @param  float   $order_amount
     * @param  float   $processing_fee
     * @param  float   $fee_refund
     * @param  string  $currency
     * @param  boolean $livemode
     * @return mixed   Return transaction UUID if insert is successful,
     *                 otherwise return false.
     */
    public function create_transaction($transaction_type, $transaction_id,
            $transaction_date, $order_uuid, $order_amount, $processing_fee,
            $fee_refund, $currency, $livemode) {

        $txn_uuid = generate_uuid();

        $txn_data = array(
            'txn_currency'         => $currency,
            'txn_fee_refund'       => $fee_refund,
            'txn_livemode'         => $livemode,
            'txn_order_amount'     => $order_amount,
            'txn_ord_uuid'         => $order_uuid,
            'txn_processing_fee'   => $processing_fee,
            'txn_transaction_dt'   => $transaction_date,
            'txn_transaction_id'   => $transaction_id,
            'txn_transaction_type' => $transaction_type,
            'txn_uuid'             => $txn_uuid,
        );

        /* Persist data within a transaction. */
        $this->db->trans_begin();

        $this->db->insert(DB_TABLE_TRANSACTION, $txn_data);

        /* Commit and return TRUE if the transaction is successful. */
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $txn_uuid;
        }

        $this->db->trans_rollback();

        log_message(LOG_LEVEL_ERROR, LOG_DB_FAILED_CREATE_TRANSACTION);

        return FALSE;
    }

    /**
     * <p>Get a customer order by the given field name and key value.</p>
     * <p>Returns NULL if no order is found that corresponds to the order code
     * and logged in customer.</p>
     * 
     * @param  string $customer_code
     * @param  string $order_field
     * @param  string $order_value
     * @return object Order detail object.
     */
    public function get_customer_order(
            $customer_code, $order_field, $order_value) {

        $this->order_select()
                ->where('ord_owned_by', $customer_code)
                ->where($order_field, $order_value);

        $query = $this->db->get();

        if ($query->num_rows() === 0) {
            return NULL;
        }

        $order = $query->row();

        return (object) array(
            'code'              => $order->ord_code,
            'discount'          => $order->ord_discount,
            'delivery_total'    => $order->ord_delivery_total,
            'delivery_discount' => $order->ord_delivery_discount,
            'name'              => $order->ord_name,
            'num_items'         => $order->ord_num_items,
            'owned_by'          => $order->ord_owned_by,
            'status'            => $order->ord_status,
            'subtotal'          => $order->ord_subtotal,
            'total_amount'      => $order->ord_total_amount,
            'uuid'              => $order->ord_uuid
        );
    }

    /**
     * Get a customer order by code.
     *
     * @param  string $customer_code
     * @param  string $order_code
     * @return object Order detail object.
     */
    public function get_customer_order_by_code($customer_code, $order_code) {
        return $this->get_customer_order(
                $customer_code, 'ord_code', $order_code);
    }

    /**
     * Get a customer order by ID.
     *
     * @param  string $customer_code
     * @param  string $order_id
     * @return object Order detail object.
     */
    public function get_customer_order_by_id($customer_code, $order_id) {
        return $this->get_customer_order(
                $customer_code, 'ord_uuid', $order_id);
    }

    /**
     * Gets details of an order for a given customer by the given transaction
     * ID.
     *
     * @param  string $customer_code
     * @param  string $payment_token
     * @return object Order detail object.
     */
    public function get_customer_order_by_transaction(
            $customer_code, $transaction_id) {

        $this->db->select('txn_ord_uuid')
                ->from(DB_TABLE_TRANSACTION)
                ->where('txn_transaction_id', $transaction_id);

        $txn_query = $this->db->get();

        $txn = $txn_query->row();

        return $this->get_customer_order_by_id(
                $customer_code, $txn->txn_ord_uuid);
    }

    public function get_order_address($customer_code, $order_code) {

        $order = $this->get_customer_order_by_code($customer_code, $order_code);

        $this->db->select('oad_ord_uuid, oad_code, oad_address_type, oad_name,
            oad_house, oad_street, oad_town, oad_city, oad_country,
            oad_postcode')
                ->from(DB_TABLE_ORDER_ADDRESS)
                ->where('oad_ord_uuid', $order->uuid);

        $query = $this->db->get();

        $row = $query->row();

        return (object) array(
            'city'     => $row->oad_city,
            'code'     => $row->oad_code,
            'country'  => $row->oad_country,
            'house'    => $row->oad_house,
            'name'     => $row->oad_name,
            'postcode' => $row->oad_postcode,
            'street'   => $row->oad_street,
            'town'     => $row->oad_town,
            'type'     => $row->oad_address_type,
            'uuid'     => $row->oad_ord_uuid
        );
    }

    /**
     * Get lines for a given customer and order code.
     *
     * @param  string $customer_code
     * @param  string $order_code
     * @return array  Array of order line detail objects.
     */
    public function get_order_lines($customer_code, $order_code) {

        $order = $this->get_customer_order_by_code($customer_code, $order_code);

        $this->db->select('orl_code, orl_name, orl_product_code, orl_quantity,
            orl_total_amount, orl_unit_price')
                ->from(DB_TABLE_ORDER_LINE)
                ->where('orl_ord_uuid', $order->uuid);

        $query = $this->db->get();

        $order_lines = array();

        foreach ($query->result() as $row) {
            array_push($order_lines, (object) array(
                'code'         => $row->orl_code,
                'name'         => $row->orl_name,
                'product_code' => $row->orl_product_code,
                'quantity'     => $row->orl_quantity,
                'total_amount' => $row->orl_total_amount,
                'unit_price'   => $row->orl_unit_price
            ));
        }

        return $order_lines;
    }

    /**
     * Get an array list of orders belonging to a customer.
     *
     * @param string $customer_code
     * @return array An array of address objects.
     */
    public function get_orders($customer_code) {

        $this->order_select()
                ->where('ord_owned_by', $customer_code)
                ->order_by('ord_created_dt', 'desc');

        $query = $this->db->get();

        $orders = array();

        foreach ($query->result() as $row) {
            array_push($orders, (object) array(
                'code'              => $row->ord_code,
                'name'              => $row->ord_name,
                'subtotal'          => $row->ord_subtotal,
                'discount'          => $row->ord_discount,
                'delivery_total'    => $row->ord_delivery_total,
                'delivery_discount' => $row->ord_delivery_discount,
                'total_amount'      => $row->ord_total_amount,
                'num_items'         => $row->ord_num_items
            ));
        }

        return $orders;
    }

    private function generate_order_code() {
        $unique_code = FALSE;

        while (!$unique_code) {
            $unique_code = generate_unique_code();
        }

        return $unique_code;
    }

    /**
     * Returns a select query to select orders.
     *
     * @access private
     * @return object A select query for orders.
     */
    private function order_select() {
        return $this->db->select('ord_uuid, ord_code, ord_status, ord_owned_by,
            ord_name, ord_subtotal, ord_discount, ord_delivery_total,
            ord_delivery_discount, ord_total_amount, ord_num_items')
                ->from(DB_VIEW_ORDER);
    }
}
/* End of file order_model.php */
/* Location: ./application/models/order_model.php */