<?php if (! defined('BASEPATH')) exit;

class Attribute_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $this->load->database();
    }

    /**
     * Get details of an attribute value by the given type and code.
     * @param string $attribute_code
     * @param Description string $value_code
     * @return object An object containing the attibute value details.
     */
    function get_attribute_value($attribute_code, $value_code) {
        $query = $this->db->query(
                "SELECT atv_code, atv_name, atv_description, atv_text
                 FROM   v_attribute_value
                 WHERE  atv_atr_code = ?
                 AND    atv_code = ?
                 AND    atv_status = 'active'
                 LIMIT  1",
                array($attribute_code, $value_code));

        $result = array();

        foreach ($query->result() as $row) {
            $result = array(
                'code' => $row->atv_code,
                'name' => $row->atv_name,
                'text' => $row->atv_text
            );
        }

        return (object) $result;
    }
}
/* End of file attribute_model.php */
/* Location: ./application/models/attribute_model.php */