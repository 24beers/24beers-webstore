<?php if (!defined('BASEPATH')) { exit; }

class Kitty_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('elasticsearch');
    }

    public function get_promotion() {
        $this->elasticsearch->set_index(DSO_INDEX_STORE);
        $this->elasticsearch->set_type(DSO_TYPE_PROMOTION);
    }

    
}
/* End of file promotion_model.php */
/* Location: ./application/models/promotion_model.php */