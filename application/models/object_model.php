<?php if (! defined('BASEPATH')) { exit; }

class Object_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->database();
    }

    /**
     * Create a base <code>object</code> entry and return the ID for the type
     * entry to be created.
     *
     * @access public
     * @param  string  $code
     * @param  string  $name
     * @param  string  $type
     * @param  integer $owned_by Optional.
     * @return integer Newly created UUID.
     */
    public function create_object($obj_code, $name, $type, $status,
            $created_by_code, $owned_by_code='') {

        $now = database_datetime();

        $obj_uuid = generate_uuid();

        $obj_data = array(
            'obj_uuid'        => $obj_uuid,
            'obj_code'        => $obj_code,
            'obj_name'        => $name,
            'obj_type'        => $type,
            'obj_status'      => $status,
            'obj_created_by'  => $created_by_code,
            'obj_created_dt'  => $now,
            'obj_modified_by' => $created_by_code,
            'obj_modified_dt' => $now,
            'obj_owned_by'    => $owned_by_code
        );

        /* Persist object within a transaction. */
        $this->db->trans_begin();
        $this->db->insert(DB_TABLE_OBJECT, $obj_data);

        /* Only return the UUID if the transaction was successful. */
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            return $obj_uuid;
        }

        /* Default action: rollback database transaction and show error. */
        $this->db->trans_rollback();

        log_message(LOG_LEVEL_ERROR, LOG_DB_FAILED_CREATE_OBJECT);
        show_error(ERROR_DB_FAILED_CREATE_OBJECT);
    }

    /**
     * Update a base <code>object</code> record in the database.
     *
     * @param  string  $obj_code
     * @param  string  $user_code
     * @return boolean True if object record is updated successfully, otheriwse
     *                 false.
     */
    public function update_object($obj_uuid, $user_code, $name='') {

        $update = array(
            'obj_modified_by' => $user_code,
            'obj_modified_dt' => database_datetime()
        );

        if (!empty($name)) {
            $update['obj_name'] = $name;
        }

        $this->db->trans_start();

        $this->db->update(DB_TABLE_OBJECT,
                $update,
                array(
                    'obj_uuid' => $obj_uuid
                ));

        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            return TRUE;
        }

        log_message(LOG_ERR, LOG_DB_FAILED_UPDATE_OBJECT);
        return FALSE;
    }
}
/* End of file object_model.php */
/* Location: ./application/models/object_model.php */