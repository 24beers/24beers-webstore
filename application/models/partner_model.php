<?php if (! defined('BASEPATH')) { exit; }
/**
 * 
 */
class Partner_model extends CI_Model {

    private $partner;

    public function get_config() {
        if (isset($this->partner) && isset($this->partner->config)) {
            return $this->partner->config;
        }
    }

    public function get_partner() {
        if (isset($this->partner)) {
            return $this->partner;
        }
    }

    public function load_partner($partner_code) {
        if (empty($this->partner)) {
            $this->load->library('elasticsearch');
            $this->elasticsearch->set_type('partner');
            $this->partner = $this->elasticsearch->get($partner_code);
        }
    }
}
/* End of file partner_model.php */
/* Location: ./application/models/partner_model.php */