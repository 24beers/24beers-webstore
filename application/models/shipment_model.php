<?php if (! defined('BASEPATH')) { exit; }

class Shipment_model extends CI_Model {

    /**
     * Create a shipment record in the database from an order and order lines.
     * Return the UUID if the data is successfully persisted, otherwise return
     * false.
     *
     * @param  object $order   Order object.
     * @param  object $address Address object.
     * @return boolean Return false if shipment already exists for order,
     *                 otherwise return true.
     */
    public function create_shipment($order, $address) {

        $existing_shipment = $this->get_shipment_by_order_uuid($order->uuid);

        if (!empty($existing_shipment)) {
            log_message(LOG_LEVEL_ERROR, LOG_DB_FAILED_CREATE_OBJECT);
            return FALSE;
        }

        $shipment_lines = $this->_process_order_line_data($order->order_lines);

        $this->load->library('mongo_db');

        $this->mongo_db->insert(DB_COLLECTION_SHIPMENT, array(
            'order_code'        => $order->code,
            'order_uuid'        => $order->uuid,
            'shipment_uuid'     => generate_uuid(),
            'shipment_created'  => database_datetime(),
            'shipment_modified' => database_datetime(),
            'shipment_lines'    => $shipment_lines,
            'shipment_status'   => STATUS_NEW,
            'delivery_address'  => array(
                'name'     => $address->name,
                'house'    => $address->house,
                'street'   => $address->street,
                'town'     => $address->town,
                'city'     => $address->city,
                'country'  => $address->country,
                'postcode' => $address->postcode
            )
        ));

        return TRUE;
    }

    /**
     * Get a customer order by the given field name and key value.
     * 
     * @param  string $order_uuid
     * @param  string $order_field
     * @param  string $order_value
     * @return object Order detail object.
     */
    public function get_shipment_by_order_uuid($order_uuid) {

        $this->load->library('mongo_db');

        $result = $this->mongo_db->get_where(DB_COLLECTION_SHIPMENT,
                array('order_uuid' => $order_uuid),
                1);

        if (count($result) === 0) {
            return NULL;
        }

        $shipment = $result[0];

        $shipment_lines = array();

        foreach ($shipment['shipment_lines'] as $line) {
            array_push($shipment_lines, (object) array(
                'sku_code'     => $line['sku_code'],
                'sku_quantity' => $line['sku_quantity']
            ));
        }

        return (object) array(
            'order_uuid'     => $shipment['order_uuid'],
            'shipment_uuid'  => $shipment['shipment_uuid'],
            'shipment_lines' => $shipment_lines,
            'status'         => $shipment['status']
        );
    }

    public function update_shipment() {

    }

    /**
     * Convert the given array of order lines into hash of shipment line data.
     * @param  array  $order_lines Array of order line objects.
     * @return string JSON encoded shipment line data.
     */
    private function _process_order_line_data($order_lines) {

        $shipment_lines = array();

        foreach ($order_lines as $line) {
            array_push($shipment_lines, array(
                'sku_code'     => $line->code,
                'sku_quantity' => $line->quantity
            ));
        }

        return $shipment_lines;
    }
}
/* End of file shipment_model.php */
/* Location: ./application/models/shipment_model.php */