<?php if (!defined('BASEPATH')) { exit; }

class Account_model extends CI_Model {

    /**
     * @access private
     * @var    array   Account instance.
     */
    private $account;

    public function __construct() {
        parent::__construct();

        $this->account = array(
            DSO_FIELD_ISLOADED => FALSE
        );

        $this->load->library('mongo_db');
    }

    /**
     * Credit an account with the given amount.
     * @param  real         $credit_amount
     * @return boolean|real False if account is not loaded, otherwise return account value.
     */
    public function credit_account($credit_amount) {
        if (!$this->_is_loaded()) {
            return FALSE;
        }

        $this->account[DSO_FIELD_ACCT_VALUE] += $credit_amount;
        $this->account[DSO_FIELD_MODIFYDT] = time();

        $this->_add_transaction($this->_create_new_transaction($credit_amount, TYPE_CREDIT));

        return $this->account[DSO_FIELD_ACCT_VALUE];
    }

    public function get_account() {
        if (!$this->_is_loaded()) {
            return array();
        }
        return $this->account;
    }

    /**
     * Get a single account for a given customer code and account type.
     * @param  string $customer_code
     * @param  string $account_type
     */
    public function load_customer_account($customer_code, $account_type, $create_if_not_exist=FALSE) {
        if (!$this->_is_loaded()) {
            $accounts = $this->mongo_db->get_where(DSO_COLLECTION_ACCOUNT, array(
                DSO_FIELD_OWNEDBY => $customer_code,
                DSO_FIELD_STATUS  => STATUS_ACTIVE,
                DSO_FIELD_TYPE    => $account_type
            ));

            if (empty($accounts) && $create_if_not_exist) {
                $this->account = $this->_create_new_account($customer_code, $account_type);
                $this->account[DSO_FIELD_ISLOADED] = TRUE;
            } elseif (count($accounts) == 1) {
                $this->account = $accounts[0];
                $this->account[DSO_FIELD_ISLOADED] = TRUE;
            } elseif (count($accounts) > 1) {
                log_error(LOG_ACCT_FOUNDTOOMANY);
                show_error(ERROR_TECHNICAL);
            }
        }
    }

    public function save() {
        if (!$this->_is_loaded()) {
            return;
        }

        if ($this->account[DSO_FIELD_STATUS] === STATUS_NEW) {
            $this->account[DSO_FIELD_STATUS] = STATUS_ACTIVE;
            $this->mongo_db->insert(DSO_COLLECTION_ACCOUNT, $this->account);
        } else {
            $this->mongo_db->update(DSO_COLLECTION_ACCOUNT, $this->account);
        }
    }

    /**
     * Add the given transaction to the list of transactions on the account.
     * @param array $txn Account transaction associative array.
     */
    private function _add_transaction($txn) {
        if ($this->_is_loaded()) {
            array_push($this->account[DSO_FIELD_ACCT_TXN], $txn);
        }
    }

    /**
     * Return a new account owned by the given customer code and of the account type.
     * @param  string $customer_code
     * @param  string $account_type
     * @return array  New account.
     */
    private function _create_new_account($customer_code, $account_type) {
        return array(
            DSO_FIELD_ACCT_TXN   => array(),
            DSO_FIELD_ACCT_VALUE => 0.00,
            DSO_FIELD_CREATEDT   => time(),
            DSO_FIELD_OWNEDBY    => $customer_code,
            DSO_FIELD_STATUS     => STATUS_NEW,
            DSO_FIELD_TYPE       => $account_type
        );
    }

    /**
     * Create a new account transaction.
     * @param  real   $amount Amount credited or debited in transaction.
     * @param  string $type   Transaction type (credit|debit)
     * @return array  New account transaction.
     */
    private function _create_new_transaction($amount, $type) {
        return array(
            DSO_FIELD_AMOUNT   => $amount,
            DSO_FIELD_CREATEDT => time(),
            DSO_FIELD_TYPE     => $type
        );
    }

    private function _is_loaded() {
        if (isset($this->account[DSO_FIELD_ISLOADED]) && $this->account[DSO_FIELD_ISLOADED]) {
            return TRUE;
        }
        return FALSE;
    }
}
/* End of file account_model.php */
/* Location: ./application/models/account_model.php */