<?php if (! defined('BASEPATH')) { exit; }

class Basket_model extends CI_Model {

    const BASKET_KEY_PREFIX = 'store:basket:';

    function __construct() {
        parent::__construct();

        $this->load->library('redis');
    }

    /**
     * Add an item to a basket.  The entry is made up of a product code and
     * a quantity.
     *
     * @param  string  $basket_id
     * @param  string  $item_code
     * @param  string  $item_name
     * @param  int     $quantity
     * @param  float   $sale_price
     * @return boolean False if there is a problem adding item to basket,
     *                 otherwise true if it's successful.
     */
    public function add_item_to_basket($basket_key, $item_code, $item_name,
            $quantity, $sale_price) {

        if (empty($basket_key)) {
            log_message(LOG_LEVEL_ERROR, LOG_BASKET_KEY_NOTSPECIFIED);
            return FALSE;
        }

        if (empty($item_code)) {
            log_message(LOG_LEVEL_ERROR, LOG_ITEM_CODE_NOTSPECIFIED);
            return FALSE;
        }

        if (empty($quantity)) {
            log_message(LOG_LEVEL_ERROR, LOG_QUANTITY_NOTSPECIFIED);
            return FALSE;
        }

        /* Add item code to list of items in basket. */
        $this->redis->sadd(
                Basket_model::BASKET_KEY_PREFIX . $basket_key . ':items',
                $item_code);

        /* If this item already exists in the basket sum the quantity
         * and line total. */
        $item = $this->get_basket_item($basket_key, $item_code);

        if (!empty($item) && isset($item->quantity)) {
            $quantity += $item->quantity;
        }

        $this->redis->hmset(Basket_model::BASKET_KEY_PREFIX . $basket_key .
                ':item:' . $item_code,
                'code', $item_code,
                'name', $item_name,
                'quantity', $quantity,
                'sale_price', $sale_price);

        return TRUE;
    }


    /**
     * Adjust the quantity of the basket item by the given amount.
     * @param mixed $basket_id
     * @param string $item_code
     * @param int $amount
     */
    public function adjust_item_quantity($basket_key, $item_code, $amount) {

        if (empty($basket_key)) {
            log_message(LOG_LEVEL_ERROR, LOG_BASKET_KEY_NOTSPECIFIED);
            return;
        }

        if (empty($item_code)) {
            log_message(LOG_LEVEL_ERROR, LOG_ITEM_CODE_NOTSPECIFIED);
            return;
        }

        /* Treat unspecified amount as zero so no change is made. */
        if (empty($amount)) {
            return;
        }

        $item = $this->get_basket_item($basket_key, $item_code);

        /* Do not allow the quantity to fall below zero */
        if ($item->quantity + $amount < 0) {
            $this->redis->hset($basket_key, $item_code, 0);
        } else {
            $this->redis->hincrby(Basket_model::BASKET_KEY_PREFIX .
                    "$basket_key:item:$item_code",
                    'quantity', $amount);
        }
    }

    /**
     * Decrement the quantity of the basket item.
     * @param mixed $basket_id
     * @param string $item_code
     */
    public function decrease_item_quantity($basket_key, $item_code) {

        if (empty($basket_key)) {
            log_message(LOG_LEVEL_ERROR, LOG_BASKET_KEY_NOTSPECIFIED);
            return;
        }

        /* If quantity is decremented to zero, remove it from basket. */
        if ($this->get_item_quantity($basket_key, $item_code) - 1 <= 0) {
            $this->delete_basket_item($basket_key, $item_code);
        } else {
            $this->adjust_item_quantity($basket_key, $item_code, -1);
        }
    }

    public function delete_basket($basket_key) {

        if (empty($basket_key)) {
            log_message(LOG_LEVEL_ERROR, LOG_BASKET_KEY_NOTSPECIFIED);
            return;
        }

        $basket_items = $this->get_basket_items($basket_key);

        /* Delete each item from basket. */
        foreach ($basket_items as $item) {
            $this->delete_basket_item($basket_key, $item->code);
        }

        /* Delete basket contents list. */
        $this->redis->del(Basket_model::BASKET_KEY_PREFIX .
                $basket_key . ':items');
    }

    /**
     * Delete the given item from the basket.
     *
     * @param string $basket_key
     * @param string $item_code
     */
    public function delete_basket_item($basket_key, $item_code) {

        if (empty($basket_key)) {
            log_message(LOG_LEVEL_ERROR, LOG_BASKET_KEY_NOTSPECIFIED);
            return;
        }

        /* Delete the item details. */
        $this->redis->del(Basket_model::BASKET_KEY_PREFIX .
                $basket_key . ':item:' . $item_code);

        /* Remove the item from basket item list. */
        $this->redis->srem(Basket_model::BASKET_KEY_PREFIX .
                $basket_key . ':items', $item_code);
    }

    /**
     * Get a single item from a basket.
     *
     * @param  mixed  $basket_id
     * @param  string $item_code
     * @return object Basket item
     */
    public function get_basket_item($basket_key, $item_code) {

        if (empty($basket_key)) {
            log_message(LOG_LEVEL_ERROR, LOG_BASKET_KEY_NOTSPECIFIED);
            return NULL;
        }

        if (empty($item_code)) {
            log_message(LOG_LEVEL_ERROR, LOG_ITEM_CODE_NOTSPECIFIED);
            return NULL;
        }

        $item_list = $this->redis->hgetall(Basket_model::BASKET_KEY_PREFIX .
                "$basket_key:item:$item_code");

        $list_size = sizeof($item_list);

        $item = array();

        for ($i = 0; $i < $list_size; $i += 2) {
            $item[$item_list[$i]] = $item_list[$i + 1];
        }

        return (object) $item;
    }

    /**
     * Get contents of the customer's basket by the given basket key.
     *
     * @param string $basket_key
     * @return array An array of basket items.
     */
    public function get_basket_items($basket_key) {

        if (empty($basket_key)) {
            log_message(LOG_LEVEL_ERROR, LOG_BASKET_KEY_NOTSPECIFIED);
            return;
        }

        $item_codes = $this->redis->smembers(
                Basket_model::BASKET_KEY_PREFIX . $basket_key . ':items');

        if (empty($item_codes)) {
            return (object) array();
        }

        $basket = array();

        foreach ($item_codes as $code) {
            $item = $this->get_basket_item($basket_key, $code);
            array_push($basket, $item);
        }

        return $basket;
    }

    /**
     * Get the quantity of a given item in the basket. Return zero if the item
     * isn't in the basket or if there is no basket.
     * @param string $basket_key Key for requested basket.
     * @param string $item_code Code for basket item.
     * @return int Quantity of given item.
     */
    public function get_item_quantity($basket_key, $item_code) {

        $item = $this->get_basket_item($basket_key, $item_code);

        if (isset($item)) {
            return $item->quantity;
        }

        return 0;
    }

    /**
     * Increment the quantity of the basket item.
     * 
     * @param mixed $basket_id
     * @param string $item_code
     */
    public function increase_item_quantity($basket_key, $item_code) {

        if (empty($basket_key)) {
            log_message(LOG_LEVEL_ERROR, LOG_BASKET_KEY_NOTSPECIFIED);
            return;
        }

        $this->adjust_item_quantity($basket_key, $item_code, 1);
    }
}
/* End of file basket_model.php */
/* Location: ./application/models/basket_model.php */