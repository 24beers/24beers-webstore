<?php if (! defined('BASEPATH')) { exit; }

class Search_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->library('elasticsearch');
        $this->elasticsearch->set_type('product');
    }

    /**
     * Get search results where the exact term matches the given collection
     * code.
     * 
     * @param string $collection_code
     * @return array An array of products from the collection.
     */
    public function get_result_by_collection($collection_code) {
        $result = $this->elasticsearch->term_query('collection.code', $collection_code);

        if (!isset($result->hits) || !isset($result->hits->hits)) {
            return array();
        }

        $product_results = array();

        foreach ($result->hits->hits as $hit) {
            array_push($product_results,
                    generate_product_from_search_source($hit->_source));
        }

        return $product_results;
    }

    /**
     * Returns search results for a term query based on given field and term.
     * @param  string $field Field to search on.
     * @param  string $term  Search term.
     * @return array  Array of search result sources.
     */
    public function get_result_by_term($field, $term) {
        $result = $this->elasticsearch->term_query($field, $term);
        $results = array();

        if (isset($result->hits) && isset($result->hits->hits)) {
            foreach ($result->hits->hits as $hit) {
                array_push($results, $hit->_source);
            }
        }

        return $results;
    }

    /**
     * Get facet results with no clause.
     *
     * @return object A search facet object.
     */
    public function get_all_facet_result() {
        return $this->get_facet_search_result(array('match_all' => array()));
    }

    /**
     * Get facet results based on given query clause.
     *
     * @param string $query
     * @return object A search facet object.
     */
    public function get_query_facet_result($query) {
        if (empty($query)) {
            $query = '*';
        }

        return $this->get_facet_search_result(array(
            'query_string' => array('query' => $query)
        ));
    }

    public function get_term_facet_result($field, $term) {
        return $this->get_facet_search_result(array(
            'term' => array($field => $term)
        ));
    }

    /**
     * Gets the search result and a faceted list of categories from the search
     * server based on the search query.
     *
     * @param array $query_data
     * @return object
     */
    public function get_facet_search_result($query_data) {

        $facet_data = array(
            'style' => array(
                'terms' => array(
                    'all_terms' => true,
                    'field'     => 'style.code',
                    'order'     => 'term'
                )
            ),
            'brewery' => array(
                'terms' => array(
                    'all_terms' => true,
                    'field'     => 'brewery.code',
                    'order'     => 'term'
                )
            )
        );

        $result = $this->elasticsearch->facet_search($query_data, $facet_data);

        $product_results = array();

        foreach ($result->hits->hits as $hit) {
            array_push($product_results,
                    generate_product_from_search_source($hit->_source));
        }

        $cat_style = (object) array(
            'code'   => 'style',
            'label'  => 'Style',
            'facets' => $result->facets->style->terms
        );

        $cat_brewery = (object) array(
            'code'   => 'brewery',
            'label'  => 'Brewery',
            'facets' => $result->facets->brewery->terms
        );

        return (object) array(
            'result_count'     => $result->hits->total,
            'product_list'     => $product_results,
            'facet_categories' => array(
                (object) $cat_style,
                (object) $cat_brewery
            )
        );
    }
}
/* End of file collection_model.php */
/* Location: ./application/models/collection_model.php */