<?php if (! defined('BASEPATH')) { exit; }

class Brewery_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('elasticsearch');
        $this->elasticsearch->set_type('brewery');
    }

    /**
     * Get details of the brewery by the given brewery code.
     * @param string $code
     */
    public function get_brewery_by_code($code) {
        $result = $this->elasticsearch->get($code);

        if ($result->exists != 1) {
            return NULL;
        }

        return $this->generate_from_source($result->_source);
    }

    /**
     * Get a list of all breweries in the search engine.
     * @return array List of brewery objects.
     */
    public function get_brewery_list() {
        $this->elasticsearch->set_query('*');
        $result = $this->elasticsearch->query();
        $result_list = array();

        if (isset($result->hits) && $result->hits->total > 0) {
            foreach ($result->hits->hits as $hit) {
                array_push($result_list,
                        $this->generate_from_source($hit->_source));
            }
        }

        return $result_list;
    }

    /**
     * Generate a brewery object from a search result source.
     *
     * @param  object $source Source object from search server.
     * @return object Brewery object.
     */
    private function generate_from_source($source) {
        $brewery = $source->brewery;

        return (object) array(
            'code'        => $brewery->code,
            'description' => $brewery->description,
            'name'        => $brewery->name,
            'text'        => $brewery->text
        );
    }
}
/* End of file brewery_model.php */
/* Location: ./application/models/brewery_model.php */