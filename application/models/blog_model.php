<?php if (! defined('BASEPATH')) { exit; }
/**
 * 
 */
class Blog_model extends CI_Model {

    /**
     * Get the last "n" created blogs.
     * @param integer $n
     * @return array List of blog entries
     */
    function get_last_n_blogs($n) {
        if (!isset($n) || !is_int($n)) { return array(); }

        $this->load->library('redis');
        $id_list = $this->redis->lrange('blog:entries', 0, $n);

        if (empty($id_list)) { return array(); }

        $blog_list = array();

        foreach ($id_list as $id) {
            array_push($blog_list, $this->get_blog_by_id($id));
        }

        return $blog_list;
    }

    /**
     * Get whole blog entry by given ID.
     * @param string $id
     * @return array Blog entry
     */
    function get_blog_by_id($id) {
        $this->load->library('redis');

        if ($this->redis->command("EXISTS blog:$id") == 0) {
            return false;
        }

        return (object) array(
            'id'       => $id,
            /* TODO: Merge this into one call to Redis server. */
            'abstract' => $this->redis->command("HGET blog:$id abstract"),
            'author'   => $this->redis->command("HGET blog:$id author"),
            'date'     => $this->redis->command("HGET blog:$id date"),
            'text'     => $this->redis->command("HGET blog:$id text"),
            'title'    => $this->redis->command("HGET blog:$id title")
        );
    }
}
/* End of file blog_model.php */
/* Location: ./application/models/blog_model.php */