<?php if (! defined('BASEPATH')) exit;

class Collection_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $this->load->library('elasticsearch');
        $this->elasticsearch->set_type('collection');
    }

    /**
     * Get details of an attribute value by the given type and code.
     * @param string $type
     * @param Description string $code
     * @return object An object containing the attibute value details.
     */
    function get_attribute_value($type, $code) {
        $query = $this->db->query(
                "SELECT atv_code, atv_name, atv_description, atv_text
                 FROM   v_attribute_value
                 WHERE  atv_type = ?
                 AND    atv_code = ?
                 AND    atv_status = 'active'
                 LIMIT  1",
                array($type, $code));

        $result = array();

        foreach ($query->result() as $row) {
            array_push($result, $row);
        }

        return (object) $result;
    }
}
/* End of file collection_model.php */
/* Location: ./application/models/collection_model.php */