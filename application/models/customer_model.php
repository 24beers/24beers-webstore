<?php if (! defined('BASEPATH')) { exit; }
/**
 * 
 */
class Customer_model extends CI_Model {

    const CUSTOMER_KEY_PREFIX = 'store:customer:';

    function __construct() {
        parent::__construct();

        $this->load->database();
    }

    /**
     * Add the product code for a product the customer has reviewed.
     * 
     * @param string $customer_code
     * @param string $product_code
     */
    public function add_product_review($customer_code, $product_code) {
        $review_key = Customer_model::CUSTOMER_KEY_PREFIX . $customer_code .
                ':products_reviewed';
        $this->load->library('redis');
        $this->redis->sadd($review_key, $product_code);
    }

    /**
     * Insert a new address for a customer.
     *
     * @param string $name
     * @param string $house
     * @param string $street
     * @param string $town
     * @param string $city
     * @param string $country
     * @param string $postcode
     * @param string $owner_code
     */
    public function create_address($address_code, $name, $house, $street, $town,
            $city, $country, $postcode, $owner_code) {

        $this->load->model('object_model');

        /* Persist an object and get the UUID for the new object. */
        $obj_uuid = $this->object_model->create_object($address_code, $name,
                TYPE_OBJECT_ADDRESS, STATUS_ACTIVE, $owner_code, $owner_code);

        $address_data = array(
            'add_uuid'     => $obj_uuid,
            'add_house'    => $house,
            'add_street'   => $street,
            'add_town'     => $town,
            'add_city'     => $city,
            'add_country'  => $country,
            'add_postcode' => strtoupper(
                    preg_replace('/[^0-9A-z]/', '', $postcode))
        );

        /* Create insert query for user data. */
        $query = $this->db->insert_string(DB_TABLE_ADDRESS, $address_data);

        /* Begin transaction. */
        $this->db->trans_start();

        /* Insert into address table. */
        $this->db->query($query);

        /* Rollback transaction if queries fail, otherwise commit. */
        $this->db->trans_complete();
    }

    /**
     * <p>Persist customer account details to the datastore.</p>
     * <p>The password is already a MD5 string when received from the form
     * validation. The MD5 is encrypted with bcrypt before persisting.</p>
     * @param string $name
     * @param string $phone
     * @param string $email
     * @param string $password
     * @param string $dob
     * @return mixed Customer's unique user ID.
     */
    public function create_customer($name, $phone, $email, $password, $dob) {

        /* Generate the customer code from the customer name. */
        $customer_code = generate_object_code($name);

        $this->load->model('object_model');

        /* Create object entry. */
        $obj_uuid = $this->object_model->create_object($customer_code, $name,
                TYPE_OBJECT_USER, STATUS_ACTIVE, $customer_code);

        $this->load->library('encrypt');

        $salt = $this->encrypt->generate_sha512_salt();
        $encr_password = $this->encrypt->encrypt($password, $salt);

        $user_data = array(
            'usr_uuid'          => $obj_uuid,
            'usr_main_phone'    => preg_replace('/[^0-9]/', '', $phone),
            'usr_email'         => $email,
            'usr_password'      => $encr_password,
            'usr_salt'          => $salt,
            'usr_date_of_birth' => $dob,
            'usr_user_type'     => TYPE_USER_CUSTOMER
        );

        /* Begin transaction. */
        $this->db->trans_start();

        /* Create insert query for user data. */
        $user_query = $this->db->insert_string(DB_TABLE_USER, $user_data);

        /* Insert into user table. */
        $this->db->query($user_query);

        /* Rollback transaction if queries fail, otherwise commit. */
        $this->db->trans_complete();

        return $customer_code;
    }

    /**
     * Create a lookup between the customer's email address and password reset
     * token. Lookup exists for 10 minutes before being expired.
     *
     * @param string $email Customer's email address.
     * @param string $token Password reset lookup token.
     */
    public function create_password_reset_lookup($token, $email) {

        $this->load->library('redis');

        /* Set the email lookup for the token with an expiry of 10 minutes. */
        $this->redis->setex(Customer_model::CUSTOMER_KEY_PREFIX .
                "password_reset_token:$token", 600, $email);
    }

    /**
     * Update the status of an address to "deleted".
     * @param string $customer_code
     * @param string $address_code
     */
    public function delete_address($customer_code, $address_code) {

        $result = $this->db->update(DB_TABLE_OBJECT,
                array(
                    'obj_modified_by' => $customer_code,
                    'obj_modified_dt' => database_datetime(),
                    'obj_status'      => STATUS_DELETED
                ),
                array(
                    'obj_owned_by' => $customer_code,
                    'obj_code'     => $address_code
                ));

        if ($result === 0) {
            log_message(LOG_LEVEL_ERROR, LOG_FAILED_DELETE_ADDRESS);
        }
    }

    /**
     *
     * @access public
     * @param string $customer_code
     * @param string $address_code
     * @return object Customer address as an object.
     */
    public function get_address($customer_code, $address_code) {
        $this->address_select()
                ->where('add_owned_by', $customer_code)
                ->where('add_code', $address_code)
                ->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() === 0) {
            return null;
        }

        $row = $query->row();

        return (object) array(
            'code'     => $row->add_code,
            'name'     => $row->add_name,
            'house'    => $row->add_house,
            'street'   => $row->add_street,
            'town'     => $row->add_town,
            'city'     => $row->add_city,
            'country'  => $row->add_country,
            'postcode' => $row->add_postcode
        );
    }

    /**
     * Return an <code>array</code> of the addresses belonging to the given
     * customer.
     *
     * @param string $customer_code
     * @return array An array of address objects.
     */
    public function get_addresses($customer_code) {
        $this->load->database();
        $this->address_select()->where('add_owned_by', $customer_code);

        $query = $this->db->get();

        $addresses = array();

        foreach ($query->result() as $row) {
            array_push($addresses, (object) array(
                'code'     => $row->add_code,
                'name'     => $row->add_name,
                'house'    => $row->add_house,
                'street'   => $row->add_street,
                'town'     => $row->add_town,
                'city'     => $row->add_city,
                'country'  => $row->add_country,
                'postcode' => $row->add_postcode
            ));
        }

        return $addresses;
    }

    /**
     * Get an <code>array</code> of the addresses belonging to the current
     * customer in session.  If no array is returned, return zero, otherwise
     * return size of <code>array</code>.
     *
     * @access public
     * @return int
     */
    public function get_address_count($customer_code) {
        $addresses = $this->get_addresses($customer_code);

        if ($addresses === NULL || !isset($addresses) || empty($addresses)) {
            return 0;
        }

        return count($addresses);
    }

    /**
     * Get customer based on given "where" field and value.
     * @param string $where_field
     * @param string $where_value
     * @return object Customer details as an object.
     */
    public function get_customer($where_field, $where_value) {

        $this->db->select('cst_uuid, cst_name, cst_code, cst_priority,
            cst_status, cst_email, cst_password, cst_salt, cst_main_phone,
            cst_alternative_phone, cst_date_of_birth')
                ->from(DB_VIEW_CUSTOMER)
                ->where('cst_status', STATUS_ACTIVE)
                ->where($where_field, $where_value)
                ->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() === 0) {
            return NULL;
        }

        $row = $query->row();

        $customer = array(
            'alternative_phone' => $row->cst_alternative_phone,
            'code'              => $row->cst_code,
            'date_of_birth'     => $row->cst_date_of_birth,
            'email'             => $row->cst_email,
            'name'              => $row->cst_name,
            'main_phone'        => $row->cst_main_phone,
            'password'          => $row->cst_password,
            'priority'          => $row->cst_priority,
            'salt'              => $row->cst_salt,
            'status'            => $row->cst_status,
            'uuid'              => $row->cst_uuid
        );

        return (object) $customer;
    }

    /**
     * Get a customer by the given customer code.
     * @param string $code
     * @return object Customer object.
     */
    public function get_customer_by_code($code) {
        return $this->get_customer('cst_code', $code);
    }

    /**
     * Get a customer by the given email.
     * @param string $email
     * @return object Customer object.
     */
    public function get_customer_by_email($email) {
        return $this->get_customer('cst_email', $email);
    }

    /**
     * Return the customer code that corresponds to given password reset token.
     * 
     * @param  string $token Password reset token.
     * @return string Customer code.
     */
    public function get_password_reset_lookup($token) {
        $this->load->library('redis');
        return $this->redis->get(Customer_model::CUSTOMER_KEY_PREFIX .
                "password_reset_token:$token");
    }

    /**
     * Get an <code>array</code> of payment cards belonging to a given customer.
     * @param string $customer_code
     * @return array An array of payment card objects.
     */
    public function get_payment_cards($customer_code) {
        return array();
    }

    /**
     * Update the customer record with amended details.
     *
     * @param  string  $customer_code
     * @param  string  $name
     * @param  string  $email
     * @param  string  $phone
     * @param  string  $alternative_phone
     * @return boolean False if customer code not found in session or database
     *                 could not be updated, otherwise true.
     */
    public function update_customer(
            $customer_code, $name, $email, $phone, $alt_phone, $dob) {

        $customer = $this->get_customer_by_code($customer_code);

        if (!isset($customer)) {
            return FALSE;
        }

        $this->load->model('object_model');

        /* Update object entry. */
        $success = $this->object_model->update_object(
                $customer->uuid, $customer_code, $name);

        if (!$success) {
            return FALSE;
        }

        /* Begin transaction. */
        $this->db->trans_start();

        $this->db->update(DB_TABLE_USER,
                array(
                    'usr_email'             => $email,
                    'usr_main_phone'        => $phone,
                    'usr_alternative_phone' => $alt_phone,
                    'usr_date_of_birth'     => $dob
                ),
                array(
                    'usr_uuid'      => $customer->uuid,
                    'usr_user_type' => TYPE_USER_CUSTOMER
                ));

        /* Rollback transaction if queries fail, otherwise commit. */
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            log_message(LOG_ERR, LOG_DB_FAILED_UPDATE_CUSTOMER);
            return FALSE;
        }

        return $customer_code;
    }

    /**
     * Update a customer's password in the database.
     *
     * @param  string  $customer_code
     * @param  string  $password
     * @return boolean False if customer is not found using customer code or
     *                 transaction is not successful, otherwise true.
     */
    public function update_password($customer_code, $password) {

        $customer = $this->get_customer_by_code($customer_code);

        if (!isset($customer)) {
            return FALSE;
        }

        $this->load->model('object_model');

        $success = $this->object_model->update_object(
                $customer->uuid, $customer_code);

        if (!$success) {
            return FALSE;
        }

        /* Encrypt the new password with super strong encryption. */
        $this->load->library('encrypt');
        $salt = $this->encrypt->generate_sha512_salt();
        $encr_password = $this->encrypt->encrypt($password, $salt);

        $this->db->trans_start();

        $this->db->update(DB_TABLE_USER,
                array(
                    'usr_password' => $encr_password,
                    'usr_salt'     => $salt
                ),
                array(
                    'usr_uuid' => $customer->uuid
                ));

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            log_message(LOG_ERR, LOG_DB_FAILED_UPDATE_PASSWORD);
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Returns the select part of a query to select addresses.
     * @access private
     * @return object A select query for addresses.
     */
    private function address_select() {
        return $this->db->select('add_code, add_name, add_house, add_street,
            add_town, add_city, add_country, add_postcode')
                ->from(DB_VIEW_ADDRESS)
                ->where('add_status', STATUS_ACTIVE);
    }
}
/* End of file customer_model.php */
/* Location: ./application/models/customer_model.php */