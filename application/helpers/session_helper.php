<?php  if (!defined('BASEPATH')) { exit; }

if (!function_exists('get_custcode_from_session')) {
    function get_custcode_from_session() {
        $CI =& get_instance();
        $custcode = $CI->session->userdata(SESSION_CUSTOMER_CODE);

        if (empty($custcode)) {
            log_message(LOG_LEVEL_ERROR, LOG_SESSION_NOCUSTCODE);
            show_error(ERROR_SESSION_NOCUSTCODE);
        }

        return $custcode;
    }
}

if (!function_exists('is_authed')) {
    /**
     * Check if a visitor has been authed.
     *
     * @access public
     * @return bool
     */
    function is_authed() {
        $CI =& get_instance();
        if ($CI->session->userdata(SESSION_IS_AUTHED)) {
            return true;
        }
        return false;
    }
}
/* End of file session_helper.php */
/* Location: ./application/helpers/session_helper.php */