<?php if (!defined('BASEPATH')) exit;

/**
 * Credit Card Functions
 *
 * This helper module contains functions which can be used to manipulate credit
 * card numbers and related information.
 *
 * @package CodeIgniter
 * @subpackage Helpers
 * @category Helpers
 * @author Ben Mullard <ben@24beers.co.uk>
 */

if (!function_exists('card_expiry_valid')) {
    /**
     * Validates a card expiry date.  Finds the midnight on first day of the
     * following month and ensures that is greater than the current time (cards
     * expire at the end of the printed month).  Assumes basic sanity checks have
     * already been performed on month/year (i.e. length, numeric, etc).
     *
     * @param integer The expiry month shown on the card.
     * @param integer The expiry year printed on the card.
     * @return boolean Returns true if the card has not expired.
     */
    function card_expiry_valid($month, $year) {
        $expiry_date = mktime(0, 0, 0, ($month + 1), 1, $year);
        return ($expiry_date > time());
    }
}

if (!function_exists('card_number_clean')) {
    /**
     * Strips all non-numerics from the card number.
     *
     * @param string The card number to clean up.
     * @return string The stripped down card number.
     */
    function card_number_clean($cardnum) {
        return preg_replace('/[^0-9]/', '', $cardnum);
    }
}

if (!function_exists('card_number_valid')) {
    /**
     * Uses the {@link http://en.wikipedia.org/wiki/Luhn_algorithm Luhn algorithm (aka Mod10)}
     * to perform basic validation of a payment card number.
     *
     * @param string The card number to validate.
     * @return boolean True if valid according to the Luhn algorith.
     */
    function card_number_valid($cardnum) {
        $card_number = strrev(card_number_clean($cardnum));
        $sum = 0;

        for ($i = 0; $i < strlen($card_number); $i++) {
            $digit = substr($card_number, $i, 1);

            // Double every second digit
            if ($i % 2 == 1) {
                $digit *= 2;
            }

            // Add digits of 2-digit numbers together
            if ($digit > 9) {
                $digit = ($digit % 10) + floor($digit / 10);
            }

            $sum += $digit;
        }

        // If the total has no remainder it's OK
        return ($sum % 10 == 0);
    }
}

if (!function_exists('truncate_card')) {
    /**
     * Truncates a card number retaining only the first 4 and the last 4 digits.
     * It then returns the truncated form.
     *
     * @param string The card number to truncate.
     * @return string The truncated card number.
     */
    function truncate_card($cardnum) {
        $size = strlen($cardnum);
        $mid = str_repeat('x', ($size <= 7 ? 0 : $size - 7));
        return substr($cardnum, 0, 4) . $mid .  substr($cardnum, -3);
    }
}
/* End of file payment_card_helper.php */
/* Location: ./application/helpers/payment_card_helper.php */