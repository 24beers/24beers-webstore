<?php  if (!defined('BASEPATH')) { exit; }

if (!function_exists('bottles_needed_to_complete_basket')) {
    /**
     * Returns the number of bottles needed to complete basket.
     *
     * @param  integer $basket_size Size (number of items) of basket.
     * @return integer Number of bottles needed to complete basket.
     */
    function bottles_needed_to_complete_basket($basket_size) {

        if ($basket_size < STORE_MIN_ORDER_SIZE) {
            return STORE_MIN_ORDER_SIZE - $basket_size;
        }

        if ($basket_size % STORE_MIN_CASE_SIZE > 0) {
            return STORE_MIN_CASE_SIZE - ($basket_size % STORE_MIN_CASE_SIZE);
        }

        return 0;
    }
}

if (!function_exists('database_datetime')) {
    /**
     * Generate a datetime string suitable to use in a database.
     * @return string Database datetime.
     */
    function database_datetime() {
        return date('Y-m-d H:i:s');
    }
}

if (!function_exists('format_address_inline')) {
    /**
     * Format an address object into a single line string.
     *
     * @param object $address
     */
    function format_address_inline($address) {
        return $address->name . ', ' . $address->house . ', ' .
                $address->street . ', ' . $address->town . ', ' .
                $address->country . ', ' . $address->postcode;
    }
}

if (!function_exists('format_date')) {
    /**
     * Formats a given date into a standard format.
     * @param string $str_date
     * @return string
     */
    function format_date($str_date) {
        $arr_date = date_parse($str_date);
        $date = new DateTime();
        $date->setDate($arr_date['year'], $arr_date['month'], $arr_date['day']);
        return date_format($date, FORMAT_DATE);
    }
}

if (!function_exists('format_decimal_price')) {
    /**
     * Format a number as a price in decimal format.
     *
     * @param string $price Decimal price to be formatted as a price decimal.
     */
    function format_decimal_price($price) {
        return number_format($price, 2);
    }
}

if (!function_exists('format_displayable_price')) {
    /**
     * Format a number as a displayable price, including HTML currency symbol.
     * @param string $price Decimal price to be formatted into a displayable
     * string.
     */
    function format_displayable_price($price) {
        return '&pound;' . number_format($price, 2);
    }
}

if (!function_exists('format_message')) {
    function format_message($message, $replacements=array()) {
        return sprintf($message, $replacements);
    }
}

if (!function_exists('format_rating_avg')) {
    function format_rating_avg($sum, $count) {
        if ($count == 0) {
            return 0;
        }

        $avg = $sum / $count;
        $round_to = 0.5;
        return round($avg / $round_to) * $round_to;
    }
}

if (!function_exists('generate_address_code')) {
    function generate_address_code() {
        return generate_object_code('address');
    }
}

if (!function_exists('generate_breadcrumb')) {
    /**
     * <p>Generate a breadcrumb array with a default element for Home.<br>
     * <code>$crumbs</code> is an array of objects that encapsulate the crumb
     * information.</p>
     * <p>Calling the method with no arguments returns just the home link.</p>
     *
     * @param  array $crumbs Array of crumb information encapsualted in an
     *                       object.
     * @return array Array of the breadcrumb items.
     */
    function generate_breadcrumb($crumbs = NULL) {

        $breadcrumb = array(
            (object) array(
                'label' => 'Home',
                'link'  => URL_HOME
            )
        );

        foreach ($crumbs as $crumb) {
            if (is_object($crumb)) {
                array_push($breadcrumb, $crumb);
            } else {
                array_push($breadcrumb, (object) $crumb);
            }
        }

        return $breadcrumb;
    }
}

if (!function_exists('generate_code_from_string')) {
    /**
     * Generate a code suitable to be persisted as the <code>obj_code</code> from a
     * given name.
     *
     * @param string Name to convert to object code.
     * @return string Code to use as <code>obj_code</code>.
     */
    function generate_code_from_string($string) {
        return preg_replace('/[^A-z0-9\-]/', '',
                preg_replace('/[\W@]+/', '-', strtolower($string)));
    }
}

if (!function_exists('generate_object_code')) {
    /**
     * Generate a code suitable to be persisted as the <code>obj_code</code>
     * using a given prefix.
     *
     * @param  string $prefix Prefix to use when creating object code.
     * @return string Code to use as <code>obj_code</code>.
     */
    function generate_object_code($prefix) {
        return preg_replace(
                array('/[^A-z0-9\-]/', '/\s+/'),
                array('', '-'),
                strtolower($prefix)) .
                '-' . generate_uuid();
    }
}

if (!function_exists('generate_order_code')) {
    function generate_order_code() {
        return generate_object_code('order');
    }
}

if (!function_exists('generate_product_from_search_source')) {
    /**
     * Generate a product object from a search result source.
     *
     * @deprecated since version 1.0
     * @param string
     * @return object Product object.
     */
    function generate_product_from_search_source($source) {

        $product = $source->product;
        $style = $source->style;
        $brewery = $source->brewery;

        return (object) array(
            'abv'            => $product->abv,
            'brewery_name'   => $brewery->name,
            'brewery_code'   => $brewery->code,
            'code'           => $product->code,
            'description'    => $product->description,
            'displayable_list_price'
                             => format_displayable_price($product->list_price),
            'displayable_sale_price'
                             => format_displayable_price($product->sale_price),
            'hops'           => $product->hops,
            'list_price'     => $product->list_price,
            'malts'          => $product->malts,
            'name'           => $product->name,
            'sale_price'     => $product->sale_price,
            'size_container' => $product->size . ' ' . $product->container,
            'sku_code'       => $product->sku_code,
            'style_name'     => $style->name,
            'style_code'     => $style->code,
            'text'           => $product->text
        );
    }
}

if (!function_exists('generate_token')) {
    /**
     * Generate a unique random token with high entropy, making it very
     * difficult to predict.
     *
     * @return string Unique random token.
     */
    function generate_token() {
        /* Generate random number. */
        $token = mt_rand(0, mt_getrandmax());

        /* Append the app encryption key. */
        $ci =& get_instance();
        $token .= $ci->config->item('encryption_key');

        /* Make the token portable by converting to a high-entropy MD5 hash. */
        return md5(uniqid($token, TRUE));
    }
}

if (!function_exists('generate_unique_code')) {
    /**
     * <p>Generate a unique code by incrementing a stored  6 character code.</p>
     * <p>The last used code is obtained and the updated code is set within a
     * transaction. If the value of the key is changed between getting the value
     * and setting it back the transaction will fail and the function must be
     * called again.</p>
     * <p>The code is sequential so inherently insecure. When it's used as a key
     * there must be another mechanism to ensure an attacker cannot access
     * information by changing the code parameter.</p>
     * TODO: Make the code sequence random.
     *
     * @return string|boolean If the unique code is updated to the new value
     *                        successfully it's returned, otherwise false.
     */
    function generate_unique_code() {
        $key = "store:unique_code";

        $ci =& get_instance();
        $ci->load->library('redis');

        $ci->redis->watch($key);
        $unique_code = $ci->redis->get($key);

        if (empty($unique_code)) {
            log_message(LOG_LEVEL_ERROR, LOG_UNIQUE_CODE_NOTFOUND);
            $unique_code = "aaaaaa";
        } else {
            $unique_code++;
        }

        $ci->redis->multi();
        $ci->redis->set("store:unique_code", $unique_code);
        $response = $ci->redis->exec();

        if (!$response || empty($response)) {
            return FALSE;
        }

        return $unique_code;
    }
}

if (!function_exists('generate_uuid')) {
    /**
     * Generate a universally unique ID that can be used for sharding.
     *
     * @param integer $shard_id The ID to shard by.
     * @param integer $seq Incrementing sequence for the database table.
     * @return string Universally unique ID.
     */
    function generate_uuid($shard_id=0, $seq=0) {
        return floor((microtime(TRUE) * 1000) - APP_EPOCH_MS) << 23 |
                ($shard_id % APP_NUM_SHARDS) << 10 |
                ($seq % 1024);
    }
}

if (!function_exists('get_delivery_discount')) {
    function get_delivery_discount() {
        $ci =& get_instance();
        $ci->load->library('promotion_lib');
        $promotion = $ci->promotion_lib->get_basket_promotion();

        return isset($promotion['delivery_discount']) ? $promotion['delivery_discount'] : 0;
    }
}

if (!function_exists('get_displayable_datetime')) {
    /**
     * <p>Get the current datetime and format it for display.</p>
     * <p>Format: YYYY-MM-DD HH24:MI:SS</p>
     *
     * @return string Formatted datetime.
     */
    function get_displayable_datetime() {
        return date('Y-m-d H:i:s');
    }
}

if (!function_exists('get_external_variable')) {
    function get_external_variable($input_type, $name) {
        $var = filter_input($input_type, $name);
        if (empty($var)) { return NULL; }
        return $var;
    }
}

if (!function_exists('get_external_variable_filtered')) {
    function get_external_variable_filtered($input_type, $name, $filter) {
        $var = filter_input($input_type, $name, $filter);
        if (empty($var)) { return NULL; }
        return $var;
    }
}

if (!function_exists('get_page_url')) {
    /**
     * Returns URL for the current page.
     *
     * @return string Current page URL.
     */
    function get_page_url() {
        return 'http' . ((is_https()) ? 's' : '') . '://' .
                get_server_variable('SERVER_NAME') .
                get_server_variable('REQUEST_URI');
    }
}

if (!function_exists('get_payment_public_key')) {
    /**
     * Returns the payment processor API public key from the config. If the
     * config is set to run in test mode the test public key is returned,
     * otherwise the live public key is returned.
     *
     * @return string Public key to access payment processor API
     */
    function get_payment_public_key() {

        $ci =& get_instance();
        $ci->load->config('stripe');

        if ($ci->config->item(CONFIG_STRIPE_TEST_MODE) === TRUE) {
            return $ci->config->item(CONFIG_STRIPE_KEY_TEST_PUBLIC);
        }

        return $ci->config->item(CONFIG_STRIPE_KEY_LIVE_PUBLIC);
    }
}

if (!function_exists('get_redirect')) {
    /**
     * Get a redirect from POST or GET parameter. If no redirect in parameter
     * use the default.
     *
     * @param string $default_redir Optional default redirect.
     * @return string Redirect URL.
     */
    function get_redirect($default_redir='') {

        $redir = $default_redir;

        $ci =& get_instance();

        /* Get any redirect value passed with request parameter. */
        if ('POST' === $ci->input->server('REQUEST_METHOD')) {
            $redir = $ci->input->post(PARAM_REDIRECT);
        } elseif ('GET' === $ci->input->server('REQUEST_METHOD')) {
            $redir = $ci->input->get(PARAM_REDIRECT);
        }

        return $redir;
    }
}

if (!function_exists('get_server_variable')) {
    function get_server_variable($name, $filter='') {
        if (empty($filter)) {
            return get_external_variable(INPUT_SERVER, $name);
        }
        return get_external_variable_filtered(INPUT_SERVER, $name, $filter);
    }
}

if (!function_exists('is_basket_complete')) {
    /**
     * Checks if the basket has at least the minimum order size and
     * the items fit into the minimum case size exactly.
     *
     * @return boolean True if basket items fit minimum case size,
     *                 otherwise false.
     */
    function is_basket_complete($basket=NULL) {

        $ci =& get_instance();

        if ($basket === NULL) {
            $ci->load->library('basket_lib');
            $basket = $ci->basket_lib->get_basket();
        }

        if ($basket->num_items >= STORE_MIN_ORDER_SIZE &&
                $basket->num_items % STORE_MIN_CASE_SIZE === 0) {
            return TRUE;
        }

        return FALSE;
    }
}

if (!function_exists('is_https')) {
    /**
     * Check if the request protocol is HTTPS.
     * @return True if the server variable, "HTTPS", is set to "on", otherwise
     *         false.
     */
    function is_https() {
        $https = get_server_variable('HTTPS');
        return (isset($https) && strtolower($https) === 'on');
    }
}
if (!function_exists('list_matches')) {
    /**
     * Check a given <code>array</code> list of patterns for a match against a
     * given <code>string</code>.
     *
     * @param  array   $list Array list of patterns.
     * @param  string  $str String to check for a match.
     * @return boolean True if a match is found, otherwise false.
     */
    function list_matches($list, $str) {
        foreach ($list as $item) {
            if (preg_match("|$item|", $str)) {
                return TRUE;
            }
        }
        return FALSE;
    }
}

if (!function_exists('send_admin_email')) {
    function send_admin_email($subject, $message) {

        $ci =& get_instance();
        $ci->load->library('email');

        $ci->load->library('email');

        $ci->email->from(APP_ADMIN_EMAIL, APP_ADMIN_NAME);
        $ci->email->to(APP_ADMIN_EMAIL);

        $ci->email->subject($subject);
        $ci->email->message($message);

        $ci->email->send();
    }
}
/* End of file store_helper.php */
/* Location: ./application/helpers/store_helper.php */
