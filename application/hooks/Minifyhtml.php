<?php if (!defined('BASEPATH')) { exit; }
/**
 * @author Ben Mullard <ben@24beers.co.uk>
 * @copyright (c) 2014, 24 Beers Limited.
 */
class Minifyhtml {
    public function output() {
        $CI =& get_instance();
        $CI->output->set_output(preg_replace(
                array('/<!--\s.*-->/','/\/\*.*\*\//','/\>[^\S ]+/s',
                    '/[^\S ]+\</s','/(\s)+/s'),
                array('','','>','<','\\1'),
                $CI->output->get_output()));
        $CI->output->_display();
    }
}
/* End of file Minifyhtml.php */
/* Location: ./application/hooks/Minifyhtml.php */