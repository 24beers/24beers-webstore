<?php if (! defined('BASEPATH')) { exit; }
/**
 * @package 24BeersWebStore
 * @subpackage Controllers
 * @category Controllers
 */
class Product extends CI_Controller {

    /**
     * Gets product information by given product code and builds the product
     * detail page.
     * @access public
     * @param type $product_code
     */
    public function index($product_code) {
        $this->_build_product_view($product_code);
    }

    /**
     * Get details of a given product and return data in JSON format.
     * 
     * @access public
     * @param string $product_code
     */
    public function get($product_code) {
        $this->load->library('product_lib');
        $this->_display_product_data($this->product_lib->get_product($product_code));
    }

    /**
     * Get details of product from a SKU code.
     * TODO: This function is added so that basket can get product details by
     * SKU code. These details should be stored with basket item.
     * 
     * @param string $sku_code
     */
    public function get_sku($sku_code) {
        $this->load->model('product_model');
        $this->_display_product_data($this->product_model->get_product_by_sku_code($sku_code));
    }

    public function get_stock($product_code) {
        $this->load->model('product_model');
        $stock_level = $this->product_model->get_stock_level($product_code);

        echo json_encode(array('stock_level' => $stock_level));
    }

    /**
     * Controller for when a product review is submitted.
     * @return void
     */
    public function review() {

        $product_code = $this->input->post('product_code', TRUE);

        $this->load->library('form_validation');

        if (!$this->form_validation->run()) {
            $this->_build_product_view($product_code);
            return;
        }

        $this->load->library('product_lib');

        $this->product_lib->add_review($product_code,
                $this->input->post('rating', TRUE),
                $this->input->post('comment', TRUE));

        redirect(URL_PRODDETAIL_BEER . "/$product_code");
    }

    private function _build_product_view($product_code) {

        /* Get the product details. */
        $this->load->library('product_lib');
        $product = $this->product_lib->get_product($product_code);

        /* Show error page if a product isn't found. */
        if (!isset($product)) {
            show_error(ERROR_PRODUCT_NOTFOUND);
        }

        $data['grid_products'] = $this->_get_cross_sells($product_code);
        $data['cross_sell_content'] = $this->load->view('include/product_grid', $data, TRUE);

        /* Generate the breadcrumb and load the content in to the page. */
        $data['breadcrumb'] = generate_breadcrumb(array(
            array(
                'label' => $product->style_name,
                'link'  => URL_CATEGORY_STYLE . "/$product->style_code"
            ),
            array(
                'label' => $product->name,
            )
        ));

        $data['breadcrumb_content'] = $this->load->view('include/breadcrumb', $data, true);

        $data['product'] = $product;

        $links = $this->_generate_links($product_code);

        $data['encoded_image_url'] = $links->encoded_image_url;
        $data['encoded_page_url']  = $links->encoded_page_url;
        $data['image_url']         = $links->image_url;
        $data['page_url']          = $links->page_url;

        $data['content'] = $this->load->view('content/product_detail_page', $data, true);

        $data['meta_description'] = $product->description;

        $this->load->view('product_view', $data);
    }

    /**
     * Display the given product object as JSON data.
     * @param object $product
     */
    private function _display_product_data($product) {
        if (isset($product)) {
            echo json_encode($product);
        }
    }

    /**
     * Generate an object containing the links that are used in the page.
     *
     * @param  string $product_code
     * @return object Object containing URLs that are used in the page.
     */
    private function _generate_links($product_code) {

        $page_url = site_url(URL_PRODDETAIL_BEER . "/$product_code");
        $image_url = site_url(URL_MEDIA_PRODUCT . "/$product_code" . "_large.jpg");

        return (object) array(
            'encoded_image_url' => urlencode($image_url),
            'encoded_page_url'  => urlencode($page_url),
            'image_url'         => $image_url,
            'page_url'          => $page_url
        );
    }

    private function _get_cross_sells($product_code) {
        $this->load->model('product_model');
        return $this->product_model->get_cross_sells($product_code);
    }
}
/* End of file product.php */
/* Location: ./application/controllers/product.php */