<?php if (!defined('BASEPATH')) { exit; }

class Blog extends CI_Controller {

    public function Blog() {
        parent::__construct();
        $this->load->model('blog_model');
    }
    /**
     * Display the blog home page.
     */
    public function index() {
        $last30blogs = $this->blog_model->get_last_n_blogs(30);
        $data['blog_list'] = array_slice($last30blogs, 0, 5);
        $data['blog_nav'] = $last30blogs;

        $data['meta_description'] = format_message(META_DESCR_BLOG_HOME);

        $this->_build_view("Beer &amp; Ale News from 24 Beers",
                'content/blog_home', $data, 60);
    }

    /**
     * Display the blog entry.
     *
     * @param string $code
     */
    public function entry($code) {
        $blog = $this->blog_model->get_blog_by_id($code);

        if (!$blog) {
            show_404("", TRUE);
        }

        $data['blog'] = $blog;
        $data['blog_nav'] = $this->blog_model->get_last_n_blogs(30);

        $this->_build_view($blog->title, 'content/blog_entry', $data, 1440);
    }

    public function feed() {
        $rss_date_format = "D, d M Y H:i:s O";
        $this->load->helper('date');
        $time = time();
        $data['copyright_year'] = gmdate("Y", $time);
        $data['publish_date'] = gmdate($rss_date_format, $time);
        $data['language'] = "en-gb";

        $blogs = $this->blog_model->get_last_n_blogs(100);
        foreach ($blogs as $blog) {
            $blog->publish_date =
                    gmdate($rss_date_format, strtotime($blog->date));
            $blog->url = site_url("/blog/entry/$blog->id");
        }
        $data['blogs'] = $blogs;

        header("Content-Type: application/rss+xml");
        $this->load->view('feed_view', $data);
        $this->output->cache(60*24);
    }

    /**
     * Build the view for the blog pages.
     * 
     * @access private
     * @param string  $title        Page title.
     * @param string  $view_content Template containing content for the view.
     * @param array   $data         Optional data to populate content.
     * @param integer $cache_ttl    Optional cache time-to-live.
     */
    private function _build_view(
            $title, $view_content, $data=array(), $cache_ttl=0) {

        $data['page_title'] = "$title | 24 Beers Blog";

        $data['content'] = $this->load->view($view_content, $data, TRUE);

        $this->load->view('blog_view', $data);

        /* If a cache time-to-live is set, cache the page for that time. */
        if ($cache_ttl > 0) {
            //$this->output->cache($cache_ttl);
        }
    }
}
/* End of file blog.php */
/* Location: ./application/controllers/blog.php */