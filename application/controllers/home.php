<?php if (!defined('BASEPATH')) { exit; }

class Home extends CI_Controller {

    public function index() {
        $this->load->library('collection_lib');
        $data['grid_products'] = $this->collection_lib->get_collection_24beers();

        $data['product_grid'] = $this->load->view('include/product_grid', $data, TRUE);

        $data['content'] = $this->load->view('content/home_page', $data, TRUE);

        $data['meta_description'] = format_message(META_DESCR_HOME, STORE_DELIVERY_CHARGE_STANDARD);

        $this->load->view('home_view', $data);
    }
}
/* End of file home.php */
/* Location: ./application/controllers/home.php */