<?php if (!defined('BASEPATH')) { exit; }

/**
 * Checkout controller for functions relating to the checkout process.
 */
class Checkout extends CI_Controller {

    /**
     * The checkout gateway decides where a request should be routed to based on
     * state of the customer's details.
     */
    public function index() {
        /* Auth validation has happened by this point. */

        $customer_code = get_custcode_from_session();
        if (empty($customer_code)) {
            show_error(ERROR_SESSION_NOCUSTCODE);
        }

        $this->load->model('customer_model');

        if ($this->customer_model->get_address_count($customer_code) === 0) {
            /* If the customer doesn't have an address load the "New Address" checkout content. */
            $this->_build_checkout_view('content/checkout_new_address', 'New Address');
        } else {
            $this->_build_checkout_final_page();
        }
    }

    /**
     * Display a form for the customer to edit their personal details.
     */
    public function aboutyou_edit() {
        $this->_build_aboutyou_edit_page();
    }

    /**
     * Update the customer's edited personal details.
     *
     * @return void
     */
    public function aboutyou_update() {

        $this->load->library('form_validation');

        if (!$this->form_validation->run()) {
            $this->_build_aboutyou_edit_page();
            return;
        }

        $this->load->library('account_lib');

        $this->account_lib->update_customer(
                $this->input->post('name', TRUE),
                $this->input->post('email', TRUE),
                $this->input->post('main_phone', TRUE),
                $this->input->post('alternative_phone', TRUE),
                $this->input->post('dob', TRUE));

        redirect(URL_CHECKOUT);
    }

    /**
     * Show form for customer to add a new address.
     */
    public function address_add() {
        $this->_build_checkout_view('content/checkout_address_add', 'Add Address');
    }

    /**
     * Create new address from the given POST values.
     */
    public function address_create() {

        $this->load->library('form_validation');

        if (!$this->form_validation->run()) {
            $this->_build_checkout_view('content/checkout_address_add', 'Add Address');
            return;
        }

        $this->load->library('account_lib');

        $this->account_lib->create_address($this->input->post('name', TRUE),
                $this->input->post('house', TRUE),
                $this->input->post('street', TRUE),
                $this->input->post('town', TRUE),
                $this->input->post('city', TRUE),
                COUNTRY_UK,
                $this->input->post('postcode', TRUE));

        redirect(URL_CHECKOUT);
    }

    /**
     * Action to delete the given address.
     *
     * @param string $address_code
     */
    public function address_delete($address_code) {
        $this->load->library('account_lib');
        $this->account_lib->delete_address($address_code);

        redirect(URL_CHECKOUT_ADDRESSBOOK);
    }

    /**
     * Page to edit an existing address.
     *
     * @param string $address_code
     */
    public function address_edit($address_code) {
        $this->_build_address_edit_page($address_code);
    }

    /**
     * List customer's addresses for edit/delete.
     */
    public function addressbook() {
        $customer_code = get_custcode_from_session();
        if (empty($customer_code)) {
            show_error(ERROR_SESSION_NOCUSTCODE);
        }

        $this->load->model('customer_model');
        $data['addresses'] = $this->customer_model->get_addresses($customer_code);

        $this->_build_checkout_view('content/addressbook', 'Address Book', $data);
    }

    /**
     * <p>Display the order confirmation page using the given order code. If an
     * order isn't found corresponding the order code and logged in customer the
     * visitor is redirected to the home page.</p>
     *
     * @param type $order_code
     */
    public function confirmation($order_code) {
        $this->load->library('checkout_lib');
        $order = $this->checkout_lib->get_order_by_code($order_code);

        if (!isset($order) || empty($order)) {
            show_404();
        }

        $data['order'] = $order;
        $data['delivery_address'] = $this->checkout_lib->get_order_address($order_code);
        $data['order_lines'] = $this->checkout_lib->get_order_lines($order_code);

        $this->_build_checkout_view('content/checkout_order_confirmation', 'Order Confirmation', $data, TRUE);
    }

    /**
     * Collect customer's basket details and redirect to
     * PayPal Express Checkout.
     */
    public function paypal() {
        /* Get contents of customer's basket. */

        /* Load PayPal configuration */
        $this->config->load('paypal', true);

        /* Redirect to PayPal Express Checkout. */
    }

    /**
     * Validate order details and send order to payment card processor.
     *
     * @return void
     */
    public function submit_order() {

        $this->load->library('form_validation');

        if (!$this->form_validation->run()) {
            $this->_build_checkout_final_page();
            return;
        }

        $token = $this->input->post('stripeToken', TRUE);

        /* Set payment token into session so it can't be resubmitted. */
        $this->session->set_userdata(SESSION_PAYMENT_TOKEN, $token);

        $this->load->library('checkout_lib');

        /* Generate order details */
        $order = $this->checkout_lib->generate_order();

        /* Process card payment. */
        $payment_response = $this->checkout_lib->submit_payment($token, $order);

        if (!$payment_response->success) {
            /* Notify the customer that the payment has failed. */
            $data['payment_error'] = $payment_response->message;

            $this->_build_checkout_final_page($data);
            return;
        }

        /* Complete the order with the response from submitting payment. */
        $this->checkout_lib->complete_order($order, $payment_response, $this->input->post('address_code'));

        /* Order has been successfully placed and persisted
         * so do some housekeeping. */
        $this->load->library('basket_lib');
        $this->basket_lib->empty_basket();

        $complete_order = $this->checkout_lib->get_order_by_transaction($payment_response->transaction_id);

        $this->checkout_lib->process_post_order_promotions($complete_order);
        $this->checkout_lib->send_order_confirmation_email($complete_order->code);

        redirect(URL_CHECKOUT_CONFIRMATION . "/$complete_order->code");
    }

    /**
     * Build page with form to edit About You information.
     *
     * @access private
     */
    private function _build_aboutyou_edit_page() {
        $this->load->library('account_lib');
        $data['customer'] = $this->account_lib->get_customer();

        $this->_build_checkout_view('content/checkout_aboutyou_edit', 'Change Your Details', $data);
    }

    /**
     * Build page with form to edit an address.
     *
     * @param string $address_code
     */
    private function _build_address_edit_page($address_code) {
        $this->load->library('account_lib');

        $data['address'] = $this->account_lib->get_address($address_code);

        $this->_build_checkout_view('content/checkout_address_edit', 'Edit Address', $data);
    }

    /**
     * Build final checkout page.
     *
     * @param array $data Optional data array.
     */
    private function _build_checkout_final_page($data=array()) {

        $this->load->library('account_lib');
        $data['customer'] = $this->account_lib->get_customer();

        $this->load->library('basket_lib');
        $data['basket'] = $this->basket_lib->get_basket();

        $data['payment_key'] = get_payment_public_key();

        $this->_build_checkout_view('content/checkout_final', 'Confirm Order', $data);
    }

    /**
     * Build checkout view used for each of the checkout pages.
     *
     * @param string $content_template
     * @param array $data
     */
    private function _build_checkout_view(
            $content_template, $title, $data=array(), $track_ecommerce=FALSE) {
        $data['page_title'] = $title;
        $data['track_ecommerce'] = $track_ecommerce;
        $data['content'] = $this->load->view($content_template, $data, TRUE);

        $this->load->view('checkout_view', $data);
    }

    /**
     * Check that products in the order have not gone out of stock before the
     * order is placed.
     */
    private function _validate_order_stock() {

    }
}
/* End of file chckout.php */
/* Location: ./application/controllers/checkout.php */