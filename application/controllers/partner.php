<?php if (! defined('BASEPATH')) { exit; }

class Partner extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('partner_model');
    }

    public function index($partner_code) {
        $this->session->set_userdata(SESSION_PARTNER_CODE, $partner_code);

        $this->partner_model->load_partner($partner_code);
        $config = $this->partner_model->get_config();

        if (!isset($config)) {
            redirect(URL_HOME . '?utm_medium=' . $this->input->get('utm_medium', TRUE)
                    . '&utm_source=' . $this->input->get('utm_source', TRUE)
                    . '&utm_campaign=' . $this->input->get('utm_campaign', TRUE));
        }

        $content = '';

        if (!empty($config->landing_page)) {
            $content = $this->load->view("content/partner_$partner_code", NULL, TRUE);
        }

        if (empty($content)) {
            show_404(NULL, TRUE);
        }

        $data['content'] = $content;
        $this->_build_view($data);
    }

    private function _build_view($data)  {
        $this->load->view('partner_view', $data);
    }
}
/* End of file partner.php */
/* Location: ./application/controllers/partner.php */