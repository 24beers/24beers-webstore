<?php if (!defined('BASEPATH')) { exit; }

class Brewery extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('brewery_model');
    }

    public function index($brewery_code='') {
        if (empty($brewery_code)) {
            return $this->_build_index_view();
        }

        $this->_build_detail_view($brewery_code);
    }

    private function _build_index_view() {
        $data['brewery_list'] = $this->brewery_model->get_brewery_list();
        $data['content'] = $this->load->view('content/brewery_home', $data, TRUE);

        $this->_build_view("About Our Breweries", META_DESCR_BREWERY_HOME, $data);
    }

    private function _build_detail_view($brewery_code) {
        $brewery = $this->brewery_model->get_brewery_by_code($brewery_code);

        if (empty($brewery)) {
            show_404('', TRUE);
        }

        $this->load->model('product_model');
        $products = $this->product_model->get_products_by_brewery($brewery->code);

        if (empty($products)) {
            show_404('', TRUE);
        }

        $data['brewery'] = $brewery;
        $data['grid_products'] = $products;
        $data['product_grid'] = $this->load->view('include/product_grid', $data, TRUE);
        $data['content'] = $this->load->view('content/brewery_page', $data, TRUE);

        $this->_build_view($brewery->name, $brewery->description, $data);
    }

    private function _build_view($title, $description, $data) {
        $data['page_title'] = $title;
        $data['meta_description'] = $description;
        $this->load->view('main_view', $data);
    }
}
/* End of file brewery.php */
/* Location: ./application/controllers/brewery.php */