<?php if (!defined('BASEPATH')) { exit; }

class Test extends CI_Controller {

    public function index() {
        echo "<pre>";
        $this->load->library('promotion_lib');
        $this->promotion_lib->run_kitty('benmullard-255437244259631104', 'aaaaao');
        echo "<pre>";
    }

    public function view() {
        $this->load->model('basket_model');
        $this->basket_model->set_basket_key('test_basket');
        $item = $this->basket_model->get_item_from_basket('test_product');
        print_r($item);
    }

    public function viewall() {
        $this->load->model('basket_model');
        $this->basket_model->set_basket_key('test_basket');
        $basket = $this->basket_model->get_basket();
        print_r($basket);
    }

    public function phpinfo() {
        phpinfo();
    }

    public function uuid() {
        echo generate_uuid();
    }
}
/* End of file test.php */
/* Location: ./application/controllers/test.php */