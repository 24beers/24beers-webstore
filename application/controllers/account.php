<?php if (! defined('BASEPATH')) { exit; }

/**
 * Account controller for functions relating to customer account admin pages,
 * including registration and sign-in pages.
 */
class Account extends CI_Controller {

    /**
     * Display the main account page.
     */
    public function index() {
        $this->load->library('account_lib');
        $customer = $this->account_lib->get_customer();

        if (empty($customer)) {
            show_404(NULL, TRUE);
        }

        $this->load->model('account_model');
        $this->account_model->load_customer_account($customer->code, ACCT_TYPE_CREDIT);
        $customer->credit_account = $this->account_model->get_account();

        $data['customer'] = $customer;
        $this->_build_view('Your Account', 'content/account_home', $data);
    }

    /**
     * Show form for customer to add a new address.
     */
    public function address_add() {
        $this->_build_view('Add Address', 'content/address_add');
    }

    /**
     * Create new address from the given POST values.
     */
    public function address_create() {
        $this->load->library('form_validation');

        if (!$this->form_validation->run()) {
            $this->_build_view('Add Address', 'content/address_add');
            return;
        }

        $this->load->library('account_lib');

        $this->account_lib->create_address($this->input->post('name', true),
                $this->input->post('house', true),
                $this->input->post('street', true),
                $this->input->post('town', true),
                $this->input->post('city', true),
                COUNTRY_UK,
                $this->input->post('postcode', true));

        redirect(URL_ACCT_HOME);
    }

    /**
     * Action to delete the given address.
     *
     * @param string $address_code
     */
    public function address_delete($address_code) {
        $this->load->library('account_lib');

        $this->account_lib->delete_address($address_code);

        redirect(URL_ACCT_HOME);
    }

    /**
     * Page to edit an existing address.
     *
     * @param string $address_code
     */
    public function address_edit($address_code) {
        $this->load->library('account_lib');

        $data['address'] = $this->account_lib->get_address($address_code);

        $this->_build_view('Edit Address', 'content/address_edit', $data);
    }

    /**
     * Validate the updated address and persist it if validated. Otherwise build
     * an error view.
     */
    public function address_update() {
        $this->load->library('form_validation');

        if (!$this->form_validation->run()) {
            $data['address'] = (object) array(
                'name' => $this->input->post('name', true)
            );

            $this->_build_view('Edit Address', 'content/address_edit', $data);
            return;
        }

        /*$this->load->library('account_lib');

        $this->account_lib->create_address($this->input->post('name', true),
                $this->input->post('house', true),
                $this->input->post('street', true),
                $this->input->post('town', true),
                $this->input->post('city', true),
                COUNTRY_UK,
                $this->input->post('postcode', true));

        redirect(URL_ACCT_HOME);*/
    }

    /**
     * Authorise a customer's sign-in details.
     * @return void
     */
    public function auth() {

        $this->load->library('form_validation');

        if (!$this->form_validation->run()) {
            $this->_build_view(
                    'Sign in or Join', 'content/account_signin');
            return;
        }

        $this->load->library('auth_lib');

        /* Validate the email and password credentials given for the auth. */
        $customer_code = $this->_get_auth(
                $this->input->post('email', true),
                $this->input->post('password', true));

        if ($customer_code === FALSE) {
            /* De-authorise the session if validation fails. */
            $this->auth_lib->deauth_session();

            $this->_build_view(
                    'Sign in or Join', 'content/account_signin');

            return;
        }

        /* Authorise the session if validation is successful. */
        $this->auth_lib->auth_session($customer_code);

        redirect(get_redirect(URL_ACCT_HOME));
    }

    /**
     * Action to create a new customer account.
     */
    public function create() {
        $this->load->library('form_validation');

        /* If validation is not successful, show the join page with errors. */
        if (!$this->form_validation->run()) {
            $this->_build_view('About You', 'content/account_join');
            return;
        }

        /* Date of birth can be entered in either DD/MM/YYYY or YYYY-MM-DD.
         * Convert it to YYYY-MM-DD before inserting. */


        $this->load->model('Customer_model');

        /* Persist customer details to datastore */
        $customer_code = $this->Customer_model->create_customer(
                $this->input->post('name', true),
                $this->input->post('phone', true),
                $this->input->post('email', true),
                $this->input->post('password', true),
                $this->_convert_dob($this->input->post('dob', true)));

        /* Once customer account is created, auth the session. */
        $this->load->library('auth_lib');
        $this->auth_lib->auth_session($customer_code);

        redirect(get_redirect(URL_ACCT_HOME));
    }

    public function detail() {
        $this->_build_detail_edit_page();
    }

    public function detail_update() {

        $this->load->library('form_validation');

        if (!$this->form_validation->run()) {
            $this->_build_detail_edit_page();
            return;
        }

        $this->load->library('account_lib');

        $this->account_lib->update_customer(
                $this->input->post('name', TRUE),
                $this->input->post('email', TRUE),
                $this->input->post('main_phone', TRUE),
                $this->input->post('alternative_phone', TRUE),
                $this->input->post('dob', TRUE));

        redirect(URL_ACCT_HOME);
    }

    /**
     * Display the join form for new customers to create an account.
     */
    public function join() {
        $this->_build_view('About You', 'content/account_join');
    }

    /**
     * Form to enter new password when using "reset password" function.
     * 
     * @param string $token Password reset token that is emailed to customer.
     */
    public function new_password($token) {

        $data['token'] = $token;

        $this->_build_view(
                'New Password', 'content/account_new_password', $data);
    }

    /**
     * Validate the email address supplied and send a link to reset the password
     * for the customer account associated with the email address.
     */
    public function reset_password() {
        $this->load->library('form_validation');

        $data = array();

        if ($this->form_validation->run()) {
            $this->load->library('account_lib');

            /* Send an email with a reset link */
            $this->account_lib->create_password_reset(
                    $this->input->post('reset-email', true));

            $data['password_reset_message'] = INFO_PASSWORD_RESET_SENT;
        }

        $this->_build_view('Sign in or Join', 'content/account_signin', $data);
    }

    /**
     * Show the sign-in form.
     */
    public function signin() {
        $this->_build_view('Sign in or Join', 'content/account_signin');
    }

    /**
     * Sign the customer out
     */
    public function signout() {
        $this->load->library('auth_lib');
        $this->auth_lib->deauth_session();
        redirect();
    }

    /**
     * Action to update the customer's password from reset password request.
     *
     * @return void
     */
    public function update_password() {

        $token = $this->input->post('token', TRUE);

        $this->load->library('form_validation');

        /* Validate the update password form. */
        if (!$this->form_validation->run()) {
            $data['token'] = $token;

            $this->_build_view(
                'New Password', 'content/account_new_password', $data);
            return;
        }

        /* Get the customer code from the password reset token. */
        $this->load->library('account_lib');
        $email = $this->account_lib->get_password_reset_email($token);

        $success = $this->account_lib->update_password(
                $email, $this->input->post('password', TRUE));

        if (!$success) {
            $data['token'] = $token;

            $this->_build_view(
                'New Password', 'content/account_new_password', $data);
            return;
        }

        redirect(URL_ACCT_SIGNIN);
    }

    /**
     * Build the edit details form page.
     *
     * @access private
     */
    private function _build_detail_edit_page() {

        $this->load->library('account_lib');
        $data['customer'] = $this->account_lib->get_customer();

        $this->_build_view('Edit Details', 'content/account_detail', $data);
    }

    /**
     * Build the account page view and include content.
     *
     * @access private
     * @param  string $title        Page title.
     * @param  string $content_path Path to content template.
     * @param  array  $data         Optional associative array of page data.
     */
    private function _build_view($title, $content_path, $data=array()) {

        $data['page_title'] = $title;

        $data['content'] = $this->load->view($content_path, $data, true);

        $this->load->view('account_view', $data);
    }

    /**
     * Date of birth can be entered in either DD/MM/YYYY or YYYY-MM-DD.
     * Convert it to YYYY-MM-DD for database insert.
     *
     * @param string $dob Date of birth string.
     */
    private function _convert_dob($dob) {

        if (strpos($dob, '/')) {
            list($d, $m, $y) = explode('/', $dob);
            return "$y-$m-$d";
        }

        return $dob;
    }

    /**
     * Private method to validate that the submitted credentials match what's
     * stored with the customer account details. If auth is successful the
     * customer code is returned.
     *
     * @access private
     * @param type $email
     * @param type $password
     * @return mixed False if authorisation is not valid, otherwise return
     *               customer code.
     */
    private function _get_auth($email, $password) {

        $this->load->model('Customer_model');

        $customer = $this->Customer_model->get_customer_by_email($email);

        if (empty($customer)) {
            $this->form_validation->set_message(
                    '_get_auth', ERROR_AUTH_FAILED);
            return false;
        }

        $this->load->library('encrypt');

        /* Auth succeeds if password from DB matches given password when
         * encrypted with salt from DB. */
        if ($customer->password ===
                $this->encrypt->encrypt($password, $customer->salt)) {

            /* If auth is successful return the customer code. */
            return $customer->code;
        }

        $this->form_validation->set_message('_get_auth', ERROR_AUTH_FAILED);

        return false;
    }
}
/* End of file account.php */
/* Location: ./application/controllers/account.php */