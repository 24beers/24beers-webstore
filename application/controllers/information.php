<?php if (! defined('BASEPATH')) { exit; }

class Information extends CI_Controller {

    public function index() {
        header('HTTP/1.0 403 Forbidden');
    }

    public function competition($comp_name) {
        switch ($comp_name):
            case (COMP_NAME_MAY2014):
                $this->_build_comp_may2014();
                break;
            default:
                show_404('', TRUE);
        endswitch;
    }

    public function twentyfourbeers() {
        $this->_build_view("Twenty Four Beers",
                $this->load->view('content/twentyfourbeers', NULL, TRUE));
    }

    public function delivery() {
        $this->_build_view("Delivery Information",
                $this->load->view('content/delivery', NULL, TRUE));
    }

    public function faq() {
        $this->_build_view("Frequently Asked Questions",
                $this->load->view('content/faq', NULL, TRUE));
    }

    public function newsroom() {
        $this->_build_view("Newsroom",
                $this->load->view('content/newsroom', NULL, TRUE));
    }

    public function privacy() {
        $this->_build_view("Privacy Policy",
                $this->load->view('content/privacy', NULL, TRUE));
    }

    /**
     * Controller method switches for given promotion name and builds the
     * promotion content. If promotion name doesn't match a configured promotion
     * a Page Not Found error is displayed.
     *
     * @param string $promo Promotion name
     */
    public function promo($promo) {
        switch ($promo):
            case (PROMO_NAME_BELGIANBEER):
                $this->_build_promo_belgianbeer();
                break;
            case (PROMO_NAME_STGEORGESDAY):
                $this->_build_promo_stgeorgesday();
                break;
            case (PROMO_NAME_VALENTINESGIFTS):
                $this->_build_promo_valentinesday();
                break;
            default:
                show_404("", TRUE);
        endswitch;
    }

    public function terms() {
        $this->_build_view("Terms &amp; Conditions for " . SITE_NAME,
                $this->load->view('content/terms', NULL, TRUE));
    }

    private function _build_comp($title, $content_name, $data=array()) {
        $data['content'] = $this->load->view("content/$content_name", $data, TRUE);
        $data['page_title'] = $title;
        $this->load->view('main_view', $data);
    }

    private function _build_comp_may2014() {
        $data['meta_description'] = format_message(META_DESCR_COMP_MAY2014);
        $this->_build_comp('Win Free Beer in May with 24 Beers', 'comp_may2014', $data);
    }

    /**
     * Build promotion page using the given content.
     * @param string $title Page title.
     * @param string $content_name Name of file holding the content.
     * @param array  $data Optional array of content parameters.
     */
    private function _build_promo($title, $content_name, $data=array()) {
        $this->_build_view($title, $this->load->view("content/$content_name", $data, TRUE), $data);
    }

    /**
     * Build content for Belgian Beers promotion page.
     */
    private function _build_promo_belgianbeer() {
        $data['meta_description'] = format_message(
                META_DESCR_PROMO_BELGIANBEER,
                STORE_DELIVERY_CHARGE_STANDARD);
        $this->_build_promo(
                "Buy Belgian Beer Online", "promo_belgianbeer", $data);

    }

    /**
     * Build content for St. George's Day promotion page.
     */
    private function _build_promo_stgeorgesday() {
        $data['meta_description'] = format_message(
                META_DESCR_PROMO_STGEORGESDAY,
                STORE_DELIVERY_CHARGE_STANDARD);
        $this->_build_promo(
                "St. George's Day Beer", "promo_stgeorgesday", $data);

    }

    /**
     * Build content for Valentine's Day promotion page.
     */
    private function _build_promo_valentinesday() {
        $data['meta_description'] = format_message(
                META_DESCR_PROMO_VALENTINESDAY,
                STORE_DELIVERY_CHARGE_STANDARD);
        $this->_build_promo(
                "Valentine's Day Gifts", "promo_valentinesgifts", $data);
    }

    private function _build_view($title, $information, $data=array()) {

        $data['page_title'] = $title;
        $data['information'] = $information;

        $data['content'] =
                $this->load->view('content/information_page', $data, TRUE);

        /* Get facet navigation for view. */
        /*$this->load->model('search_model');
        $search_result = $this->search_model->get_all_facet_result();
        $data['facet_categories'] = $search_result->facet_categories;*/

        $this->load->view('main_view', $data);

        //$this->output->cache(CACHE_INFORMATION_PAGE);
    }
}
/* End of file information.php */
/* Location: ./application/controllers/information.php */