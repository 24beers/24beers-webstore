<?php if (!defined('BASEPATH')) { exit; }

class Basket extends CI_Controller {

    public function index() {
        $this->_build_basket_page();
    }

    /**
     * Add a product and quantity to the customer's basket and redirect back to
     * the page they came from.
     */
    public function add() {
        $this->add_async();
        $this->_build_basket_page();
    }

    /**
     * Add a product to the basket but do not return content. Used if called
     * asynchronously, such as with JavaScript.
     *
     * @access public
     */
    public function add_async() {
        if ($this->_addtobasket_validate()) {
            $this->_addtobasket($this->input->post('sku_code', TRUE), $this->input->post('quantity', TRUE));
        }
    }

    /**
     * Validate that the basket is good to proceed to the checkout.
     *
     * @access public
     * @return void
     */
    public function checkout() {
        $this->load->library('basket_lib');
        $basket = $this->basket_lib->get_basket();
        /* Bottles must fit into minimum case size or
         * basket value must be above threshold order value. */
        if (!is_basket_complete($basket)) {
            $this->_build_basket_page();
            return;
        }
        redirect(URL_CHECKOUT);
    }

    /**
     * Get the contents of the customer's basket and return it in JSON format.
     * @access public
     * @return string Basket contents in JSON encoded format.
     */
    public function get() {
        $this->load->library('basket_lib');
        echo $this->basket_lib->get_basket_data();
    }

    /**
     * Remove an item of the given product code from the basket.
     *
     * @param string $item_code
     */
    public function less($item_code) {

        $this->load->library('basket_lib');
        $this->basket_lib->decrease_item_in_basket($item_code);

        redirect(URL_BASKET);
    }

    /**
     * Add an item of the given product code to the basket.
     *
     * @param string $item_code
     */
    public function more($item_code) {

        $this->load->library('basket_lib');
        $this->basket_lib->increase_item_in_basket($item_code);

        redirect(URL_BASKET);
    }

    public function remove($item_code) {

        $this->load->library('basket_lib');
        $this->basket_lib->remove_basket_item($item_code);

        redirect(URL_BASKET);
    }

    /**
     * Add quantity of product to the basket.
     *
     * @access private
     */
    private function _addtobasket($item_code, $quantity) {
        $this->load->library('basket_lib');
        $this->basket_lib->add_item_to_basket($item_code, $quantity);
    }

    /**
     * Validate that correct form values have been submitted.
     *
     * @access private
     * @return boolean True if all submitted form values are correct.
     */
    private function _addtobasket_validate() {
        $this->load->library('form_validation');
        return $this->form_validation->run();
    }

    /**
     * @param array Data to populate page.
     * @access private
     */
    private function _build_basket_page() {
        $this->load->library('basket_lib');
        $basket = $this->basket_lib->get_basket();

        $bottles_needed = bottles_needed_to_complete_basket($basket->num_items);

        $basket_messages = array();

        /* If basket is not complete set a message to show customer how many
         * more bottles needed to fill next case. */
        if ($bottles_needed > 0) {
            array_push($basket_messages, sprintf(INFO_CASE_REMAINDER, $bottles_needed, STORE_MIN_ORDER_SIZE, STORE_MIN_CASE_SIZE));
        }

        $data['basket_messages'] = $basket_messages;
        $data['page_title'] = 'Your Basket';
        $data['basket'] = $basket;
        $data['content'] = $this->load->view('content/basket_page', $data, true);

        $this->load->view('basket_view', $data);
    }
}
/* End of file basket.php */
/* Location: application/controllers/basket.php */