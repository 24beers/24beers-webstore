<?php if (!defined('BASEPATH')) { exit; }

class Search extends CI_Controller {

    public function index() {

        /* Get search query input and clean XSS. */
        $query = $this->input->get('q', true);

        $this->load->model('search_model');

        /* Get search results using query parameter. */
        $search_result = $this->search_model->get_query_facet_result($query);

        $this->_build_view($query, '',
                $search_result->result_count,
                $search_result->product_list);
    }

    function category_filter($cat_type, $cat_code) {

        /* Get details for selected category. */
        $attribute = $this->_get_attribute_detail($cat_type, $cat_code);

        $this->load->model('search_model');

        /* Get search results for all products in given category with given
         * category code. */
        $search_result = $this->search_model->get_term_facet_result(
                "$cat_type.code", $cat_code);

        $this->_build_view($attribute->name,
                $attribute->text,
                $search_result->result_count,
                $search_result->product_list);
    }

    function _build_view($title, $category_text, $result_count, $product_list) {

        $this->load->model('search_model');

        $facet_result = $this->search_model->get_all_facet_result();

        $data['page_title'] = (empty($title)) ? "All Beers" : $title;
        $data['category_text'] = $category_text;
        $data['result_count'] = $result_count;
        $data['grid_products'] = $product_list;
        $data['content'] = $this->load->view('content/search_result_page', $data, true);
        $data['facet_categories'] = $facet_result->facet_categories;

        $this->load->view('main_view', $data);
    }

    function _get_attribute_detail($type, $code) {
        $this->load->model('attribute_model');

        return $this->attribute_model->get_attribute_value($type, $code);
    }
}
/* End of file search.php */
/* Location: ./application/controllers/search.php */